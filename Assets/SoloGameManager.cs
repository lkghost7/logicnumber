﻿using System;
using UnityEngine;

public class SoloGameManager : GenericSingletonClass<SoloGameManager>
{
    [SerializeField] public PvpWinSolo pvpWinSolo;
    string magic = String.Empty;
    
    public void Show(int status)
    {
        GetMagicNum();
        int rowNum = GameManager.Instance.turnControl.rawNumber+1;
        pvpWinSolo.ShowLocal(status, TimeManager.Instance.timeMain.text,magic,rowNum.ToString());
    }

    private void GetMagicNum()
    {
        for (int i = 0; i < GameManager.Instance.turnControl.numLimit; i++)
        {
            magic += GameManager.Instance.turnControl.magicNumber[i];
        }
    }
}
