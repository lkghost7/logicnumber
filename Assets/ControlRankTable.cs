﻿using UnityEngine;
using UnityEngine.UI;

public class ControlRankTable : MonoBehaviour
{
    [SerializeField] public RankPanel tableMain;
    [SerializeField] public RankPanelClassic tableClassic;

    // [SerializeField] private Button mainRankBtn;
    // [SerializeField] private Button classicRankBtn;
    
    [SerializeField] private GameObject mainRankPanel;
    [SerializeField] private GameObject classicRankPanel;
    
    public void PressBtnRank()
    {
        mainRankPanel.SetActive(false);
        tableClassic.Show();
    }
    
    public void PressBtnRankIMenu()
    {
        mainRankPanel.SetActive(false);
        tableClassic.Show(true);
    }
 
    public void PressBtnClassic()
    {
        classicRankPanel.SetActive(false);
        tableMain.Show(); // tru
    }
    
    public void PressBtnClassicInMenu()
    {
        classicRankPanel.SetActive(false);
        tableMain.Show(true); // tru
    }

    public void ExitToMenu()
    {
        GameManager.Instance.startGameControlRef.ExitMainMenu();
    }
    
    // public void Show()
    // {
    //     tableMain.Show();
    // }
    //
    // public void GetRank()
    // {
    //     tableMain.GetRank();
    // }

    public void ShowAchive()
    {
        GameSettings.Instance.achiveManager.ShowAchive();
    }

    public void ShowRecord()
    {
        GameSettings.Instance.achiveManager.ShowLeader();
    }
}
