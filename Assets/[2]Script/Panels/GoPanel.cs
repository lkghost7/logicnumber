﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class GoPanel : MonoBehaviour
{
    public void Show()
    {
        gameObject.SetActive(true);
        StartCoroutine(Hide());
    }

    IEnumerator Hide()
    {
        yield return new WaitForSecondsRealtime(1.35f);
        gameObject.GetComponent<CanvasGroup>().DOFade(0, 0.5f).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }
}