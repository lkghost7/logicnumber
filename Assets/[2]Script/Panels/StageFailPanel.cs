﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class StageFailPanel : MonoBehaviour
{
    [SerializeField] private CanvasGroup magicNums;
    [SerializeField] private ResultWinPanel resultWinPanel;
    [SerializeField] private Button reloadBtn;
    [SerializeField] private Button exitBtn;
    
    public void Show()
    { 
        AudioManagerGame.Instance.PlayFail();
        
        gameObject.SetActive(true);
        magicNums.DOFade(1, 0.5f).OnComplete(() =>
        {
            resultWinPanel.ShowResult();
        });
        
        GetComponent<CanvasGroup>().alpha = 0;
        GetComponent<CanvasGroup>().DOFade(1, 1);
    }
 
    public void Hide()
    {
        GetComponent<CanvasGroup>().DOFade(0, 1).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }

    public void ReloadLevel()
    {
        GameManager.Instance.startGameControlRef.ReloadLvl();
        reloadBtn.interactable = false;
    }

    public void ExitMainMenu()
    {
        GameManager.Instance.startGameControlRef.LoadMainMenu();
        exitBtn.interactable = false;
    }
}
