﻿using System;
using DG.Tweening;
using Lean.Localization;
using TMPro;
using UnityEngine;

public class IsDonePanel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI difficultyText;
    [SerializeField] private GameObject blockRayCast;

    public void Show(string diff)
    { 
        gameObject.SetActive(true);
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(1, 0.5f).SetDelay(0.6f).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            blockRayCast.SetActive(false);
        });
        string diffucalty = LeanLocalization.GetTranslationText("stage.diff.select");
        string selectDiffStr = String.Format(diffucalty, diff);
        difficultyText.text = selectDiffStr;
    }
 
    public void Hide()
    {
        transform.DOScale(0, 0.5f).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }
}
