﻿using System;
using DG.Tweening;
using Lean.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PrepareStart : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI level;
    [SerializeField] private TextMeshProUGUI uniqNum;
    [SerializeField] private TextMeshProUGUI roundTime;
    [SerializeField] private TextMeshProUGUI rowTime;

    [SerializeField] private Button button;
    private DebuffLevel currentDebuff;
  
    
    
    public void Show(DebuffLevel debuff)
    {
      
        currentDebuff = debuff;
        button.interactable = true;

        string levelPr = LeanLocalization.GetTranslationText("pr.level");
        string levelUniq = LeanLocalization.GetTranslationText("pr.uniq");
        string levelUnUniq = LeanLocalization.GetTranslationText("pr.ununiq");
        string levelRound = LeanLocalization.GetTranslationText("pr.round.time");
        string levelRow = LeanLocalization.GetTranslationText("pr.row.time");
        string levelStr = String.Empty;

        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            levelStr = String.Format(levelPr,GameManager.Instance.startGameControlRef.sceneLvl-22);
        }
        else
        {
            levelStr = String.Format(levelPr,GameManager.Instance.startGameControlRef.sceneLvl-1);
        }
        
        string uniqStr = GameManager.Instance.startGameControlRef.uniq ? levelUniq : levelUnUniq;
        string roundStr = String.Format(levelRound, Mathf.RoundToInt(TimeManager.Instance.AmountRoundTimeLeft /  60));
        string rowStr = String.Format(levelRow, TimeManager.Instance.AmountRawTimeLeft);
        
        level.text = levelStr;
        uniqNum.text = uniqStr;
        roundTime.text = roundStr;
        rowTime.text = rowStr;
        gameObject.SetActive(true);
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(1, 0.5f).SetEase(Ease.OutCubic);
    }

    public void Hide()
    {
        AudioManagerGame.Instance.PlayPrepareSound();
        button.interactable = false;
        transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            transform.gameObject.SetActive(false);
            TimeManager.Instance.StopTimer = false;
            GameManager.Instance.goPanel.Show();
            GameManager.Instance.StartDiffucultyDebuff(currentDebuff);
            GameManager.Instance.avatarRef.StartAvatar();
        });
    }
}
