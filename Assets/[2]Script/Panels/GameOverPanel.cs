﻿using System;
using System.Collections;
using DG.Tweening;
using Lean.Localization;
using TMPro;
using UnityEngine;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

public class GameOverPanel : MonoBehaviour
{
    [SerializeField] private CanvasGroup magicNums;
    [SerializeField] private ResultWinPanel resultWinPanel;
    
    [SerializeField] private TMP_InputField inputField;
    [SerializeField] private TextMeshProUGUI rankText;
    [SerializeField] private BlinkButton buttonStart;
    [SerializeField] private Button button;
    [SerializeField] private Image fade;
    private bool isSave;
     
       
    public void Show()
    {
        AudioManagerGame.Instance.PlayFail();

        gameObject.SetActive(true);
        
        rankText.gameObject.SetActive(false);
        buttonStart.gameObject.SetActive(false);
        
        magicNums.DOFade(1, 0.5f).OnComplete(() =>
        {
            resultWinPanel.ShowResult();
        });
        
        GetComponent<CanvasGroup>().alpha = 0;
        GetComponent<CanvasGroup>().DOFade(1, 1);

        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            SaveManager.Instance.InitClassic();
        }
        else
        {
            // int freeRow = GameManager.Instance.turnControl.GetFreeRow();
            // ExpManager.Instance.CurrentRoundExp += ExpManager.Instance.CurrentExperience * freeRow;
            
            GameSettings.Instance.achiveManager.AddLeaderBoard(ExpManager.Instance.CurrentRoundExp);
            SaveManager.Instance.InitDefault();
        }
    }
    
    public void SaveTable()
    {
        isSave = true;
        GameManager.Instance.NameTable = inputField.text;

        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            GameManager.Instance.dataBaseRef.SaveDataClassic(inputField.text);
        }
        else
        {
            GameManager.Instance.dataBaseRef.SaveData(inputField.text);
        }
        
        buttonStart.FadeOn();
        buttonStart.StartBlinkBtn();
        button.interactable = false;
        fade.gameObject.SetActive(true);
 
        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            StartCoroutine(DelayTableClassic());
        }
        else
        {
            StartCoroutine(DelayTable());
        }
    }
    
    IEnumerator DelayTable()
    {
        yield return new WaitForSecondsRealtime(4);

        GameManager.Instance.dataBaseRef.LoadTableData();
        
        yield return new WaitForSecondsRealtime(3);
        rankText.gameObject.SetActive(true);
        buttonStart.gameObject.SetActive(false);

        int rank = GameManager.Instance.controlRankTable.tableMain.GetRank();
        rankText.text = String.Format(LeanLocalization.GetTranslationText("victory.rank"),rank );
        
        yield return new WaitForSecondsRealtime(3);
        Hide();
        GameManager.Instance.controlRankTable.tableMain.Show();
    }
    
    IEnumerator DelayTableClassic()
    {
        yield return new WaitForSecondsRealtime(4);

        GameManager.Instance.dataBaseRef.LoadTableDataClassic();
        
        yield return new WaitForSecondsRealtime(3);
        rankText.gameObject.SetActive(true);
        buttonStart.gameObject.SetActive(false);

        int rank = GameManager.Instance.controlRankTable.tableClassic.GetRank();
        rankText.text = String.Format(LeanLocalization.GetTranslationText("victory.rank"), rank);
        
        yield return new WaitForSecondsRealtime(3);
        Hide();
        GameManager.Instance.controlRankTable.tableClassic.Show();
    }
    
    private void Update()
    {
        if (isSave)
        {
            return;
        }
        
        if (inputField.text == String.Empty)
        {
            button.interactable = false;
            fade.gameObject.SetActive(true);
        }
        else
        {
            button.interactable = true;
            fade.gameObject.SetActive(false);
        }
    }

    public void Hide()
    {
        GetComponent<CanvasGroup>().DOFade(0, 1).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }
    
    public void ExitMainMenu()
    {
        GameManager.Instance.startGameControlRef.LoadMainMenu();
    }
}
