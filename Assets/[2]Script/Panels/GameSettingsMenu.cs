﻿using Coffee.UIExtensions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GameSettingsMenu : MonoBehaviour
{
    private float soundVolume;
    private float musicVolume;
    
    public Slider soundSlider;
    public Slider musicSlider;

    public UIShiny[] shinys;
    
    public void Show()
    {
        AudioManagerGame.Instance.PlayTestSound();
        transform.gameObject.SetActive(true);
        TimeManager.Instance.StopTimer = true;
        ShinyPlay();
        if (soundSlider != null)
        {
            soundVolume = GameSettings.LoadKeySound();
            musicVolume = GameSettings.LoadKeyMusic();

            soundSlider.value = soundVolume;
            musicSlider.value = musicVolume;

            ChangeVolumeSound(soundVolume);
            ChangeVolumeMusic(musicVolume);
        }
        
        transform.GetComponent<CanvasGroup>().alpha = 0;
        transform.GetComponent<CanvasGroup>().DOFade(1, 0.5f);
    }

    public void Hide()
    {
        AudioManagerGame.Instance.PlayTestSound();
        transform.GetComponent<CanvasGroup>().DOFade(0, 0.5f).OnComplete(() =>
        {
            TimeManager.Instance.StopTimer = false;
            transform.gameObject.SetActive(false);
        });
    }

    public void ReloadLvl()
    {
        GameManager.Instance.startGameControlRef.ReloadLvl();
    }

    public void ExitMainMenu()
    {
        Hide();
        GameManager.Instance.startGameControlRef.ExitMainMenu();
    }
    
    public void ChangeVolumeSound(float newVolume)
    {
        soundVolume = newVolume;
        float correctVolumeSound = Mathf.Log10(newVolume) * 20;
        GameSettings.Instance.audioMixer.SetFloat("soundV", correctVolumeSound);
    }

    public void ChangeVolumeMusic(float newVolume)
    {
        musicVolume = newVolume;
        float correctVolumeMusic = Mathf.Log10(newVolume) * 20;
        GameSettings.Instance.audioMixer.SetFloat("musicV", correctVolumeMusic);
    }

    public void ExitSaveVolume()
    {
        GameSettings.SaveSound(soundVolume);
        GameSettings.SaveMusic(musicVolume);
    }

    public void TestSoundPlay()
    {
        AudioManagerGame.Instance.PlayTestSound();
    }

    private void ShinyPlay()
    {
        foreach (UIShiny uiShiny in shinys)
        {
            uiShiny.Play();
        }
    }
}
