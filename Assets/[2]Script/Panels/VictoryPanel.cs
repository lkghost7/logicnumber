﻿using System;
using System.Collections;
using DG.Tweening;
using Lean.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VictoryPanel : MonoBehaviour
{
    [SerializeField] private TMP_InputField inputField;
    [SerializeField] private TextMeshProUGUI rankText;
    [SerializeField] private Button button;
    [SerializeField] private Image fade;
    [SerializeField] private BlinkButton buttonStart;
    private bool isSave;
     
    public void Show()
    {
        AudioManagerGame.Instance.PlayWin();
        
        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            GameSettings.Instance.achiveManager.CheckClassicStatus();
            SaveManager.Instance.InitClassic();
        }
        else
        {
            GameSettings.Instance.achiveManager.ChekVictoryAchieve(GameSettings.Instance.CurrentDifficulty);
            
            int freeRow = GameManager.Instance.turnControl.GetFreeRow();
            ExpManager.Instance.CurrentRoundExp += ExpManager.Instance.CurrentExperience * freeRow;
            
            GameSettings.Instance.achiveManager.AddLeaderBoard(ExpManager.Instance.CurrentRoundExp);
            SaveManager.Instance.InitDefault();
        }
        
        print("victory ");
        gameObject.SetActive(true);

        rankText.gameObject.SetActive(false);
        buttonStart.gameObject.SetActive(false);
        GetComponent<CanvasGroup>().alpha = 0;
        GetComponent<CanvasGroup>().DOFade(1, 1);
    }
 
    public void Hide()
    {
        GetComponent<CanvasGroup>().DOFade(0, 1).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }

    public void SaveTable()
    {
        int freeRow = GameManager.Instance.turnControl.GetFreeRow();
        
        isSave = true;
        GameManager.Instance.NameTable = inputField.text;

        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            GameManager.Instance.dataBaseRef.SaveDataClassic(inputField.text);
            ExpManager.Instance.CurrentRoundExp += ExpManager.Instance.CurrentExperience * freeRow;
        }
        else
        {
            GameManager.Instance.dataBaseRef.SaveData(inputField.text);
            ExpManager.Instance.CurrentRoundExpClassic += ExpManager.Instance.CurrentExperienceClassic * freeRow;
        }
        
        buttonStart.FadeOn();
        buttonStart.StartBlinkBtn();
        button.interactable = false;
        fade.gameObject.SetActive(true);
        
        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            StartCoroutine(DelayTableClassic());
        }
        else
        {
            StartCoroutine(DelayTable());
        }
    }
    
    
  
    IEnumerator DelayTable()
    {
        yield return new WaitForSecondsRealtime(4);

        GameManager.Instance.dataBaseRef.LoadTableData();
        
        yield return new WaitForSecondsRealtime(3);
        rankText.gameObject.SetActive(true);
        buttonStart.gameObject.SetActive(false);

        rankText.text = String.Format(LeanLocalization.GetTranslationText("victory.rank"), GameManager.Instance.controlRankTable.tableMain.GetRank());
        
        yield return new WaitForSecondsRealtime(1);
        Hide();
        GameManager.Instance.controlRankTable.tableMain.Show();
    }
    
    IEnumerator DelayTableClassic()
    {
        yield return new WaitForSecondsRealtime(4);

        GameManager.Instance.dataBaseRef.LoadTableDataClassic();
        
        yield return new WaitForSecondsRealtime(3);
        rankText.gameObject.SetActive(true);
        buttonStart.gameObject.SetActive(false);

        rankText.text = String.Format(LeanLocalization.GetTranslationText("victory.rank"), GameManager.Instance.controlRankTable.tableClassic.GetRank());
        
        yield return new WaitForSecondsRealtime(3);
        Hide();
        GameManager.Instance.controlRankTable.tableClassic.Show();
    }

    private void Update()
    {
        if (isSave)
        {
            return;
        }
        
        if (inputField.text == String.Empty)
        {
            button.interactable = false;
            fade.gameObject.SetActive(true);
        }
        else
        {
            button.interactable = true;
            fade.gameObject.SetActive(false);
        }
    }
}
