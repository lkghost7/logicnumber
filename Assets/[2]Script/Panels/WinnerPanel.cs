﻿using System;
using System.Collections;
using Coffee.UIExtensions;
using DG.Tweening;
using Lean.Pool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
 
public class WinnerPanel : MonoBehaviour
{
    [SerializeField] private Transform parentStats;
    [SerializeField] private Transform parentLvlUp;

    [Header("current text bonus")] [SerializeField]
    private TextMeshProUGUI currentTimeWand;
 
    [SerializeField] private TextMeshProUGUI currentNumWand;
    [SerializeField] private TextMeshProUGUI currentRawWand;
    [SerializeField] private TextMeshProUGUI currentRoundWand;

    [Header("Shiny")] [SerializeField] private UIShiny shinyTimeWand;
    [SerializeField] private UIShiny shinyNumWand;
    [SerializeField] private UIShiny shinyRawWand;
    [SerializeField] private UIShiny shinyRoundWand;

    [Header("Transform")] [SerializeField] private Transform shinyTimeWandTransform;
    [SerializeField] private Transform shinyNumWandTransform;
    [SerializeField] private Transform shinyRawWandTransform;
    [SerializeField] private Transform shinyRoundWandTransform;

    [Header("Button")] [SerializeField] private Button shinyTimeWandButton;
    [SerializeField] private Button shinyNumWandButton;
    [SerializeField] private Button shinyRawWandButton;
    [SerializeField] private Button shinyRoundButton;

    [SerializeField] private Button next;
    [SerializeField] private Button home;

    [SerializeField] private GameObject blockRaycast;

    [Header("Win score")]
    
    [SerializeField] private TextMeshProUGUI allScore;
    [SerializeField] private TextMeshProUGUI crossScore;
    [SerializeField] private TextMeshProUGUI checkmarkScore;
    [SerializeField] private TextMeshProUGUI bonusScore;
    [SerializeField] private TextMeshProUGUI roundBonusScore;
    [SerializeField] private TextMeshProUGUI timeBonusScore;
    [SerializeField] private TextMeshProUGUI rowScore;
    [SerializeField] private CanvasGroup magicNums;

    [SerializeField] private ResultWinPanel resultWinPanel;
    [Header("+ Bonus")]
    [SerializeField] private BonusExp plusBonus;
    
    [SerializeField] private Button reloadBtn;
    [SerializeField] private Button exitBtn;
    
    private int freeRow;
    private int expCurrent;

    private void InitScoreStart()
    {
        allScore.text = "0";
        crossScore.text = "0";
        checkmarkScore.text = "0";
        bonusScore.text = "0";
        roundBonusScore.text = "0";
        timeBonusScore.text = "0";
        rowScore.text = "0";
    }

    IEnumerator ShowScore()
    {
        AudioManagerGame.Instance.PlayWin();
        CountScore();

        magicNums.DOFade(1, 0.5f).OnComplete(() => { resultWinPanel.ShowResult(); });

        yield return new WaitForSecondsRealtime(0.3f);
        crossScore.text = "(" + ExpManager.Instance.countExperienceX + ")" + "  " + ExpManager.Instance.CurrentExperienceX;
        EffectResult(crossScore.transform);
        yield return new WaitForSecondsRealtime(0.22f);
        checkmarkScore.text = "(" + ExpManager.Instance.countExperienceY + ")" + "  " + ExpManager.Instance.CurrentExperienceY;
        EffectResult(checkmarkScore.transform);
        yield return new WaitForSecondsRealtime(0.22f);
        bonusScore.text = "(" + ExpManager.Instance.countExperienceBonus + ")" + "  " + ExpManager.Instance.CurrentExperienceBonus;
        EffectResult(bonusScore.transform);
        yield return new WaitForSecondsRealtime(0.22f);
        roundBonusScore.text = ExpManager.Instance.CurrentExperienceRoundBonus.ToString();
        EffectResult(roundBonusScore.transform);
        yield return new WaitForSecondsRealtime(0.22f);
        timeBonusScore.text = ExpManager.Instance.CurrentExperienceTimeRound.ToString();
        EffectResult(timeBonusScore.transform);
        yield return new WaitForSecondsRealtime(0.22f);
        rowScore.text = freeRow + " * " + ExpManager.Instance.CurrentExperience; // свободные ряды умножить на весь опыт уровня
        print("freeRow2" + freeRow);
        EffectResult(rowScore.transform);
        yield return new WaitForSecondsRealtime(0.4f);
        expCurrent = ExpManager.Instance.CurrentRoundExp += ExpManager.Instance.CurrentExperience * freeRow;
        allScore.text = expCurrent.ToString();
        EffectResultAllWin(allScore.transform);
        ScoreAchieve();
        GameSettings.Instance.achiveManager.ChekRowNumAchieve(GameManager.Instance.turnControl.rawNumber, GameManager.Instance.turnControl.numLimit);
        
        yield return new WaitForSecondsRealtime(0.5f);

        GameManager.Instance.UnBlockRayCast();
        ControlLife();

        SaveManager.Instance.SaveRound("CurrentRoundExp", ExpManager.Instance.CurrentRoundExp);
    }

    private void ControlLife()
    {
        switch (ExpManager.Instance.ControlLife)
        {
            case 0:
                if (ExpManager.Instance.CurrentRoundExp >= 70000)
                {
                    GameManager.Instance.AddLifeAction.Invoke(0);
                    SaveManager.Instance.SaveRound("ControlLife", 1);
                    ExpManager.Instance.ControlLife++;
                }

                break;

            case 1:
                if (ExpManager.Instance.CurrentRoundExp >= 150000)
                {
                    GameManager.Instance.AddLifeAction.Invoke(0);
                    SaveManager.Instance.SaveRound("ControlLife", 2);
                    ExpManager.Instance.ControlLife++;
                }

                break;

            case 2:
                if (ExpManager.Instance.CurrentRoundExp >= 250000)
                {
                    GameManager.Instance.AddLifeAction.Invoke(0);
                    SaveManager.Instance.SaveRound("ControlLife", 3);
                    ExpManager.Instance.ControlLife++;
                }

                break;

            case 3:
                if (ExpManager.Instance.CurrentRoundExp >= 350000)
                {
                    GameManager.Instance.AddLifeAction.Invoke(0);
                    SaveManager.Instance.SaveRound("ControlLife", 4);
                    ExpManager.Instance.ControlLife++;
                }

                break;
        }
        SaveManager.Instance.SaveLife(GameManager.Instance.CurrentLife);
    }

    private void EffectResult(Transform numPos)
    {
        GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.winEffectNum, numPos.position, Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
    }

    private void EffectResultAllWin(Transform numPos)
    {
        GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.winEffectNumAll, numPos.position, Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
    }

    public void ShowStatsPanel()
    {
        InitScoreStart();
        magicNums.alpha = 0;
        allScore.text = ExpManager.Instance.CurrentRoundExp.ToString(); // вначале показть весь опыт без прибавки
 
        blockRaycast.SetActive(false);
        GameSettings.Instance.SaveLevel(GameManager.Instance.startGameControlRef.sceneLvl);
        SaveManager.Instance.SaveRank(GameManager.Instance.startGameControlRef.sceneLvl - 2, GameManager.Instance.CurrenLevelRank);
        SaveManager.Instance.SaveBonus(GameManager.Instance.CurrentBonus);
        SaveManager.Instance.SaveLife(GameManager.Instance.CurrentLife);
        gameObject.SetActive(true);

        parentLvlUp.gameObject.SetActive(false);
        parentStats.localScale = new Vector3(1, 0, 1);
        parentStats.DOScale(1, 1f).SetDelay(1f).SetEase(Ease.OutBack).OnComplete(() =>
        {
            StartCoroutine(ShowScore());
        });
    }
 
    private void CountScore()
    {
        freeRow = GameManager.Instance.turnControl.GetFreeRow();
        if (TimeManager.Instance.IsRoundTimeStart)
        {
            plusBonus.Show();

            GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.burstEffect2, plusBonus.transform.position,Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
            
            ExpManager.Instance.CurrentExperience += ExpManager.Instance.AddExpToScoreBonusTime();
            ExpManager.Instance.CurrentExperienceTimeRound += ExpManager.Instance.AddExpToScoreBonusTime();
        }
    }

    public void ScoreAchieve()
    {
        if (ExpManager.Instance.CurrentRoundExp > 20000)
        {
            print("20");
            GameSettings.Instance.achiveManager.AchieveAction.Invoke(GameSettings.Instance.achiveManager.Score20);
        }
        
        if (ExpManager.Instance.CurrentRoundExp > 50000)
        {
            GameSettings.Instance.achiveManager.AchieveAction.Invoke(GameSettings.Instance.achiveManager.Score50);
            print("50");
        }
        
        if (ExpManager.Instance.CurrentRoundExp > 80000)
        {
            GameSettings.Instance.achiveManager.AchieveAction.Invoke(GameSettings.Instance.achiveManager.Score80);
            print("80");
        }
        
        if (ExpManager.Instance.CurrentRoundExp > 120000)
        {
            GameSettings.Instance.achiveManager.AchieveAction.Invoke(GameSettings.Instance.achiveManager.Score120);
            print("120");
        }
        
        if (ExpManager.Instance.CurrentRoundExp > 150000)
        {
            GameSettings.Instance.achiveManager.AchieveAction.Invoke(GameSettings.Instance.achiveManager.Score150);
            print("150");
        }
    }

    public void BlockRayCastOn()
    {
        blockRaycast.SetActive(true);
    }

    public void ShowLevelUpPanel()
    {
        parentLvlUp.gameObject.SetActive(true);
        parentLvlUp.localScale = new Vector3(0, 0, 0);
        parentLvlUp.DOScale(1, 0.5f).SetDelay(0.2f).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            GameManager.Instance.UnBlockRayCast();
        });
        ShowLevelUpWandAmount();
    }

    public void ShowLevelUpWandAmount()
    {
        string value = "({0})";

        currentTimeWand.text = String.Format(value, ExpManager.Instance.WandCountToLevelTime);
        currentNumWand.text = String.Format(value, ExpManager.Instance.WandCountToLevelNum);
        currentRawWand.text = String.Format(value, ExpManager.Instance.WandCountToLevelRaw);
        currentRoundWand.text = String.Format(value, ExpManager.Instance.WandCountToLevelRoundBonus);
    }

    public void HideLevelUpPanel(float delay)
    {
        parentLvlUp.DOScale(0, 0.5f).SetDelay(delay).SetEase(Ease.InBack).OnComplete(() =>
        {
            parentLvlUp.gameObject.SetActive(false);
            blockRaycast.SetActive(false);
        });
    }

    IEnumerator DelayEffect(Transform iconTransform, Button button)
    {
        yield return new WaitForSeconds(0.3f);
        GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.bonusUp, iconTransform.position, Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));

        yield return new WaitForSeconds(1f);
        button.interactable = true;
    }

    public void NextLevel()
    {
        GameManager.Instance.startGameControlRef.LoadNextLvl();
        reloadBtn.interactable = false;
    }

    public void MainMenu()
    {
        GameManager.Instance.startGameControlRef.ExitMainMenu();
        exitBtn.interactable = false;
    }
}