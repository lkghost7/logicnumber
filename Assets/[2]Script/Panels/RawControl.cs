﻿using UnityEngine;

public class RawControl : MonoBehaviour
{ 
    public bool isFrozenRaw;
    public bool turnComplete;

    public void Freeze()
    {
        GetComponent<CanvasGroup>().alpha = 0.2f;
    }

    public void UnFreeze()
    {
        GetComponent<CanvasGroup>().alpha = 1;
    }
    
    public void TurnComplete()
    {
        turnComplete = true;
    }
}