﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Localization;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class Avatar : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI avatarText;
    private List<string> avatarsList = new List<string>();
    public bool IsDone { get; set; } = false;

    private void Start()
    {
        Init();
    }

    public void StartAvatar()
    {
        StartCoroutine(ShowContentText());
    }

    IEnumerator ShowContentText()
    {
        if (IsDone || !GameSettings.Instance.isAvatarOn)
        {
            yield return null;
        }
        else
        {
            yield return new WaitForSecondsRealtime(5f);
            avatarText.text = String.Empty;
            avatarText.GetComponent<CanvasGroup>().alpha = 1;
            int randomNum = Random.Range(0, 47);
            string sayText = avatarsList[randomNum];
            avatarText.text = sayText;
            avatarText.ForceMeshUpdate();

            int totalVisibleCharacters = avatarText.textInfo.characterCount; 
            int counter = 0;
            int visibleCount = 0;

            while (true)
            {
                visibleCount = counter % (totalVisibleCharacters + 1);
                avatarText.maxVisibleCharacters = visibleCount; 
                if (visibleCount >= totalVisibleCharacters)
                {
                    break;
                }

                counter += 1;
                yield return new WaitForSecondsRealtime(0.02f);
            }
        
            yield return new WaitForSecondsRealtime(20f);
            avatarText.GetComponent<CanvasGroup>().DOFade(0,1).OnComplete(() =>
            {
                StartCoroutine(ShowContentText());
            });
        }
 
    }

    private void Init()
    {
        for (int i = 0; i < 87; i++)
        {
            string sufix = "avatar.say";
            string prefix = i.ToString();
            string say = sufix + prefix;
            string localization = LeanLocalization.GetTranslationText(say);
            avatarsList.Add(localization);
        }
    }
}
