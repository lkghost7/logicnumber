﻿using System.Collections;
using Lean.Pool;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour
{
    [SerializeField] private Transform lifeBarTransform;
    [SerializeField] private Image lifeIcon;

    public void InitRound(int lifeRound)
    {
        for (int i = 0; i < lifeRound; i++)
        {
            LeanPool.Spawn(lifeIcon, lifeBarTransform);
        }
        
        GameManager.Instance.LostLifeAction += LostLife;
        GameManager.Instance.LostLifeActionClassic += LostLifeClassic;
        GameManager.Instance.AddLifeAction += AddLife;
    }

    private void LostLife(int life)
    {
        print("LostLife");
        int child = lifeBarTransform.childCount;

        if (child > 1)
        {
            Transform lifeIcon = lifeBarTransform.GetChild(0);
            GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.addHeart, lifeIcon.transform.position, Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
            Destroy(lifeIcon.gameObject);
            GameManager.Instance.CurrentLife--;
            // SaveManager.Instance.SaveLife(GameManager.Instance.CurrentLife);
        }
        else
        {
            print(" гейм овер");

            if (lifeBarTransform.childCount >0)
            {
                Transform lifeIcon = lifeBarTransform.GetChild(0);
                GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.addHeart, lifeIcon.transform.position, Quaternion.identity);
                StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
            
                Destroy(lifeIcon.gameObject);
                GameManager.Instance.CurrentLife--;
                // SaveManager.Instance.SaveLife(GameManager.Instance.CurrentLife);
            }
            else
            {
                Debug.LogError("lifeIcon null");
            }

            // StartCoroutine(DelayLifeEffect());
        }
    }
    
    private void LostLifeClassic(int life)
    {
        print("LostLifeClassic");
        int child = lifeBarTransform.childCount;

        if (child > 1)
        {
            Transform lifeIcon = lifeBarTransform.GetChild(0);
            GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.addHeart, lifeIcon.transform.position, Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
            Destroy(lifeIcon.gameObject);
            GameManager.Instance.CurrentLifeClassic--;
            // SaveManager.Instance.SaveLife(GameManager.Instance.CurrentLife);
        }
        else
        {
            print(" гейм овер");

            if (lifeBarTransform.childCount >0)
            {
                Transform lifeIcon = lifeBarTransform.GetChild(0);
                GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.addHeart, lifeIcon.transform.position, Quaternion.identity);
                StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
            
                Destroy(lifeIcon.gameObject);
                GameManager.Instance.CurrentLifeClassic--;
                // SaveManager.Instance.SaveLife(GameManager.Instance.CurrentLife);
            }
            else
            {
                Debug.LogError("lifeIcon null");
            }

            // StartCoroutine(DelayLifeEffect());
        }
    }

    // IEnumerator DelayLifeEffect()
    // {
    //     yield return new WaitForSecondsRealtime(1);
    //     GameManager.Instance.GameOverPanel.Show();
    // }

    public void AddLife(int life)
    {
        print("AddLife");
        
        int child = lifeBarTransform.childCount;
        if (child > GameManager.Instance.MaxLife-1)
        {
            print(" no add life");
        }
        else
        {
            Image lifePosition = LeanPool.Spawn(lifeIcon, lifeBarTransform);

            GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.addHeart, lifePosition.transform.position, Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
            
            GameManager.Instance.CurrentLife ++;
            // SaveManager.Instance.SaveLife(GameManager.Instance.CurrentLife);
        }
    }
}
