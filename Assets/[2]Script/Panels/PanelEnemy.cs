﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using UnityEngine;
using UnityEngine.UI;

public class PanelEnemy : MonoBehaviour
{
    [SerializeField] private List<EnemyRowControl> enemyRowControls;
    
    public void ControlCheck(int num, int mark, int cross)
    {
        enemyRowControls[num].Show(mark, cross);
    }
}
