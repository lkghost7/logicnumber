﻿using System.Collections;
using Lean.Pool;
using TMPro;
using UnityEngine;

public class ResultWinPanel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI[] result;

    public void ShowResult()
    {
        StartCoroutine(DelayNum());
    }
   
    IEnumerator DelayNum()
    {
        for (int i = 0; i < result.Length; i++)
        {
            result[i].text = GameManager.Instance.turnControl.magicNumber[i].ToString();
            GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.numEffectView, result[i].transform.position, Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
            yield return new WaitForSecondsRealtime(0.2f);
        }
    }
}
