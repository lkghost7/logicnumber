﻿using Lean.Pool;
using UnityEngine;
using UnityEngine.UI;

public class BonusBar : MonoBehaviour
{ 
    [SerializeField] private Transform bonusBarTransform;
    [SerializeField] private Image bonusIcon;

    public void InitRound(int lifeRound)
    {
        for (int i = 0; i < lifeRound; i++)
        {
            LeanPool.Spawn(bonusIcon, bonusBarTransform);
        }
        
        GameManager.Instance.LostBonusAction += LostBonus;
        GameManager.Instance.AddBonusAction += AddBonus;
    }

    public void LostBonus(int life)
    {
        int child = bonusBarTransform.childCount;

        if (child <= 0)
        {
            return;
        }
        Transform lifeIcon = bonusBarTransform.GetChild(0);
        GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.burstEffect, lifeIcon.transform.position, Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
        Destroy(lifeIcon.gameObject);
        GameManager.Instance.CurrentBonus--;
       
    }

    private void AddBonus(int life)
    {
        int child = bonusBarTransform.childCount;
        if (child >= 3)
        {
        }
        else
        {
            Image lifePosition = LeanPool.Spawn(bonusIcon, bonusBarTransform);

            GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.burstEffect, lifePosition.transform.position, Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
            
            GameManager.Instance.CurrentBonus ++;
        }
    }
}