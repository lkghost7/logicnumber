﻿using System.Collections;
using DG.Tweening;
using Lean.Pool;
using TMPro;
using UnityEngine;

public class WinnerPanelClassic : MonoBehaviour
{
    // [SerializeField] private GameObject blockRaycast;
    // [SerializeField] private GameObject fadePanel;
    [SerializeField] private ResultWinPanel resultWinPanel;
    
    [SerializeField] private TextMeshProUGUI allScore;
    [SerializeField] private TextMeshProUGUI crossScore;
    [SerializeField] private TextMeshProUGUI checkmarkScore;
    [SerializeField] private TextMeshProUGUI timeBonusScore;
    [SerializeField] private TextMeshProUGUI timeLeft;
    [SerializeField] private TextMeshProUGUI rowScore;
    [SerializeField] private CanvasGroup magicNums;

    private int freeRow;
    private int expCurrent;

    public void ShowWinnClassic()
    {
        AudioManagerGame.Instance.PlayWin();
        gameObject.SetActive(true);
        
        // blockRaycast.SetActive(false);
        // fadePanel.SetActive(false);
        magicNums.alpha = 0;
        InitScoreStart();
        CountScore();
        
        GameSettings.Instance.SaveLevelClassic(GameManager.Instance.startGameControlRef.sceneLvl + 1);
        SaveManager.Instance.SaveRankClassic(GameManager.Instance.startGameControlRef.sceneLvl - 23,
            GameManager.Instance.CurrenLevelRankClassic);
        SaveManager.Instance.SaveLifeClassic(GameManager.Instance.CurrentLifeClassic);
    }

    private void CountScore()
    {
        freeRow = GameManager.Instance.turnControl.GetFreeRow();
        // expCurrent = ExpManager.Instance.CurrentRoundExpClassic += ExpManager.Instance.CurrentExperienceClassic * freeRow;
        
        // allScore.text = expCurrent.ToString();
    

        StartCoroutine(ShowScore());
    }
    
    private void InitScoreStart()
    {
        allScore.text = "0";
        crossScore.text = "0";
        checkmarkScore.text = "0";
        timeBonusScore.text = "0";
        rowScore.text = "0";
    }
     
     IEnumerator ShowScore()
    {
        allScore.text = ExpManager.Instance.CurrentRoundExpClassic.ToString(); // вначале показть весь опыт без прибавки
        magicNums.DOFade(1, 0.5f).OnComplete(() => { resultWinPanel.ShowResult(); });

        yield return new WaitForSecondsRealtime(0.3f);
        crossScore.text = ExpManager.Instance.countExperienceX + "*" + 15;
        EffectResult(crossScore.transform);
        yield return new WaitForSecondsRealtime(0.22f);
        checkmarkScore.text = ExpManager.Instance.countExperienceY + "*" + 30;
        EffectResult(checkmarkScore.transform);
        yield return new WaitForSecondsRealtime(0.22f);
        timeBonusScore.text = TimeManager.Instance.countRoundLeft.ToString(); // 2000
        EffectResult(timeBonusScore.transform);
        timeLeft.text = TimeManager.Instance.countTimeLeft + "*"+ 350;
        EffectResult(timeLeft.transform);
        yield return new WaitForSecondsRealtime(0.22f);
        rowScore.text = freeRow + " * " + ExpManager.Instance.CurrentExperienceClassic; // свободные ряды умножить на весь опыт уровня
        EffectResult(rowScore.transform);
        yield return new WaitForSecondsRealtime(0.3f);
        expCurrent = ExpManager.Instance.CurrentRoundExpClassic += ExpManager.Instance.CurrentExperienceClassic * freeRow;
        allScore.text = expCurrent.ToString();
        EffectResultAllWin(allScore.transform);
        
        yield return new WaitForSecondsRealtime(0.3f);

        GameManager.Instance.UnBlockRayCast();
        SaveManager.Instance.SaveRound("CurrentRoundExpClassic", ExpManager.Instance.CurrentRoundExpClassic);
    }
     
     private void EffectResult(Transform numPos)
     {
         GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.winEffectNum, numPos.position, Quaternion.identity);
         StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
     }
     
     private void EffectResultAllWin(Transform numPos)
     {
         GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.winEffectNumAll, numPos.position, Quaternion.identity);
         StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
     }

    public void NextLevel()
    {
        print("NextLevel");
        GameManager.Instance.startGameControlRef.LoadNextLvl();
    }

    public void MainMenu()
    {
        GameManager.Instance.startGameControlRef.ExitMainMenu();
    }
}