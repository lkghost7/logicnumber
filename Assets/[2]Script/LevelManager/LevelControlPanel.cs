﻿using System;
using System.Collections.Generic;
using System.IO;
using Lean.Pool;
using TMPro;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
 
public class LevelControlPanel : MonoBehaviour
{
    [SerializeField] private DifficultyLevelPanel difficultyLevelPanel;
    // [SerializeField] private LevelRankUi[] levelRankUis;
    [SerializeField] private TextMeshProUGUI diffDescr;
    [SerializeField] private LevelRankUi prefabRank;
    [SerializeField] private Transform parentRank;
    
    public void Show()
    {
        UpdateDiff();
        GameManager.ClearParent(parentRank);
        GameSettings.Instance.LoadLevel();
        SaveManager.Instance.LoadAllRank();
   
        
        for (int i = 0; i < 20; i++)
        {
            LevelRankUi levelRank = LeanPool.Spawn(prefabRank, parentRank);
            levelRank.Init(i+1);
            // levelRankUis[i].Init(i+1);
        }
    }
    
    public void ShowDifficulty()
    {
        int isActiveDifficulty = LoadingScene.Instance.CurrenDifficulty;
        if (isActiveDifficulty == 0)
        {
            difficultyLevelPanel.gameObject.SetActive(true);
            difficultyLevelPanel.Show();
        }
    }

    public void ShowNewDifficulty()
    {
        ShowDifficulty();
        Show();
    }
    
    public void UpdateDiff()
    {
        diffDescr.text = GameSettings.Instance.CurrentDifficulty.ToString();
    }
}

