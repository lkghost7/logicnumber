﻿using System;
using Coffee.UIExtensions;
using DG.Tweening;
using UnityEngine;

public class DifficultyLevelPanel : MonoBehaviour
{
    [SerializeField] private IsDonePanel isDonePanel;
    [SerializeField] private UIShiny[] shinyButtons;
    [SerializeField] private LevelControlPanel levelControlPanel;
    [SerializeField] private GameObject blockRayCast;
    
    public void Show()
    {
        transform.gameObject.SetActive(true);
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(1, 0.5f).SetEase(Ease.OutCubic);
    }
 
    public void Hide()
    {
        transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            transform.gameObject.SetActive(false);
        });
    }

    public void PressShinyBtn(int numBtn)
    {
        shinyButtons[numBtn].Play();
    }

    public void ShowDifficulty(int difficultyEnum)
    {
        blockRayCast.SetActive(true);
        string difficultyStr = String.Empty;
        switch (difficultyEnum)
        {
            case 0:
                difficultyStr = "EASY";
                GameSettings.Instance.CurrentDifficulty = DifficultyEnum.EASY;
                break;
            case 1:
                difficultyStr = "NORMAL";
                GameSettings.Instance.CurrentDifficulty = DifficultyEnum.NORMAL;
                break;
            case 2:
                difficultyStr = "HARD";
                GameSettings.Instance.CurrentDifficulty = DifficultyEnum.HARD;
                break;
        }
        
        isDonePanel.Show(difficultyStr);
    }
 
    public void IsDoneDifficulty()
    {
        // SaveManager.Instance.SaveDifficulty();
        LoadingScene.Instance.SaveDifficultyLoading(); // установленна сложность, устноавлена ли она вообще как опция
        GameSettings.Instance.SaveOptionsPreferences(GameSettings.Instance.CurrentDifficulty); // сохранить текущую сложность 
        levelControlPanel.UpdateDiff(); // обновить текст
        GameManager.Instance.InitLifeFirstStartGame();
        Hide();
    }
}
