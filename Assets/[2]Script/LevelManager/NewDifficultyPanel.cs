﻿using DG.Tweening;
using UnityEngine;

public class NewDifficultyPanel : MonoBehaviour
{
    [SerializeField] private LevelControlPanel levelControlPanel;
    [SerializeField] private LevelClassicPanel levelControlClassic;
    
    public void Show()
    {
        transform.gameObject.SetActive(true);
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(1, 0.5f).SetEase(Ease.OutCubic);
    }
 
    public void Hide()
    {
        transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            transform.gameObject.SetActive(false);
        });
    }

    public void Accept()
    {
        SaveManager.Instance.InitDefault();
        transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            levelControlPanel.ShowNewDifficulty();
            transform.gameObject.SetActive(false);
        });
    }
    
    public void AcceptClassic()
    {
        SaveManager.Instance.InitClassic();
        transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            levelControlClassic.Show();
            transform.gameObject.SetActive(false);
        });
    }
}
