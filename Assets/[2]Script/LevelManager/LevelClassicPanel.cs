﻿using Lean.Pool;
using UnityEngine;

public class LevelClassicPanel : MonoBehaviour
{
    [SerializeField] private LevelRankClassicUi prefabRank;
    [SerializeField] private Transform parentRank;
    
    public void Show()
    {
        GameManager.ClearParent(parentRank);
        GameSettings.Instance.LoadLevelClassic();
        SaveManager.Instance.LoadAllRankClassic();
        
        for (int i = 0; i < 10; i++)
        {
            LevelRankClassicUi levelRank = LeanPool.Spawn(prefabRank, parentRank);
            levelRank.Init(i+23);
        }
    }
}
