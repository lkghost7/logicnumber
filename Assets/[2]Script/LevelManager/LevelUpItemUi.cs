﻿using System;
using System.Collections;
using Coffee.UIExtensions;
using Lean.Pool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
 
public class LevelUpItemUi : MonoBehaviour
{ 
    public TextMeshProUGUI description;
    public TextMeshProUGUI percent;
    public TextMeshProUGUI chanceText;
    public Image icon;
    public Button button;
    [SerializeField] private UIShiny shiny;
    [SerializeField] private TextMeshProUGUI descriptionQuestion;
    [SerializeField] private bool isCurrentItemBuf;
    
    private int currentChance;
    
    public void Init(string description, string iconName, int chance, string descript)
    {
        switch (GameSettings.Instance.CurrentDifficulty)
        {
            case DifficultyEnum.EASY:
                currentChance = 5;
                break;
            case DifficultyEnum.NORMAL:
                currentChance = 4;
                break;
            case DifficultyEnum.HARD:
                currentChance = 3;
                break;
        }
        
        this.description.text = description;
        icon.sprite = Resources.Load<Sprite>("BufItem/" + iconName);
        ShowPercent(chance);
        descriptionQuestion.text = descript;

        if (isCurrentItemBuf)
        {
            chanceText.text =  "+" + currentChance + "%";
        }
        else
        {
            chanceText.text =  "-" + currentChance + "%";
        }

        // if (iconName == "Life")
        // {
        //     chanceText.text = "+3%";
        // }
    }

    public void ShowPercent(int chance)
    {
        string value = "({0})";
        percent.text = String.Format(value, chance);
    }
    
    public void ShowEffect(int chance)
    {
        AudioManagerGame.Instance.PlayAchive2();
        shiny.Play();
        ShowPercent(chance);
        StartCoroutine(DelayEffect(icon.transform));
    }
    
    IEnumerator DelayEffect(Transform iconTransform)
    {
        yield return new WaitForSeconds(0.3f);
        GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.RawEffect, iconTransform.position, Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
    }
}
