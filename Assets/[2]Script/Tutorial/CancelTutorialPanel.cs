﻿using DG.Tweening;
using UnityEngine;

public class CancelTutorialPanel : MonoBehaviour
{
    public void Show()
    {
        gameObject.SetActive(true);
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(1, 0.5f).SetDelay(0.6f).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            
        });
    }

    public void Hide()
    {
        transform.DOScale(0, 0.5f).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }

    public void AcceptCancel()
    {
        transform.DOScale(0, 0.5f).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            gameObject.SetActive(false);
            TutorialManager.Instance.TutorialIsDone();
            TutorialManager.Instance.LoadFirstLevel();
        });
    }
}