﻿using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
public class TutorControl : MonoBehaviour
{
    [SerializeField] private CanvasGroup[] fadePanels;
    [SerializeField] private CanvasGroup emptyPanel;
    [SerializeField] private CanvasGroup panelDescription;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private CanvasGroup flashWindow;

    private float delayFade = 1f;
    [SerializeField][Tooltip("номер следущего акта")] private int actNum;

    public void Show(string descriptionAct)
    {
        panelDescription.gameObject.SetActive(false);
        transform.gameObject.SetActive(true);
        description.text = descriptionAct;
        flashWindow.gameObject.SetActive(true);
        flashWindow.alpha = 0;
        
        foreach (CanvasGroup panel in fadePanels)
        {
            panel.alpha = 0;
            panel.DOFade(0.85f, delayFade);
        }

        flashWindow.DOFade(1, delayFade);
        
        emptyPanel.DOFade(0.85f, delayFade).OnComplete(() =>
        {
            panelDescription.gameObject.SetActive(true);
            panelDescription.transform.localScale = new Vector3(0, 0, 0);
            panelDescription.transform.DOScale(1, 0.5f).SetEase(Ease.OutCubic);
            LevelAction(actNum);
        });
          
    }
    public void HideAndNext()
    {
        print("HideAndNext()");
        TutorialManager.Instance.bonusRouletePanel.DOFade(0, 0.5f);
        panelDescription.transform.DOScale(0, 0.5f).SetEase(Ease.OutCubic).OnComplete(() =>
        {
            panelDescription.gameObject.SetActive(false);
            NextStep1();
            print("NextStep1();");
        });
    }

    private void NextStep1()
    {
        foreach (CanvasGroup panel in fadePanels)
        {
            panel.DOFade(0f, delayFade);
        }
        
        flashWindow.DOFade(0, delayFade);
        
        print("emptyPanel");
        emptyPanel.DOFade(0, delayFade).OnComplete(() =>
        {
            TutorialManager.Instance.ShowNextAct(actNum);
            transform.gameObject.SetActive(false);
        });
    }

    private void LevelAction(int actNum)
    {
        // if (actNum == 6)
        // {
        //     StartCoroutine(Act6Demo());
        // }
        
        if (actNum == 5)
        {
            StartCoroutine(Act5Demo());
        }
        
        if (actNum == 9)
        {
            StartCoroutine(Act9Demo());
        }
        
        if (actNum == 10)
        {
            StartCoroutine(Act10Demo());
        }
        
        if (actNum == 11)
        {
            StartCoroutine(Act11Demo());
        }
        
        if (actNum == 12)
        {
            StartCoroutine(Act12Demo());
        }
        
        if (actNum == 14)
        {
            StartCoroutine(Act14Demo());
        }
        
        if (actNum == 15)
        {
            StartCoroutine(Act15Demo());
        }
        
        if (actNum == 16)
        {
            StartCoroutine(Act16Demo());
        }
        
        if (actNum == 17)
        {
            StartCoroutine(Act17Demo());
        }
        
        // if (actNum == 19)
        // {
        //     StartCoroutine(Act19Demo());
        // }
        //
        if (actNum == 20)
        {
            StartCoroutine(Act20Demo());
        }
        
        if (actNum == 21)
        {
            StartCoroutine(Act21Demo());
        }
        if (actNum == 22)
        {
            StartCoroutine(Act22Demo());
        }
        
        if (actNum == 23)
        {
            StartCoroutine(Act23Demo());
        }
        
        if (actNum == 24)
        {
            StartCoroutine(Act24Demo());
        }
        
    }
    
    IEnumerator Act5Demo()
    {
        yield return new WaitForSecondsRealtime(0.2f);
        TutorialManager.Instance.buttonTurnEnd.alpha = 0;
    }
    
    IEnumerator Act9Demo()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        GameManager.Instance.AddBonusAction.Invoke(0);
        yield return new WaitForSecondsRealtime(0.15f);
        GameManager.Instance.AddBonusAction.Invoke(0);
        yield return new WaitForSecondsRealtime(0.15f);
        GameManager.Instance.AddBonusAction.Invoke(0);
    }
    
    IEnumerator Act10Demo()
    {
        yield return new WaitForSecondsRealtime(0.2f);
        GameManager.Instance.timeBonusRoulette.shinyButtonRoulette.ButtonAcceptEnable();
    }
    
    IEnumerator Act11Demo()
    {
        TutorialManager.Instance.bonusRouletePanel.alpha = 1;
        yield return new WaitForSecondsRealtime(0f);
        BtnClick.Instance.StartRoulette();
    }
    
    IEnumerator Act12Demo()
    {
        yield return new WaitForSecondsRealtime(1f);
        BonusManager.Instance.UnBlockClearBtn(0);
        yield return new WaitForSecondsRealtime(0.15f);
        BonusManager.Instance.UnBlockCellBtn(0);
        yield return new WaitForSecondsRealtime(0.15f);
        BonusManager.Instance.UnBlockRawBtn(0);
        yield return new WaitForSecondsRealtime(0.15f);
        BonusManager.Instance.UnBlockMagicBtn(0);
    }
    
    IEnumerator Act14Demo()
    {
        yield return new WaitForSecondsRealtime(1f);
        ExpManager.Instance.AddRawExp();
    }
    
    IEnumerator Act15Demo()
    {
        yield return new WaitForSecondsRealtime(1f);
        ExpManager.Instance.AddNumExp(6);
    }
    
    IEnumerator Act16Demo()
    {
        yield return new WaitForSecondsRealtime(1f);
        ExpManager.Instance.AddTimeExp();
    }
    
    IEnumerator Act17Demo()
    {
        yield return new WaitForSecondsRealtime(1f);
        ExpManager.Instance.AddLevelExp(24);
    }
    
    // IEnumerator Act19Demo()
    // {
    //     yield return new WaitForSecondsRealtime(0f);
    //     // TutorialManager.Instance.winnerPanel.gameObject.SetActive(true);
    //     TutorialManager.Instance.winnerPanel.alpha = 0;
    //     TutorialManager.Instance.winnerPanel.DOFade(1, 0.3f);
    // }
    
    IEnumerator Act20Demo()
    {
        yield return new WaitForSecondsRealtime(1f);
        BtnClick.Instance.btn1();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn2();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn3();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn4();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn5();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.endTurn();
    }
    
    IEnumerator Act21Demo()
    {
        yield return new WaitForSecondsRealtime(1f);
        BtnClick.Instance.btn2();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn4();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn5();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn6();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.Empty();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.endTurn();
    }
    
    IEnumerator Act22Demo()
    {
        yield return new WaitForSecondsRealtime(1f);
        BtnClick.Instance.btn1();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn3();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.Empty();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.Empty();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.Empty();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.endTurn();
    }
    
    IEnumerator Act23Demo()
    {
        yield return new WaitForSecondsRealtime(1f);
        BtnClick.Instance.btn1();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn3();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn8();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn9();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn7();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.endTurn();
    }
    
    IEnumerator Act24Demo()
    {
        yield return new WaitForSecondsRealtime(1f);
        BtnClick.Instance.btn1();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn3();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn7();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn8();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.btn9();
        yield return new WaitForSecondsRealtime(0.15f);
        BtnClick.Instance.endTurn();
    }
}
