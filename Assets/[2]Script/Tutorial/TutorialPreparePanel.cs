﻿using DG.Tweening;
using UnityEngine;

public class TutorialPreparePanel : MonoBehaviour 
{
    public void Show()
    {
        gameObject.SetActive(true);
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(1, 0.5f).SetEase(Ease.OutCubic);
    }

    public void HideAll()
    { 
        transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            GameManager.Instance.turnControl.magicNumber[0] = 1;
            GameManager.Instance.turnControl.magicNumber[1] = 3;
            GameManager.Instance.turnControl.magicNumber[2] = 7;
            GameManager.Instance.turnControl.magicNumber[3] = 8;
            GameManager.Instance.turnControl.magicNumber[4] = 9;
            transform.gameObject.SetActive(false);
            // TimeManager.Instance.StopTimer = false;
            // GameManager.Instance.goPanel.Show();
            // TimeManager.Instance.StopTimer = false;
            TutorialManager.Instance.ShowNextAct(0);
        });
    }
    
    public void HideSimple()
    {
        transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            GameManager.Instance.turnControl.magicNumber[0] = 1;
            GameManager.Instance.turnControl.magicNumber[1] = 3;
            GameManager.Instance.turnControl.magicNumber[2] = 7;
            GameManager.Instance.turnControl.magicNumber[3] = 8;
            GameManager.Instance.turnControl.magicNumber[4] = 9;
            transform.gameObject.SetActive(false);
            // TimeManager.Instance.StopTimer = false;
            // GameManager.Instance.goPanel.Show();
            // TimeManager.Instance.StopTimer = false;
            TutorialManager.Instance.ShowNextAct(18);
        });
    }
}