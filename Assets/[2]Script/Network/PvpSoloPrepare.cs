﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Pool;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class PvpSoloPrepare : MonoBehaviour
{
  [Header("generate")]
    [SerializeField] private TextMeshProUGUI generateBtnText;
  
    [Header("my digit")]
    [SerializeField] private TMP_InputField inputFieldChose;
    
    [Header("chose")]
    [SerializeField] private TextMeshProUGUI choseDigitText;
    
    private int[] currentChoseNum;
    public int[] acceptChoseNum { get; private set; }

    private bool isGetReady;
    private CanvasGroup panel;

    public void Show()
    {
        panel.gameObject.SetActive(true);
        panel.alpha = 0;
        panel.DOFade(1, 1);
    }

    public void Hide()
    {
        panel.DOFade(0, 1).OnComplete(() =>
        {
            panel.gameObject.SetActive(false);
            TimeManager.Instance.StartTimerPvp();
            GameManager.Instance.goPanel.Show();
            // вкл тймер
        });
    }
   
    private void Start()
    {
        // Show();
        panel = GetComponent<CanvasGroup>();
        inputFieldChose.characterLimit = GameManager.Instance.turnControl.numLimit;
        string numText = String.Empty;
        
        acceptChoseNum = Helper.SetRepeatNum(GameManager.Instance.turnControl.numLimit);

        foreach (int i in acceptChoseNum)
        {
            numText += i.ToString();
        }

        choseDigitText.text = numText;
    }

    public void AcceptDigit()
    {
        if (isGetReady)
        {
            return;
        }
        
        string numText = String.Empty;

        if (currentChoseNum == null)
        {
            return;
        }
        
        acceptChoseNum = currentChoseNum;
        
        foreach (int i in acceptChoseNum)
        {
            numText += i.ToString();
        }

        choseDigitText.text = numText;
    }
    
    public void GenerateDigit()
    {
        if (isGetReady)
        {
            return;
        }
        
        string numText = String.Empty;
        currentChoseNum = Helper.SetRepeatNum(GameManager.Instance.turnControl.numLimit);

        foreach (int i in currentChoseNum)
        {
            numText += i.ToString();
        }

        generateBtnText.text = numText;
    }
    
    public void ChoseDigitAccept()
    {
        if (isGetReady)
        {
            return;
        }

        List<int> nums = new List<int>();

        if (inputFieldChose.text.Length <= GameManager.Instance.turnControl.numLimit -1)
        {
            return;
        }
        
        for (int i = 0; i < GameManager.Instance.turnControl.numLimit; i++)
        {
            char inputNum = inputFieldChose.text[i];
            int num = Convert.ToInt32(inputNum.ToString());

            if (num == 0)
            {
                num = 1;
            }
     
            nums.Add(num);
        }
        
        string numText = String.Empty;
        
        acceptChoseNum = nums.ToArray();
        
        foreach (int i in acceptChoseNum)
        {
            numText += i.ToString();
        }

        choseDigitText.text = numText;
    }

    public void GetReady()
    {
        if (isGetReady)
        {
            return;
        }
        
        isGetReady = true;
        GameManager.Instance.turnControl.magicNumber = acceptChoseNum;
        Hide();
    }

    public void Exit()
    {
        SceneManager.LoadScene("Loading");
    }
}
