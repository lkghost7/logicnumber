﻿using UnityEngine;
using UnityEngine.UI;

public class Message : MonoBehaviour
{
    [SerializeField] private Text message;

    public void Show(string nick, string message, Color colorMessage)
    {
        this.message.color = colorMessage;
        this.message.text = nick + ": " + message;
    }
    
    public void ShowUp()
    {
        this.message.text = "";
    }
}