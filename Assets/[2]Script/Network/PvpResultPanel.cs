﻿using System.Collections;
using DG.Tweening;
using Lean.Localization;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PvpResultPanel : MonoBehaviour
{
    [Header("pref")]
    [SerializeField] private  CanvasGroup canvas;
    [SerializeField] private GameObject win;
    [SerializeField] private GameObject lose;
    
    [Header("enemy")]
    [SerializeField] private TextMeshProUGUI timeEnemy;
    [SerializeField] private TextMeshProUGUI digitEnemy;
    [SerializeField] private TextMeshProUGUI rowEnemy;
    
    [Header("my")]
    [SerializeField] private TextMeshProUGUI timeMY;
    [SerializeField] private TextMeshProUGUI digitMy;
    [SerializeField] private TextMeshProUGUI rowMy;
    
    [Header("nick")]
    [SerializeField] private TextMeshProUGUI myNick;
    [SerializeField] private TextMeshProUGUI enemyNick;

    public void ShowLocal(int status, string nickName)
    {
        myNick.text = nickName;
        // при победе
        gameObject.SetActive(true);
        canvas.alpha = 0;
        canvas.DOFade(1, 1);

        if (status == 1)
        {
            win.gameObject.SetActive(true);
            lose.gameObject.SetActive(false);
            GameSettings.Instance.PvpAchieve++;
            GameSettings.Instance.SavePvpAchieve();
            GameSettings.Instance.achiveManager.CheckPvpStatus(GameSettings.Instance.PvpAchieve);
        }
        else
        {
            win.gameObject.SetActive(false);
            lose.gameObject.SetActive(true);
        }
    }
    
    public void ShowNameEnemy(string nickName)
    {
        enemyNick.text = nickName;
    }
    
    public void ShowName(string nickName)
    {
        myNick.text = nickName;
    }
    
    public void ShowNet(string nickName)
    {
        enemyNick.text = nickName;
        // при проигрыше
        gameObject.SetActive(true);
        canvas.alpha = 0;
        canvas.DOFade(1, 1);
        
        win.gameObject.SetActive(false);
        lose.gameObject.SetActive(true);
    }

    public void InitEnemyLocal(string time, string digit, string rowCount)
    {
        timeEnemy.text = time;
        digitEnemy.text = digit;
        rowEnemy.text = rowCount;
    }
    
    public void InitEnemyRemote(string time, string digit, string rowCount)
    {
        timeEnemy.text = time;
        digitEnemy.text = digit;
        rowEnemy.text = rowCount;
    }

    public void InitMyLocal(string time, string digit, string rowCount)
    {
        timeMY.text = time;
        digitMy.text = digit;
        rowMy.text = rowCount;
    }
    
    public void InitMyRemote(string time, string digit, string rowCount)
    {
        timeMY.text = time;
        digitMy.text = digit;
        rowMy.text = rowCount;
    }

    public void Hide()
    {
        canvas.DOFade(0, 1).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }

    public void Exit()
    {
        GameManager.Instance.gameNetManager.ExitPvpLvl();
    }
    
    public void Reload()
    {
        GameManager.Instance.gameNetManager.ReloadLvl();
    }
    
    private int nextUpdate = 1;
    private bool connectStatus;
    private bool connectPlayer;
    private Player[] playerList;
    [SerializeField] private TextMeshProUGUI enemyWin;
    
    public void Update()
    {
        if (Time.time >= nextUpdate)
        {
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            StatusConnect();
        }
    }

    private void StatusConnect()
    {
        connectStatus = PhotonNetwork.IsConnected;
        playerList = PhotonNetwork.PlayerList;

        if (playerList.Length >= 2)
        {
            connectPlayer = true;
        }
        else
        {
            connectPlayer = false;
            enemyWin.gameObject.SetActive(true);
            enemyWin.text = LeanLocalization.GetTranslationText("pvp.disconnect");
        }

        if (!connectStatus)
        {
            enemyWin.gameObject.SetActive(true);
            enemyWin.text = LeanLocalization.GetTranslationText("pvp.disconnectMy");
        }
    }
    
}
