﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Localization;
using Lean.Pool;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PvpPrepare : MonoBehaviour
{
    [Header("generate")]
    [SerializeField] private Button generateBtn;
    [SerializeField] private TextMeshProUGUI generateBtnText;
    [SerializeField] private Button generateBtnAccept;

    [Header("my digit")]
    [SerializeField] private TMP_InputField inputFieldChose;
    [SerializeField] private Button choseBtnAccept;
    
    [Header("chose")]
    [SerializeField] private TextMeshProUGUI choseDigitText;
    
    [Header("Ready")]
    [SerializeField] private Button acceptBtnReady;
    [SerializeField] private GameObject youReadyCellCheckMark;
    [SerializeField] private GameObject enemyReadyCellCheckMark;
    
    [SerializeField] private GameObject youReadyCellCross;
    [SerializeField] private GameObject enemyReadyCellCross;

    private int[] currentChoseNum;
    public int[] acceptChoseNum { get; private set; }

    private bool isGetReady;
    private CanvasGroup panel;
    
    [SerializeField] private Message messagePrefab;
    [SerializeField] private InputField chatField;
    [SerializeField] private Transform contentT;
    [SerializeField] private Color myColor;
    [SerializeField] private Color enemyColor;
    
    private PhotonView currentPhoton;
    
    public void Show()
    {
        panel.gameObject.SetActive(true);
        panel.alpha = 0;
        panel.DOFade(1, 1);
    
    }

    public void Hide()
    {
        panel.DOFade(0, 1).OnComplete(() =>
        {
            panel.gameObject.SetActive(false);
            TimeManager.Instance.StartTimerPvp();
            GameManager.Instance.goPanel.Show();
            // вкл тймер
        });
    }

    private void Start()
    {
        currentPhoton = GetComponent<PhotonView>();
        panel = GetComponent<CanvasGroup>();
        inputFieldChose.characterLimit = GameManager.Instance.turnControl.numLimit;
        string numText = String.Empty;
        
        acceptChoseNum = Helper.SetRepeatNum(GameManager.Instance.turnControl.numLimit);

        foreach (int i in acceptChoseNum)
        {
            numText += i.ToString();
        }

        choseDigitText.text = numText;
        
        Message message = LeanPool.Spawn(messagePrefab, contentT);
        message.ShowUp();
    }

    public void AcceptDigit()
    {
        if (isGetReady)
        {
            return;
        }
        
        string numText = String.Empty;

        if (currentChoseNum == null)
        {
            return;
        }
        
        acceptChoseNum = currentChoseNum;
        
        foreach (int i in acceptChoseNum)
        {
            numText += i.ToString();
        }

        choseDigitText.text = numText;
    }
    
    public void GenerateDigit()
    {
        if (isGetReady)
        {
            return;
        }
        
        string numText = String.Empty;
        currentChoseNum = Helper.SetRepeatNum(GameManager.Instance.turnControl.numLimit);

        foreach (int i in currentChoseNum)
        {
            numText += i.ToString();
        }

        generateBtnText.text = numText;
    }
    
    public void ChoseDigitAccept()
    {
        if (isGetReady)
        {
            return;
        }

        List<int> nums = new List<int>();

        if (inputFieldChose.text.Length <= GameManager.Instance.turnControl.numLimit -1)
        {
            return;
        }
        
        for (int i = 0; i < GameManager.Instance.turnControl.numLimit; i++)
        {
            char inputNum = inputFieldChose.text[i];
            int num = Convert.ToInt32(inputNum.ToString());

            if (num == 0)
            {
                num = 1;
            }
     
            nums.Add(num);
        }
        
        string numText = String.Empty;
        
        acceptChoseNum = nums.ToArray();
        
        foreach (int i in acceptChoseNum)
        {
            numText += i.ToString();
        }

        choseDigitText.text = numText;
    }

    public void GetReady()
    {
        if (isGetReady)
        {
            return;
        }
        
        isGetReady = true;
        youReadyCellCheckMark.SetActive(true);
        youReadyCellCross.SetActive(false);
        
        GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.winPvpCheck2, youReadyCellCheckMark.transform.position, Quaternion.identity);
        StartCoroutine( GameManager.Instance.effector.Despawner(effectBum, 2));
        GameManager.Instance.gameNetManager.Ready();
    }

    public void EnemyReady()
    {
        enemyReadyCellCheckMark.SetActive(true);
        enemyReadyCellCross.SetActive(false);
        
        GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.winPvpCheck2, enemyReadyCellCheckMark.transform.position, Quaternion.identity);
        StartCoroutine( GameManager.Instance.effector.Despawner(effectBum, 2));
    }

    public void Exit()
    {
        SceneManager.LoadScene("Loading");
    }
    
    public void SendMessageMy()
    {
        SendMessage();
    }
    
    private void SendMessage() // отправить сообщение 
    {
        if (chatField.text.Length == 0)
        {
            return;
        }
        
        Message message = LeanPool.Spawn(messagePrefab, contentT);
        message.Show(PhotonNetwork.NickName, chatField.text, myColor);
        currentPhoton.RPC("TakeMessage2", RpcTarget.Others, PhotonNetwork.NickName, chatField.text);
        chatField.text = "";
    }

    [PunRPC]
    private void TakeMessage2(string nick, string rpcMessage) // приниять сообщение 
    {
        Message message = LeanPool.Spawn(messagePrefab, contentT);
        message.Show(nick, rpcMessage, enemyColor);
    }
    
    private int nextUpdate = 1;
    private bool connectStatus;
    private bool connectPlayer;
    private Player[] playerList;
    [SerializeField] private TextMeshProUGUI enemyWin;
    
    public void Update()
    {
        if (Time.time >= nextUpdate)
        {
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            StatusConnect();
        }
    }

    private void StatusConnect()
    {
        connectStatus = PhotonNetwork.IsConnected;
        playerList = PhotonNetwork.PlayerList;

        if (playerList.Length >= 2)
        {
            connectPlayer = true;
        }
        else
        {
            connectPlayer = false;
            enemyWin.gameObject.SetActive(true);
            enemyWin.text = LeanLocalization.GetTranslationText("pvp.disconnect");
        }

        if (!connectStatus)
        {
            enemyWin.gameObject.SetActive(true);
            enemyWin.text = LeanLocalization.GetTranslationText("pvp.disconnectMy");
        }
    }

}
