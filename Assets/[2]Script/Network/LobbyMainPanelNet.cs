﻿using System;
using ExitGames.Client.Photon;
using Photon.Realtime;
using System.Collections.Generic;
using Lean.Pool;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class LobbyMainPanelNet : MonoBehaviourPunCallbacks
{
    [Header("Login Panel")] public GameObject LoginPanel;

    public InputField PlayerNameInput;

    [Header("Selection Panel")] public GameObject SelectionPanel;

    [Header("Create Room Panel")] public GameObject CreateRoomPanel;

    public InputField RoomNameInputField;
    public InputField MaxPlayersInputField;

    [Header("Join Random Room Panel")] public GameObject JoinRandomRoomPanel;

    [Header("Room List Panel")] public GameObject RoomListPanel;

    public GameObject RoomListContent;
    public GameObject RoomListEntryPrefab;

    [Header("Inside Room Panel")] public GameObject InsideRoomPanel;

    public Button StartGameButton;
    public GameObject PlayerListEntryPrefab;

    private Dictionary<string, RoomInfo> cachedRoomList;
    private Dictionary<string, GameObject> roomListEntries;
    private Dictionary<int, GameObject> playerListEntries;

    private PhotonView currentPhoton;
    private PvpLevel pvpLevel;
    private PvpSoloLevel pvpSoloLevel;
 
    [SerializeField] private TextMeshProUGUI online;
    [SerializeField] private TextMeshProUGUI rooms;
    [SerializeField] private Message messagePrefab;
    [SerializeField] private InputField chatField;
    [SerializeField] private Transform contentT;
    [SerializeField] private Color myColor;
    [SerializeField] private Color enemyColor;
    // [SerializeField] private ToggleGroup toggleGroup;
    // [SerializeField] private Toggle[] togglesList;
    [SerializeField] private MyToggle[] myToggle;
    [SerializeField] private MyToggle[] myToggle2;
    
    private bool connectStatus;
    private bool connectPlayer;
    private Player[] playerList;
    private int nextUpdate = 1;
    private int roomCount;

    public void Awake()
    {
        pvpLevel = PvpLevel.LEVEL_5;
        pvpSoloLevel = PvpSoloLevel.LEVEL_SOLO_5;
        PhotonNetwork.AutomaticallySyncScene = true;

        cachedRoomList = new Dictionary<string, RoomInfo>();
        roomListEntries = new Dictionary<string, GameObject>();

        string defaultUser = "User " + Random.Range(1, 1000);
        
        string namePlayer = PlayerPrefs.GetString("PlayerName",defaultUser);
        PlayerNameInput.text = namePlayer;
    }

    private void Start()
    {
        currentPhoton = GetComponent<PhotonView>();

        Message message = LeanPool.Spawn(messagePrefab, contentT);
        message.ShowUp();
    }

    public void Hide()
    {
        print("hide");
        gameObject.SetActive(false);
    }

    public override void OnConnectedToMaster()
    {
        this.SetActivePanel(SelectionPanel.name);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        ClearRoomListView();

        UpdateCachedRoomList(roomList);
        UpdateRoomListView();
    }

    public override void OnJoinedLobby()
    {
        // whenever this joins a new lobby, clear any previous room lists
        cachedRoomList.Clear();
        ClearRoomListView();
    }

    // note: when a client joins / creates a room, OnLeftLobby does not get called, even if the client was in a lobby before
    public override void OnLeftLobby()
    {
        cachedRoomList.Clear();
        ClearRoomListView();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        SetActivePanel(SelectionPanel.name);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        SetActivePanel(SelectionPanel.name);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        string roomName = "Room " + Random.Range(1000, 10000);

        RoomOptions options = new RoomOptions {MaxPlayers = 2};

        PhotonNetwork.CreateRoom(roomName, options, null);
    }

    public override void OnJoinedRoom()
    {
        // joining (or entering) a room invalidates any cached lobby room list (even if LeaveLobby was not called due to just joining a room)
        cachedRoomList.Clear();

        SetActivePanel(InsideRoomPanel.name);

        if (playerListEntries == null)
        {
            playerListEntries = new Dictionary<int, GameObject>();
        }

        foreach (Player p in PhotonNetwork.PlayerList)
        {
            GameObject entry = Instantiate(PlayerListEntryPrefab);
            entry.transform.SetParent(InsideRoomPanel.transform);
            entry.transform.localScale = Vector3.one;
            entry.GetComponent<PlayerListEntryNet>().Initialize(p.ActorNumber, p.NickName);

            object isPlayerReady;
            if (p.CustomProperties.TryGetValue(PrefNetPlayers.PLAYER_READY, out isPlayerReady))
            {
                entry.GetComponent<PlayerListEntryNet>().SetPlayerReady((bool) isPlayerReady);
            }

            playerListEntries.Add(p.ActorNumber, entry);
        }


        StartGameButton.gameObject.SetActive(CheckPlayersReady());

        if (PhotonNetwork.CurrentRoom.PlayerCount <= 1)
        {
            StartGameButton.gameObject.SetActive(false);
        }

        Hashtable props = new Hashtable
        {
            {PrefNetPlayers.PLAYER_LOADED_LEVEL, false}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    }

    public override void OnLeftRoom()
    {
        SetActivePanel(SelectionPanel.name);

        foreach (GameObject entry in playerListEntries.Values)
        {
            Destroy(entry.gameObject);
        }

        playerListEntries.Clear();
        playerListEntries = null;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        GameObject entry = Instantiate(PlayerListEntryPrefab);
        entry.transform.SetParent(InsideRoomPanel.transform);
        entry.transform.localScale = Vector3.one;
        entry.GetComponent<PlayerListEntryNet>().Initialize(newPlayer.ActorNumber, newPlayer.NickName);

        playerListEntries.Add(newPlayer.ActorNumber, entry);

        StartGameButton.gameObject.SetActive(CheckPlayersReady());

        if (PhotonNetwork.CurrentRoom.PlayerCount <= 1)
        {
            StartGameButton.gameObject.SetActive(false);
        }
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Destroy(playerListEntries[otherPlayer.ActorNumber].gameObject);
        playerListEntries.Remove(otherPlayer.ActorNumber);

        StartGameButton.gameObject.SetActive(CheckPlayersReady());

        if (PhotonNetwork.CurrentRoom.PlayerCount <= 1)
        {
            StartGameButton.gameObject.SetActive(false);
        }
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
        {
            StartGameButton.gameObject.SetActive(CheckPlayersReady());

            if (PhotonNetwork.CurrentRoom.PlayerCount <= 1)
            {
                StartGameButton.gameObject.SetActive(false);
            }
        }
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        if (playerListEntries == null)
        {
            playerListEntries = new Dictionary<int, GameObject>();
        }

        GameObject entry;
        if (playerListEntries.TryGetValue(targetPlayer.ActorNumber, out entry))
        {
            object isPlayerReady;
            if (changedProps.TryGetValue(PrefNetPlayers.PLAYER_READY, out isPlayerReady))
            {
                entry.GetComponent<PlayerListEntryNet>().SetPlayerReady((bool) isPlayerReady);
            }
        }

        StartGameButton.gameObject.SetActive(CheckPlayersReady());

        if (PhotonNetwork.CurrentRoom.PlayerCount <= 1)
        {
            StartGameButton.gameObject.SetActive(false);
        }
    }


    public void OnBackButtonClicked()
    {
        if (PhotonNetwork.InLobby)
        {
            PhotonNetwork.LeaveLobby();
        }

        SetActivePanel(SelectionPanel.name);
    }

    public void OnCreateRoomButtonClicked()
    {
        string roomName = RoomNameInputField.text;
        roomName = (roomName.Equals(string.Empty)) ? "Room " + Random.Range(1000, 10000) : roomName;

        byte maxPlayers;
        byte.TryParse(MaxPlayersInputField.text, out maxPlayers);
        maxPlayers = (byte) Mathf.Clamp(maxPlayers, 2, 8);

        RoomOptions options = new RoomOptions {MaxPlayers = maxPlayers, PlayerTtl = 10000};

        PhotonNetwork.CreateRoom(roomName, options, null);
    }

    public void OnJoinRandomRoomButtonClicked()
    {
        SetActivePanel(JoinRandomRoomPanel.name);

        PhotonNetwork.JoinRandomRoom();
    }

    public void OnLeaveGameButtonClicked()
    {
        PhotonNetwork.LeaveRoom();
    }

    public void OnLoginButtonClicked()
    {
        string playerName = PlayerNameInput.text;

        if (!playerName.Equals(""))
        {
            PhotonNetwork.LocalPlayer.NickName = playerName;
            PhotonNetwork.ConnectUsingSettings();
            PlayerPrefs.SetString("PlayerName", playerName);
        }
        else
        {
            Debug.LogError("Player Name is invalid.");
        }
    }

    public void OnRoomListButtonClicked()
    {
        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }

        SetActivePanel(RoomListPanel.name);
    }

    public void OnStartGameButtonClicked()
    {
        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.CurrentRoom.IsVisible = false;

        switch (pvpLevel)
        {
            case PvpLevel.LEVEL_3:
                PhotonNetwork.LoadLevelLk("LevelPvp1");
                break;
            case PvpLevel.LEVEL_4:
                PhotonNetwork.LoadLevelLk("LevelPvp2");
                break;
            case PvpLevel.LEVEL_5:
                PhotonNetwork.LoadLevelLk("LevelPvp3");
                break;
            case PvpLevel.LEVEL_6:
                PhotonNetwork.LoadLevelLk("LevelPvp4");
                break;
            case PvpLevel.LEVEL_7:
                PhotonNetwork.LoadLevelLk("LevelPvp5");
                break;
        }

        // LoadingScene.Instance.ActiveLoadingCanvas();
        // AsyncOperation operation = SceneManager.UnloadSceneAsync("MainMenu");
        // ControlMainMenu.Instance.HideLevelPVP((SceneIndex)33);
    }

    public bool CheckPlayersReady()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            return false;
        }

        foreach (Player p in PhotonNetwork.PlayerList)
        {
            object isPlayerReady;
            if (p.CustomProperties.TryGetValue(PrefNetPlayers.PLAYER_READY, out isPlayerReady))
            {
                if (!(bool) isPlayerReady)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    public void ClearRoomListView()
    {
        foreach (GameObject entry in roomListEntries.Values)
        {
            Destroy(entry.gameObject);
        }

        roomListEntries.Clear();
    }

    public void LocalPlayerPropertiesUpdated()
    {
        StartGameButton.gameObject.SetActive(CheckPlayersReady());

        if (PhotonNetwork.CurrentRoom.PlayerCount <= 1)
        {
            StartGameButton.gameObject.SetActive(false);
        }
    }

    public void SetActivePanel(string activePanel)
    {
        LoginPanel.SetActive(activePanel.Equals(LoginPanel.name));
        SelectionPanel.SetActive(activePanel.Equals(SelectionPanel.name));
        CreateRoomPanel.SetActive(activePanel.Equals(CreateRoomPanel.name));
        JoinRandomRoomPanel.SetActive(activePanel.Equals(JoinRandomRoomPanel.name));
        RoomListPanel.SetActive(activePanel.Equals(RoomListPanel.name));
        InsideRoomPanel.SetActive(activePanel.Equals(InsideRoomPanel.name));
    }

    public void UpdateCachedRoomList(List<RoomInfo> roomList)
    {
        foreach (RoomInfo info in roomList)
        {
            // Remove room from cached room list if it got closed, became invisible or was marked as removed
            if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
            {
                if (cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList.Remove(info.Name);
                }

                continue;
            }

            // Update cached room info
            if (cachedRoomList.ContainsKey(info.Name))
            {
                cachedRoomList[info.Name] = info;
            }
            // Add new room info to cache
            else
            {
                cachedRoomList.Add(info.Name, info);
            }
        }
    }

    public void UpdateRoomListView()
    {
        foreach (RoomInfo info in cachedRoomList.Values)
        {
            GameObject entry = Instantiate(RoomListEntryPrefab);
            entry.transform.SetParent(RoomListContent.transform);
            entry.transform.localScale = Vector3.one;
            entry.GetComponent<RoomListEntryNet>().Initialize(info.Name, (byte) info.PlayerCount, info.MaxPlayers);

            roomListEntries.Add(info.Name, entry);
        }
    }

    public void PressMyCheck(int num)
    {
        
        pvpLevel = (PvpLevel) num;
        
        for (int i = 0; i < myToggle.Length; i++)
        {
            myToggle[i].Hide();
        }
        
        myToggle[num-3].Show();
        
        currentPhoton.RPC("Send_Data4", RpcTarget.Others, num);
    }
   
    [PunRPC]
    private void Send_Data4(int num) // все приняли
    {
        pvpLevel = (PvpLevel) num;
        
        for (int i = 0; i < myToggle.Length; i++)
        {
            myToggle[i].Hide();
        }
        
        myToggle[num-3].Show();
    }
    
    
    // /////////// ///////// online   ///////////////////////////


    public void SendMessageMy()
    {
        SendMessage();
    }

    private void SendMessage() // отправить сообщение 
    {
        if (chatField.text.Length == 0)
        {
            return;
        }
        
        Message message = LeanPool.Spawn(messagePrefab, contentT);
        message.Show(PhotonNetwork.NickName, chatField.text, myColor);
        currentPhoton.RPC("TakeMessage", RpcTarget.Others, PhotonNetwork.NickName, chatField.text);
        chatField.text = "";
    }

    [PunRPC]
    private void TakeMessage(string nick, string rpcMessage) // приниять сообщение 
    {
        Message message = LeanPool.Spawn(messagePrefab, contentT);
        message.Show(nick, rpcMessage, enemyColor);
    }

    public void Update()
    {
        if (Time.time >= nextUpdate)
        {
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            StatusConnect();
        }
    }

    private void StatusConnect()
    {
        connectStatus = PhotonNetwork.IsConnected;
        playerList = PhotonNetwork.PlayerList;
        roomCount = PhotonNetwork.CountOfRooms;

        rooms.text = PhotonNetwork.CountOfRooms + " / 10";
        online.text = PhotonNetwork.CountOfPlayers + " / 20";
    }

    public void LoadSoloLevel()
    {
        switch (pvpSoloLevel)
        {
            case PvpSoloLevel.LEVEL_SOLO_3:
                // PhotonNetwork.LoadLevelLk("LevelPvp1");
                SceneManager.LoadScene("LevelPvp6");
                break;
            case PvpSoloLevel.LEVEL_SOLO_4:
                // PhotonNetwork.LoadLevelLk("LevelPvp2");
                SceneManager.LoadScene("LevelPvp7");
                break;
            case PvpSoloLevel.LEVEL_SOLO_5:
                // PhotonNetwork.LoadLevelLk("LevelPvp3");
                SceneManager.LoadScene("LevelPvp8");
                break;
            case PvpSoloLevel.LEVEL_SOLO_6:
                // PhotonNetwork.LoadLevelLk("LevelPvp4");
                SceneManager.LoadScene("LevelPvp9");
                break;
            case PvpSoloLevel.LEVEL_SOLO_7:
                // PhotonNetwork.LoadLevelLk("LevelPvp5");
                SceneManager.LoadScene("LevelPvp10");
                break;
        }
    }

    public void OnStartGameButtonClickedSolo(int numLvl)
    {
        pvpSoloLevel = (PvpSoloLevel) numLvl;
        
        for (int i = 0; i < myToggle2.Length; i++)
        {
            myToggle2[i].Hide();
        }
        
        myToggle2[numLvl-3].Show();
    }
}

public enum PvpLevel
{
    LEVEL_3 = 3,
    LEVEL_4 = 4,
    LEVEL_5 = 5,
    LEVEL_6 = 6,
    LEVEL_7 = 7
}

public enum PvpSoloLevel
{
    LEVEL_SOLO_3 = 3,
    LEVEL_SOLO_4 = 4,
    LEVEL_SOLO_5 = 5,
    LEVEL_SOLO_6 = 6,
    LEVEL_SOLO_7 = 7
}