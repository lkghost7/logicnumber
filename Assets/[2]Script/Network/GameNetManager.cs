﻿using System;
using Lean.Localization;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

public class GameNetManager : MonoBehaviour
{
    [SerializeField] private PanelEnemy enemy;
    [SerializeField] private TextMeshProUGUI enemyWin;
    [SerializeField] private PvpResultPanel pvpResultPanel;
    [SerializeField] private PvpPrepare pvpPrepare;
    [SerializeField] private GameObject block;

    private PhotonView photonView;
    private int enemyStatus = 0;
    private bool connectStatus;
    private bool connectPlayer;
    private Player[] playerList;

    private int rowCount = 0;
    string magic = String.Empty;

    private void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    public void SendRowStatus(int num, int mark, int cross) // в игры отправить статус другому игроку о рядах
    {
        print("SendRowStatus");
        rowCount++;
        photonView.RPC("Send_Data", RpcTarget.Others, num, mark, cross);
    }

    [PunRPC]
    private void Send_Data(int num, int mark, int cross) // удаленный вызов
    {
        enemy.ControlCheck(num, mark, cross);
    }

    // срабатывает при победе
    public void SendWinStatus(int status) // вызвать панель и заблочить у врага панель
    {
        string timeMain = TimeManager.Instance.timeMain.text;
        pvpResultPanel.ShowLocal(status, PhotonNetwork.NickName);
        GetMagicNum();
        photonView.RPC("Send_Win", RpcTarget.Others, PhotonNetwork.NickName, status);
        pvpResultPanel.InitMyLocal(timeMain, magic, rowCount.ToString()); // записать себе в таблицу данные
        SendToEnemyData(timeMain, magic, rowCount.ToString()); // отправить данные врагу
    }

    private void GetMagicNum()
    {
        for (int i = 0; i < GameManager.Instance.turnControl.numLimit; i++)
        {
            magic += GameManager.Instance.turnControl.magicNumber[i];
        }
    }

    public void SendFailStatus(int status) // отправить проигравший, он должен дождатся завершения хода.
    {
        enemyWin.gameObject.SetActive(true);
        enemyWin.text = LeanLocalization.GetTranslationText("pvp.digit5");
        block.SetActive(true);

        photonView.RPC("First_Fail", RpcTarget.Others, status);
        enemyStatus++; // проигравший сделает +1

        if (enemyStatus >= 2)
        {
            SendWinStatus(2);
        }
    }

    [PunRPC]
    private void First_Fail(int status) // получить, если  другой игрок проиграл
    {
        enemyStatus++; //этот статус примет тот, кто еще в игре и пока не проиграл
    }

    [PunRPC]
    private void Send_Win(string name, int status) // удаленный вызов
    {
        string timeMain = TimeManager.Instance.timeMain.text;

        pvpResultPanel.ShowNet(name); // удаленный вызов
        GetMagicNum();
        pvpResultPanel.InitMyRemote(timeMain, magic, rowCount.ToString());
    }

    private void SendToEnemyData(string time, string digit, string numRow) // локальный вызов
    {
        photonView.RPC("Send_Digit", RpcTarget.Others, time, digit, numRow);
    }

    [PunRPC]
    private void Send_Digit(string time, string digit, string numRow) // удаленный вызов
    {
        string timeMain = TimeManager.Instance.timeMain.text;
        pvpResultPanel.InitEnemyRemote(time, digit, numRow); // это примет тот кто проиграл 
        SendToEnemyCallBack(timeMain, magic, rowCount.ToString()); // это примет и сразу отправит, тот кто проиграл обратно тому кто выйграл
    }

    private void SendToEnemyCallBack(string time, string digit, string numRow) // вызов колбэка
    {
        pvpResultPanel.ShowName(PhotonNetwork.NickName);
        photonView.RPC("Send_CallBack", RpcTarget.Others, time, digit, numRow, PhotonNetwork.NickName);
    }

    [PunRPC]
    private void Send_CallBack(string time, string digit, string numRow, string nik) // callback
    {
        pvpResultPanel.InitEnemyLocal(time, digit, numRow); // результат колбэка
        pvpResultPanel.ShowNameEnemy(nik);
    }

    public void ReloadLvl()
    {
        SendReload();
    }

    public void ExitPvpLvl()
    {
        SendExit();
    }

    private void SendReload()
    {
        photonView.RPC("ReloadPvp", RpcTarget.All);
    }

    [PunRPC]
    private void ReloadPvp() // callback
    {
        PhotonNetwork.LoadLevel((int) GameManager.Instance.startGameControlRef.sceneIndex); // перегрузить текущий для всех.
    }

    private void SendExit()
    {
        photonView.RPC("ExitMainMenuPvp", RpcTarget.All);
    }

    [PunRPC]
    private void ExitMainMenuPvp() // callback
    {
        PhotonNetwork.LoadLevel("Loading"); //  main menu
    }

    // Prepare //     // Prepare //     // Prepare //     // Prepare //

    private int isGetReady = 0;

    public void Ready()
    {
        photonView.RPC("SendReady", RpcTarget.Others, pvpPrepare.acceptChoseNum);
        isGetReady++;

        if (isGetReady >= 2)
        {
            pvpPrepare.Hide();
            photonView.RPC("SendReadyGo", RpcTarget.Others);
        }
    }

    [PunRPC]
    private void SendReady(int[] enemyNum) // пришлет готовность, если готов первый и число
    {
        isGetReady++;
        pvpPrepare.EnemyReady();
        // тут первый кто нажал пришлет число
        GameManager.Instance.turnControl.magicNumber = enemyNum; // назначить то число что вырал враг
    }

    [PunRPC]
    private void SendReadyGo() // закроет панель у врага если готовы оба
    {
        pvpPrepare.Hide();
    }

    private int nextUpdate = 1;
    

    public void Update()
    {
        if (Time.time >= nextUpdate)
        {
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            StatusConnect();
        }
    }

    private void StatusConnect()
    {
        connectStatus = PhotonNetwork.IsConnected;
        playerList = PhotonNetwork.PlayerList;

        if (playerList.Length >= 2)
        {
            connectPlayer = true;
        }
        else 
        {
            connectPlayer = false;
            enemyWin.gameObject.SetActive(true);
            enemyWin.text = LeanLocalization.GetTranslationText("pvp.disconnect");
        }

        if (!connectStatus)
        {
            enemyWin.gameObject.SetActive(true);
            enemyWin.text = LeanLocalization.GetTranslationText("pvp.disconnectMy");
        }
    }
}