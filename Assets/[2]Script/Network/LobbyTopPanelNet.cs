﻿using Photon.Pun;
using TMPro;
using UnityEngine;

public class LobbyTopPanelNet : MonoBehaviour
{
    private readonly string connectionStatusMessage = "    Connection Status: ";
    [Header("UI References")] public TextMeshProUGUI ConnectionStatusText;

    public void Update()
    {
        ConnectionStatusText.text = connectionStatusMessage + PhotonNetwork.NetworkClientState;
    }
}