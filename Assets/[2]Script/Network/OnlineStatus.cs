﻿using System;
using Lean.Pool;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OnlineStatus : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI online;
    [SerializeField] private TextMeshProUGUI rooms;
    [SerializeField] private Message messagePrefab;
    [SerializeField] private InputField chatField;
    [SerializeField] private Transform contentT;
    [SerializeField] private Color myColor;
    [SerializeField] private Color enemyColor;
    private PhotonView photonView;
    // [SerializeField] private RectTransform rect;
    
    private bool connectStatus;
    private bool connectPlayer;
    private Player[] playerList;
    private int nextUpdate = 1;
    private int roomCount;

    public void SendMessageMy()
    {
        SendMessage();
        SendRowStatus(2,3,4);
    }

    private void Start()
    {
        photonView = GetComponent<PhotonView>();
        Message message = LeanPool.Spawn(messagePrefab, contentT);
        message.ShowUp();
    }

    public void SendRowStatus(int num, int mark, int cross) // в игры отправить статус другому игроку о рядах
    {
        print("SendRowStatus");
        photonView.RPC("Send_Data2", RpcTarget.Others, num, mark, cross);
    }

    [PunRPC]
    private void Send_Data2(int num, int mark, int cross) // удаленный вызов
    {
      print("send");
      print("num" + num + mark + cross);
    }

    private void SendMessage() // отправить сообщение 
    {
        print("SendMessage");
        Message message = LeanPool.Spawn(messagePrefab, contentT);
        message.Show(PhotonNetwork.NickName, chatField.text, myColor);
        photonView.RPC("TakeMessage", RpcTarget.Others, PhotonNetwork.NickName, chatField.text);
        // LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
    }

    [PunRPC]
    private void TakeMessage(string nick, string rpcMessage) // приниять сообщение 
    {
        print("TakeMessage");
        Message message = LeanPool.Spawn(messagePrefab, contentT);
        message.Show(nick, rpcMessage, enemyColor);
        // LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
    }
    
    public void Update()
    {
        if (Time.time >= nextUpdate)
        {
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            StatusConnect();
        }
    }
    
    private void StatusConnect()
    {
        print("");
        connectStatus = PhotonNetwork.IsConnected;
        playerList = PhotonNetwork.PlayerList;
        roomCount = PhotonNetwork.CountOfRooms;

        rooms.text = roomCount + " / 10";
        online.text = playerList.Length + " / 20";
    }
}
