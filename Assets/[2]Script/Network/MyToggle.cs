﻿using UnityEngine;

public class MyToggle : MonoBehaviour
{
    [SerializeField] private GameObject chek;

    public void Show()
    {
        chek.SetActive(true);
    }
    
    public void Hide()
    {
        chek.SetActive(false);
    }
}
