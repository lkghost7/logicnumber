﻿using System;
using UnityEngine;
using UnityEngine.UI;

using ExitGames.Client.Photon;
using Lean.Localization;
using Photon.Pun;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;

    public class PlayerListEntryNet : MonoBehaviour
    {
        [Header("UI References")]
        public Text PlayerNameText;

        public Image PlayerColorImage;
        public Button PlayerReadyButton;
        public Image PlayerReadyImage;

        private int ownerId;
        private bool isPlayerReady;

        public void OnEnable()
        {
            PlayerNumbering.OnPlayerNumberingChanged += OnPlayerNumberingChanged;
        }

        public void Start()
        {
            if (PhotonNetwork.LocalPlayer.ActorNumber != ownerId)
            {
                PlayerReadyButton.gameObject.SetActive(false);
            }
            else
            {
                Hashtable initialProps = new Hashtable()
                {
                    {PrefNetPlayers.PLAYER_READY, isPlayerReady}, {PrefNetPlayers.PLAYER_LIVES, PrefNetPlayers.PLAYER_MAX_LIVES}
                };
                PhotonNetwork.LocalPlayer.SetCustomProperties(initialProps);
                PhotonNetwork.LocalPlayer.SetScore(0);

                PlayerReadyButton.onClick.AddListener(() =>
                {
                    isPlayerReady = !isPlayerReady;
                    SetPlayerReady(isPlayerReady);

                    Hashtable props = new Hashtable()
                    {
                        {PrefNetPlayers.PLAYER_READY, isPlayerReady}
                    };
                    PhotonNetwork.LocalPlayer.SetCustomProperties(props);

                    if (PhotonNetwork.IsMasterClient)
                    {
                        FindObjectOfType<LobbyMainPanelNet>().LocalPlayerPropertiesUpdated();
                    }
                });
            }
        }

        public void OnDisable()
        {
            PlayerNumbering.OnPlayerNumberingChanged -= OnPlayerNumberingChanged;
        }

        public void Initialize(int playerId, string playerName)
        {
            ownerId = playerId;
            PlayerNameText.text = playerName;
        }

        private void OnPlayerNumberingChanged()
        {
            foreach (Player p in PhotonNetwork.PlayerList)
            {
                if (p.ActorNumber == ownerId)
                {
                    PlayerColorImage.color = PrefNetPlayers.GetColor(p.GetPlayerNumber());
                }
            }
        }

        public void SetPlayerReady(bool playerReady)
        {
            string ready1 = LeanLocalization.GetTranslationText("pvp.lobby.Ready1");
            string ready2 = LeanLocalization.GetTranslationText("pvp.lobby.Ready2");
            PlayerReadyButton.GetComponentInChildren<Text>().text = playerReady ? ready1 : ready2;
            PlayerReadyImage.enabled = playerReady;
        }
    }