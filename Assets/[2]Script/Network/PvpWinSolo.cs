﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PvpWinSolo : MonoBehaviour
{
 [Header("pref")]
    [SerializeField] private  CanvasGroup canvas;
    [SerializeField] private GameObject win;
    [SerializeField] private GameObject lose;
    
    [Header("my")]
    [SerializeField] private TextMeshProUGUI timeMY;
    [SerializeField] private TextMeshProUGUI digitMy;
    [SerializeField] private TextMeshProUGUI rowMy;

    public void ShowLocal(int status, string time, string digit, string rowCount)
    {
        // при победе
        print("локальный вызов show");
        gameObject.SetActive(true);
        canvas.alpha = 0;
        canvas.DOFade(1, 1);

        if (status == 1)
        {
            win.gameObject.SetActive(true);
            lose.gameObject.SetActive(false);
        }
        else
        {
            win.gameObject.SetActive(false);
            lose.gameObject.SetActive(true);
        }
        
        print("InitMyLocal");
        timeMY.text = time;
        digitMy.text = digit;
        rowMy.text = rowCount;
    }

    public void Hide()
    {
        canvas.DOFade(0, 1).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }

    public void Exit()
    {
        SceneManager.LoadScene("Loading");
    }
    
    public void Reload()
    {
        SceneManager.LoadScene((int) GameManager.Instance.startGameControlRef.sceneIndex);
    }
}
