﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Proyecto26;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

public class DataBase : MonoBehaviour
{
    public static readonly string DATABASE = "https://neonlogic-63076-default-rtdb.firebaseio.com/users";
    public static readonly string DATABASE_CLASSIC = "https://neonlogic-63076-default-rtdb.firebaseio.com/usersClassic";

    private string urlMain = "https://neonlogic-63076-default-rtdb.firebaseio.com/users.json";
    private string urlClassic = "https://neonlogic-63076-default-rtdb.firebaseio.com/usersClassic.json";

    private static void SendToDataBase(RangItem userData, string separator)
    {
        RestClient.Put<RangItem>(DATABASE + "/" + separator + ".json", userData);
    }
    
    private static void SendToDataBaseClassic(RangItem userData, string separator)
    {
        RestClient.Put<RangItem>(DATABASE_CLASSIC + "/" + separator + ".json", userData);
    }
 
    public void LoadTableData()
    {
        StartCoroutine(SendRequest());
    }
    
    public void LoadTableDataClassic()
    {
        StartCoroutine(SendRequestClassic());
    }
    
    private IEnumerator SendRequest()
    {
        GameSettings.Instance.RangUsers.Clear();
        UnityWebRequest request = UnityWebRequest.Get(urlMain);
        yield return request.SendWebRequest();
    
        List<RangItem> rangItemsListNoSort  = new List<RangItem>();
        Dictionary<string,object> objects = JsonConvert.DeserializeObject<Dictionary<string, object>>(request.downloadHandler.text);
    
        foreach (KeyValuePair<string,object> keyValuePair in objects)
        {
            RangItem item  = JsonUtility.FromJson<RangItem>(keyValuePair.Value.ToString());
            rangItemsListNoSort.Add(item);
        }

        List<RangItem> sortRang = rangItemsListNoSort.OrderByDescending(r => r.score).ToList();
        GameSettings.Instance.RangUsers.AddRange(sortRang);
    }
    
    private IEnumerator SendRequestClassic()
    { 
        GameSettings.Instance.RangUsersClassic.Clear();
        UnityWebRequest request = UnityWebRequest.Get(urlClassic);
        yield return request.SendWebRequest();
    
        List<RangItem> rangItemsListNoSort  = new List<RangItem>();
        Dictionary<string,object> objects = JsonConvert.DeserializeObject<Dictionary<string, object>>(request.downloadHandler.text);
    
        foreach (KeyValuePair<string,object> keyValuePair in objects)
        {
            RangItem item  = JsonUtility.FromJson<RangItem>(keyValuePair.Value.ToString());
            rangItemsListNoSort.Add(item);
        }

        List<RangItem> sortRang = rangItemsListNoSort.OrderByDescending(r => r.score).ToList();
        GameSettings.Instance.RangUsersClassic.AddRange(sortRang);
    }
    
    public void SaveData(string currentName)
    {
        print("SaveData");
        int uniqId = Random.Range(0, 10000);
        string uniqName = currentName + uniqId;

        RangItem rangItem = new RangItem();
        
        rangItem.id = uniqName;
        rangItem.name = currentName;
        rangItem.difficulty = GameSettings.Instance.CurrentDifficulty.ToString(); // установим  текущую сложность
        rangItem.score = ExpManager.Instance.CurrentRoundExp;
        rangItem.level = GameManager.Instance.startGameControlRef.sceneLvl -1;
        print("rangItem.level" + rangItem.level);

        GameSettings.Instance.CurrentBdName = uniqName;
        SendToDataBase(rangItem, currentName);
    }
    
    public void SaveDataClassic(string currentName)
    {
        print("SaveDataClassic");
        int uniqId = Random.Range(0, 10000);
        string uniqName = currentName + uniqId;

        RangItem rangItem = new RangItem();
        
        rangItem.id = uniqName;
        rangItem.name = currentName;
        // rangItem.difficulty = GameSettings.Instance.CurrentDifficulty.ToString(); // установим  текущую сложность
        rangItem.score = ExpManager.Instance.CurrentRoundExpClassic;
        rangItem.level = GameManager.Instance.startGameControlRef.sceneLvl -22;
        print("rangItem.level" + rangItem.level);

        GameSettings.Instance.CurrentBdName = uniqName;
        SendToDataBaseClassic(rangItem, currentName);
    }
}
