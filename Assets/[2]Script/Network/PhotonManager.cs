﻿using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhotonManager : MonoBehaviourPunCallbacks
{
     private string region = "ru";
    
    [SerializeField] private TMP_InputField roomName;
    [SerializeField] private LobbyItem itemPrefab;
    [SerializeField] private Transform content;
    [SerializeField] private string nickName;

    private List<RoomInfo> allRoomInfo = new List<RoomInfo>();
    private GameObject player;
    // [SerializeField]  GameObject player_Pref;

    public void Show()
    {
        gameObject.SetActive(true);
    }
    
    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.ConnectToRegion(region);

        if (SceneManager.GetActiveScene().name == "game_scene")
        {
            // player = PhotonNetwork.Instantiate(player_Pref.name, Vector3.zero, Quaternion.identity);
            player.GetComponent<SpriteRenderer>().color = Color.blue;
        }
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("вы подключены к " + PhotonNetwork.CloudRegion);

        if (nickName == "")
        {
            PhotonNetwork.NickName = "User";
        }
        else
        {
            PhotonNetwork.NickName = nickName;
        }
        
        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }
        
        // PhotonNetwork.JoinLobby();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("вы отключены от сервера");
    }

    public void CreateRoomButton()
    {
        if (!PhotonNetwork.IsConnected)
        {
            print("нет подключения");
            return;
        }
        
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 5;
        PhotonNetwork.CreateRoom(roomName.text, roomOptions, TypedLobby.Default);
        // PhotonNetwork.LoadLevel("game_scene");
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("создали комнату " + PhotonNetwork.CurrentRoom.Name);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("не удалось создать комнату");
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        print("on room update");
        foreach (RoomInfo info in roomList)
        {
            for (int i = 0; i < allRoomInfo.Count; i++)
            {
                if (allRoomInfo[i].masterClientId == info.masterClientId)
                {
                    return;
                }
            }

            LobbyItem lobbyItem = Instantiate(itemPrefab, content);

            if (lobbyItem != null)
            {
                lobbyItem.SetInfo(info);
                allRoomInfo.Add(info);
            }
        }
    }
 
    public override void OnJoinedRoom()
    {
        print(" OnJoinedRoom");
        // PhotonNetwork.LoadLevel("game_scene");
        LoadingScene.Instance.ActiveLoadingCanvas();
        ControlMainMenu.Instance.HideLevelMenu((SceneIndex)33);
    }

    public void JoinRandomButton()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public void JoinButton()
    {
        PhotonNetwork.JoinRoom(roomName.text);
    }

    public void LeaveBtn()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.Destroy(player.gameObject);
        PhotonNetwork.LoadLevel("Main"); 
    }

}
