﻿using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

public class LobbyItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI nameRoom;
    [SerializeField] private TextMeshProUGUI playerCount;
    
    public void SetInfo(RoomInfo info)
    {
        nameRoom.text = info.Name;
        playerCount.text = info.PlayerCount + "/" + info.MaxPlayers;
    }

    public void JoinRoom()
    {
        PhotonNetwork.JoinRoom(nameRoom.text);
    }
    
}
