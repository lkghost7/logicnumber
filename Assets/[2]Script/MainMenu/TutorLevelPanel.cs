﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorLevelPanel : MonoBehaviour
{
    public void Show()
    {
        if (this.gameObject.activeSelf)
        {
            return;
        }
        
        transform.gameObject.SetActive(true);
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(1, 0.5f).SetEase(Ease.OutCubic);
    }

    public void Hide()
    {
        transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            transform.gameObject.SetActive(false);
        });
    }

    public void Accept()
    {
        transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
        {
            print("TutorLevelPanel");
            // LoadingScene.Instance.ShowСurtainMainMenu(SceneIndex.TUTORIAL);
            ControlMainMenu.Instance.RestTutorial();
            transform.gameObject.SetActive(false);
        });
    }
    
    public void Exit()
    {
        SceneManager.LoadScene("Loading");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
