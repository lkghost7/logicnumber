﻿using System.Xml.Xsl;
using UnityEngine;

public class AudioManager : GenericSingletonClass<AudioManager>
{
    [SerializeField] private AudioSource audioSourceSound;
    [SerializeField] private AudioClip clip;
    [SerializeField] private AudioClip clickMenu;
    [SerializeField] private AudioClip hyperJump;
    [SerializeField] private AudioClip testSound;
    [SerializeField] private AudioClip acceptMenuSound;
    [SerializeField] private AudioClip cancelMenuSound;
    [SerializeField] private AudioClip togleMenuSound;
    [SerializeField] private AudioClip click17;
    
     
    public void PlaySound()
    {
        audioSourceSound.PlayOneShot(clip);
    }

    public void ButtonClickMenu()
    {
        audioSourceSound.PlayOneShot(clickMenu);
    }
    
    public void HypperSoundStart()
    {
        audioSourceSound.PlayOneShot(hyperJump);
    }
    
    public void TestSound()
    {
        audioSourceSound.PlayOneShot(testSound);
    }
    
    public void AcceptSound()
    {
        audioSourceSound.PlayOneShot(acceptMenuSound);
    }
    
    public void CancelSound()
    {
        audioSourceSound.PlayOneShot(cancelMenuSound);
    }
    
    public  void  TogleSound()
    {
        audioSourceSound.PlayOneShot(togleMenuSound);
    }
    
    public  void  ClickMenuPvpSound()
    {
        audioSourceSound.PlayOneShot(click17);
    }

}
