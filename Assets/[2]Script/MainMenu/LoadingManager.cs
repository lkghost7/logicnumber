﻿using System;
using UnityEngine;

public class LoadingManager : MonoBehaviour
{
    [SerializeField] private TutorLevelPanel quitPanel;
    [SerializeField] private GameObject rayCast;
    [SerializeField] private GameObject[] panels;
    
    public void ExitGame()
    {
        Application.Quit();
    }

    private void Update()
    {
        if (rayCast.activeSelf)
        {
            return;
        }

        foreach (GameObject panel in panels)
        {
            if (panel.activeSelf)
            {
                return;
            }
        }
        
        if (Input.GetButtonDown("Cancel"))
        {
            quitPanel.Show();
        }
    }
}