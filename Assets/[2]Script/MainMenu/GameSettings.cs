﻿// using Firebase.Database;

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

//Хранит все игровые настройки
public class GameSettings : GenericSingletonClass<GameSettings>
{
    [Header("audio settings")] public AudioMixer audioMixer;
    [SerializeField] public AchiveManager achiveManager;

    private const string KEY_SOUND_VOLUME = "settings.sound.volume";
    private const string KEY_MUSIC_VOLUME = "settings.music.volume";

    private const string DIFFICULTY_SAVE = "toggleSelect";
    private const string START_DIFFICULTY = "isActiveDifficulty";

    private const string KEY_HELPER = "helperKey";
    private const string KEY_LANG = "languageKey";
    private const string KEY_AVATAR = "avatarKey";
    
    private const string KEY_CURRENT_LEVEL = "CurrentLevelNums";
    private const string KEY_CLASSIC_LEVEL = "CurrentLevelClassic";
 
    // public DataSnapshot CurrentSnapshot { get; set; }
    public List<RangItem> RangUsers { get; set; } = new List<RangItem>(); //  ранг для юзеров
    public List<RangItem> RangUsersClassic { get; set; } = new List<RangItem>(); //  ранг для юзеров
    public string CurrentBdName { get; set; }
    
    public DifficultyEnum CurrentDifficulty { get; set; } // тут текущая сложность при старте игры
    public int CurrentLevel { get; set; } // текущий уровень который не прошел
    public int CurrentLevelClassic { get; set; } // текущий уровень который не прошел
    public bool isHelperOn { get; set; }
    public bool isAvatarOn { get; set; }
    public bool isLangueRu { get; set; }
    public int First { get; set; }
    
    // Achieve
    public int RowBonusAchieve { get; set; }
    public int NumBonusAchieve { get; set; }
    public int TimeBonusAchieve { get; set; }
    public int RoundBonusAchieve { get; set; }
    public int BluesBonusAchieve { get; set; }
    public int GreenBonusAchieve { get; set; }
    public int RedBonusAchieve { get; set; }
    public int PvpAchieve { get; set; }

    public void SaveRowBonusAchieve()
    {
        PlayerPrefs.SetInt("RowBonusAchieve",RowBonusAchieve);
    }
    
    public void SaveNumBonusAchieve()
    {
        PlayerPrefs.SetInt("NumBonusAchieve",NumBonusAchieve);
    }
    
    public void SaveTimeBonusAchieve()
    {
        PlayerPrefs.SetInt("TimeBonusAchieve",TimeBonusAchieve);
    }
    
    public void SaveRoundBonusAchieve()
    {
        PlayerPrefs.SetInt("RoundBonusAchieve",RoundBonusAchieve);
    }
    
    public void SaveBlueBonusAchieve()
    {
        PlayerPrefs.SetInt("BluesBonusAchieve",BluesBonusAchieve);
    }
    
    public void SaveGreenBonusAchieve()
    {
        PlayerPrefs.SetInt("GreenBonusAchieve",GreenBonusAchieve);
    }
    
    public void SaveRedBonusAchieve()
    {
        PlayerPrefs.SetInt("RedBonusAchieve",RedBonusAchieve);
    }

    public void SavePvpAchieve()
    {
        PlayerPrefs.SetInt("PvpAchieve",PvpAchieve);
    }

    public void InitAchieve()
    {
        LoadAchieve("RowBonusAchieve");
        LoadAchieve("NumBonusAchieve");
        LoadAchieve("TimeBonusAchieve");
        LoadAchieve("RoundBonusAchieve");
        LoadAchieve("BluesBonusAchieve");
        LoadAchieve("GreenBonusAchieve");
        LoadAchieve("RedBonusAchieve");
        LoadAchieve("PvpAchieve");
    }

    private void LoadAchieve(string achieve)
    {
        PlayerPrefs.GetInt(achieve);
    }

    public override void Awake()
    {
        First = LoadFirstGame();
    }
    private void Start()
    {
        InitPreferences();
        First = LoadFirstGame();
    }
    
    private int LoadFirstGame()
    {
        return PlayerPrefs.GetInt("firstInitGame");
    }
    
    public void SaveLevel(int levelNum)
    {
         PlayerPrefs.SetInt(KEY_CURRENT_LEVEL, levelNum);
         CurrentLevel = levelNum;
    }
    
    public void SaveLevelClassic(int levelNum)
    {
        PlayerPrefs.SetInt(KEY_CLASSIC_LEVEL, levelNum);
        CurrentLevelClassic = levelNum;
    }
    
    public void LoadLevel()
    {
        CurrentLevel = PlayerPrefs.GetInt(KEY_CURRENT_LEVEL,1);
    }
    
    public void LoadLevelClassic()
    {
        CurrentLevelClassic = PlayerPrefs.GetInt(KEY_CLASSIC_LEVEL,23);
    }
    
    public static void SaveSound(float volumeSave)
    {
        PlayerPrefs.SetFloat(KEY_SOUND_VOLUME, volumeSave);
    }

    public static void SaveMusic(float volumeSave)
    {
        PlayerPrefs.SetFloat(KEY_MUSIC_VOLUME, volumeSave);
    }

    public static void SaveHelper(int num)
    {
        PlayerPrefs.SetInt(KEY_HELPER, num);
    }

    public static void SaveLanguage(int num)
    {
        PlayerPrefs.SetInt(KEY_LANG, num);
    }
    
    public static void SaveAvatar(int num)
    {
        PlayerPrefs.SetInt(KEY_AVATAR, num);
    }


    public static float LoadKeySound()
    {
        return PlayerPrefs.GetFloat(KEY_SOUND_VOLUME,0.5f);
    }

    public static float LoadKeyMusic()
    {
        return PlayerPrefs.GetFloat(KEY_MUSIC_VOLUME, 0.5f);
    }

    public static int LoadHelper()
    {
        return PlayerPrefs.GetInt(KEY_HELPER);
    }

    public static int LoadLanguege()
    {
        return PlayerPrefs.GetInt(KEY_LANG);
    }
    
    public static int LoadAvatar()
    {
        return PlayerPrefs.GetInt(KEY_AVATAR);
    }



    public void SelectDifficulty(DifficultyEnum difficultyEnum)
    {
        CurrentDifficulty = difficultyEnum;
    }
 
    public void SaveOptionsPreferences(DifficultyEnum difficultyEnum) //выбор сложности
    {
        // isDifficultyStart = 1;
        switch (difficultyEnum)
        {
            case DifficultyEnum.EASY:
                PlayerPrefs.SetInt(DIFFICULTY_SAVE, 0);
                break;
            case DifficultyEnum.NORMAL:
                PlayerPrefs.SetInt(DIFFICULTY_SAVE, 1);
                break;
            case DifficultyEnum.HARD:
                PlayerPrefs.SetInt(DIFFICULTY_SAVE, 2);
                break;
        }
    }

    public void SavePrefHelper()
    {
        if (isHelperOn)
        {
            SaveHelper(0);
        }
        else
        {
            SaveHelper(1);
        }

        if (isLangueRu)
        {
            SaveLanguage(1);
        }
        else
        {
            SaveLanguage(0);
        }
        
        if (isAvatarOn)
        {
            SaveAvatar(0);
        }
        else
        {
            SaveAvatar(1);
        }
    }

    private void InitPreferences() // загрузить все настройки при старте игры
    {
        InitAchieve();
        LoadKeyMusic();
        LoadKeySound();
        LoadDifficulty();
        LoadHelperStatus();
        LoadLevel();
    }
 
    private void LoadHelperStatus()
    {
        int shinyKey = LoadHelper();
        int starsKey = LoadLanguege();
        int avatar = LoadAvatar();
        
        if (avatar == 0)
        {
            isAvatarOn = true;
        }
        else
        {
            isAvatarOn = false;
        }
        
        if (shinyKey == 0)
        {
            isHelperOn = true;
        }
        else
        {
            isHelperOn = false;
        }

        if (starsKey == 0)
        {
            isLangueRu = false;
        }
        else
        {
            isLangueRu = true;
        }
    }

    private void LoadDifficulty()
    {
        int select = PlayerPrefs.GetInt(DIFFICULTY_SAVE);
        DifficultyEnum difficulty = (DifficultyEnum) select;

        switch (difficulty)
        {
            case DifficultyEnum.EASY:
                CurrentDifficulty = DifficultyEnum.EASY;
                break;
            case DifficultyEnum.NORMAL:
                CurrentDifficulty = DifficultyEnum.NORMAL;
                break;
            case DifficultyEnum.HARD:
                CurrentDifficulty = DifficultyEnum.HARD;

                break;
        }
    }
}