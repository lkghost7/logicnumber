﻿using DG.Tweening;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class ControlMainMenu : GenericSingletonClass<ControlMainMenu>
{
    public Slider soundSlider;
    public Slider musicSlider;
    public CanvasGroup panelOptions;
    public DifficultyControl difficultyControl;
    public ShinyOptionsControl shinyControl;
    public ControlStarsEffect controlStarsEffect;
    public CanvasGroup fade;
    public StarsManager starsManager;
    public Button[] mainButtons;

    private float soundVolume;
    private float musicVolume;
 
    private void Start()
    {
        PhotonNetwork.Disconnect();
        UnBlockMainBtn();
        if (soundSlider != null)
        {
            soundVolume = GameSettings.LoadKeySound();
            musicVolume = GameSettings.LoadKeyMusic();

            soundSlider.value = soundVolume;
            musicSlider.value = musicVolume;

            ChangeVolumeSound(soundVolume);
            ChangeVolumeMusic(musicVolume);
        }
    }

    public void ChangeVolumeSound(float newVolume)
    {
        soundVolume = newVolume;
        float correctVolumeSound = Mathf.Log10(newVolume) * 20;
        GameSettings.Instance.audioMixer.SetFloat("soundV", correctVolumeSound);
    }

    public void ChangeVolumeMusic(float newVolume)
    {
        musicVolume = newVolume;
        float correctVolumeMusic = Mathf.Log10(newVolume) * 20;
        GameSettings.Instance.audioMixer.SetFloat("musicV", correctVolumeMusic);
    }
 
    public void ExitSaveVolume()
    { 
        GameSettings.SaveSound(soundVolume);
        GameSettings.SaveMusic(musicVolume);
        
        GameSettings.Instance.SavePrefHelper();
        // GameSettings.Instance.SaveOptionsPreferences();
    }

    public void BlockMainBtn()
    {
        foreach (Button mainButton in mainButtons)
        {
            mainButton.interactable = false;
        }
    }

    public void UnBlockMainBtn()
    {
        foreach (Button mainButton in mainButtons)
        {
            mainButton.interactable = true;
        }
    }

    public void ShowOptions()
    {
        BlockMainBtn();
        controlStarsEffect.Show();
        shinyControl.Show();
        panelOptions.gameObject.SetActive(true);
        difficultyControl.Show();
        panelOptions.alpha = 0;
        panelOptions.DOFade(1, 0.4f);
    }

    public void HideOptions()
    {
        UnBlockMainBtn();
        panelOptions.DOFade(0, 0.4f).OnComplete(() => { panelOptions.gameObject.SetActive(false); });
    }

    public void HideLevelMenu(SceneIndex sceneIndex)
    {
        fade.GetComponent<Image>().color = Color.black;
        fade.DOFade(1, 0.7f).OnComplete(() =>
        {
            starsManager._camera.gameObject.SetActive(false);
            starsManager.mainCanvas.gameObject.SetActive(false);

            if (LoadingScene.Instance.TutorialIsDone == 0)
            {
                LoadingScene.Instance.ShowСurtainMainMenu(SceneIndex.TUTORIAL);
            }
            else
            {
                LoadingScene.Instance.ShowСurtainMainMenu(sceneIndex);
            }
        });
    }
    
    public void HideLevelPVP(SceneIndex sceneIndex)
    {
        fade.GetComponent<Image>().color = Color.black;
        fade.DOFade(1, 0.7f).OnComplete(() =>
        {
            starsManager._camera.gameObject.SetActive(false);
            starsManager.mainCanvas.gameObject.SetActive(false);
            
            LoadingScene.Instance.ShowСurtainPvp(sceneIndex);
        });
    }
 
    public void RestTutorial()
    {
        LoadingScene.Instance.ActiveLoadingCanvas();
        fade.GetComponent<Image>().color = Color.black;
        fade.DOFade(1, 0.7f).OnComplete(() =>
        {
            starsManager._camera.gameObject.SetActive(false);
            starsManager.mainCanvas.gameObject.SetActive(false);

            LoadingScene.Instance.ShowСurtainMainMenu(SceneIndex.TUTORIAL);
        });
    }
}