﻿using Coffee.UIExtensions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelRankUi : MonoBehaviour
{
    public TextMeshProUGUI levelNum;
    public GameObject[] ranks;
    public GameObject border;
    public GameObject lockLevel;
    public GameObject fade;
    public Button button;
       
    public void Init(int num)
    {
        levelNum.text = num.ToString();
        button.onClick.AddListener(() =>
        {
            button.interactable = false;
            print("onClick");
            LoadLevel(num);
        });

        if (GameSettings.Instance.CurrentLevel == num)
        {
            button.GetComponent<UIShiny>().enabled = true;
            border.SetActive(true);
            button.interactable = true;
        }
         
        if (GameSettings.Instance.CurrentLevel < num)
        {
            button.GetComponent<UIShiny>().enabled = false;
            lockLevel.SetActive(true);
            levelNum.gameObject.SetActive(false);
            button.interactable = false;
        }
        
        if (GameSettings.Instance.CurrentLevel > num)
        {
            button.GetComponent<UIShiny>().enabled = false;
            fade.SetActive(true);
            button.interactable = false;
        }
        
        SetRank(num -1);
    }

    private void SetRank(int level)
    {
        if (GameManager.Instance.CurrentRank[level] == 1)
        {
            ranks[2].SetActive(true);
        }
        
        if (GameManager.Instance.CurrentRank[level] == 2)
        {
            ranks[2].SetActive(true);
            ranks[1].SetActive(true);
        }
        
        if (GameManager.Instance.CurrentRank[level] == 3)
        {
            ranks[2].SetActive(true);
            ranks[1].SetActive(true);
            ranks[0].SetActive(true);
        }
    }
 
    private void LoadLevel(int level)
    {
        LoadingScene.Instance.ActiveLoadingCanvas();
        ControlMainMenu.Instance.HideLevelMenu((SceneIndex)level+1);
    }
}
