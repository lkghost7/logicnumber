﻿using DG.Tweening;
using UnityEngine;

public class BlinkButton : MonoBehaviour
{
    [SerializeField] private CanvasGroup button;

    public void FadeOn()
    {
        button.gameObject.SetActive(true);
        button.alpha = 0;
    }

    public void FadeOf()
    {
        button.alpha = 0;
        button.gameObject.SetActive(false);
    }

    public void StartBlinkBtn()
    {
        BlinkOn();
    }
    
    private void BlinkOn()
    {
        button.DOFade(1, 1).OnComplete(() =>
        {
            BlinkOf();
        });
    }
 
    private void BlinkOf()
    {
        button.DOFade(0, 1).OnComplete(() =>
        {
            BlinkOn();
        });
    }
}
