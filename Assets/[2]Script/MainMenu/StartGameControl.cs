﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class StartGameControl : MonoBehaviour
{  
    [SerializeField][Tooltip("задержка показа шторки")] private float delayCurtain = 3;
    [SerializeField] private TurnControl turnControl;
    [SerializeField][Tooltip("уровень для своей сцены")] public int sceneLvl = 2;
    [SerializeField][Tooltip("камера")] private Camera camera;
    [SerializeField][Tooltip("задний фон")] GameObject backGround;
    [SerializeField][Tooltip("Canvas")] GameObject canvas;
    [SerializeField][Tooltip("Canvas")] public bool uniq = true;

    [SerializeField] private CanvasGroup fadeOn;
    
    [SerializeField][Tooltip("GameScene")] public SceneIndex sceneIndex;
    [SerializeField][Tooltip("debuff level")] public DebuffLevel debuffLevel;
    [SerializeField] private HelperBtn helperBtn;
    [SerializeField] public bool isClassic;
    [SerializeField] public bool isPVP;
    [SerializeField] public bool isPVPSolo;

    private void Start()
    {
        TimeManager.Instance.StopTimer = true;
        
        if (isPVP)
        {
            canvas.gameObject.SetActive(true);
            camera.gameObject.SetActive(true);
            backGround.SetActive(true);
            StartCoroutine(InitGame());
            return;
        }
         
        camera.gameObject.SetActive(false);
        backGround.gameObject.SetActive(false);
        canvas.gameObject.SetActive(false);
        
        LoadingScene.Instance.SaveDifficultyLoading();
        LoadingScene.Instance.SaveTutorialStatus(true);
        
        helperBtn.LoadHelper();
    }

    public void CloseCurtain()
    {
        canvas.gameObject.SetActive(true);
        camera.gameObject.SetActive(true);
        backGround.SetActive(true);
        fadeOn.alpha = 1;
        fadeOn.DOFade(0, 0.6f).OnComplete(() =>
        {
            StartCoroutine(InitGame());
            GameManager.Instance.prepareStart.Show(debuffLevel);
        });
    }

    private void EndGame()
    {
        fadeOn.alpha = 0;
        fadeOn.DOFade(1, 0.5f).OnComplete(() =>
        {
            canvas.gameObject.SetActive(false);
            camera.gameObject.SetActive(false);
            backGround.SetActive(false);
            LoadingScene.Instance.ShowСurtain(sceneIndex);
        });
    }
    
    public void ReloadGame()
    {
        fadeOn.alpha = 0;
        fadeOn.DOFade(1, 0.4f).OnComplete(() =>
        {
            canvas.gameObject.SetActive(false);
            camera.gameObject.SetActive(false);
            backGround.SetActive(false);
            LoadingScene.Instance.ShowСurtainReload(sceneIndex);
        });
    }
     
    public void ExitMainMenu()
    {
        fadeOn.alpha = 0;
        fadeOn.DOFade(1, 0.5f).OnComplete(() =>
        {
            canvas.gameObject.SetActive(false);
            camera.gameObject.SetActive(false);
            backGround.SetActive(false);
            LoadingScene.Instance.ShowСurtainLoadMenu(sceneIndex);
        });
    }
    
    IEnumerator InitGame(bool isTutor = false)
    {
        yield return new WaitForSecondsRealtime(0);
        turnControl.Init();
        turnControl.StartInit();
        GameManager.Instance.InitStart(isTutor);
    }

    public void ReloadLvl()
    {
        ReloadGame();
    }
      
    public void LoadMainMenu()
    {
        ExitMainMenu();
    }

    public void LoadFirstLevel()
    {
        fadeOn.alpha = 0;
        fadeOn.DOFade(1, 0.5f).OnComplete(() =>
        {
            canvas.gameObject.SetActive(false);
            camera.gameObject.SetActive(false);
            backGround.SetActive(false);
            LoadingScene.Instance.ShowСurtainFirstLevel(sceneIndex);
        });
    } 
    
    public void LoadNextLvl()
    {
        EndGame();
    }
}