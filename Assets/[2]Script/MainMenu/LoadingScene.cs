﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Localization;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;
 
public class LoadingScene : GenericSingletonClass<LoadingScene>
{
    [SerializeField][Tooltip("шторка сам канвас что бы не мешал")] private CanvasGroup canvasLoading = null;
    [SerializeField][Tooltip("шторка панель")] private CanvasGroup loadingBackGrounds = null;
    [SerializeField] private CanvasGroup fadeOn;
    [SerializeField] private BlinkButton buttonStart;
    [SerializeField] private GameObject stars;
    [SerializeField] private SliderRunTo1 sliderToRun;
    [SerializeField] private TextMeshProUGUI trick;
    [SerializeField] private LeanLocalization localization;
    public bool isPvp;

    public Action<float> ButtonBlink;
    public int CurrenDifficulty { get; set; } = 1; // стоит ли сложность вобще
    public int TutorialIsDone { get; set; } = 0; // пройден ли тутор

    private AsyncOperation loadOperation;
 
    public override void Awake()
    {
        if (isPvp)
        {
         return;   
        }
        
        base.Awake();
        SceneManager.LoadSceneAsync((int) SceneIndex.MAIN_MENU, LoadSceneMode.Additive);
        ButtonBlink += ButtonOn;
        canvasLoading.gameObject.SetActive(false); // выключить канвас для оптимизации
        LoadDifficulty();
        LoadTutorialStatus();
        // trick.text = "ff";
    }

    public void ChangeLocalizationEngOn()
    {
        localization.SetCurrentLanguage("English");
    }
    
    public void ChangeLocalizationRuOn()
    {
        localization.SetCurrentLanguage("Russian");
    }
    
    public void LoadTutorialStatus()
    {
        TutorialIsDone = PlayerPrefs.GetInt("TutorialStatus");
    }
    
    public void SaveTutorialStatus(bool isDone)
    {
        if (isDone)
        {
            TutorialIsDone = 1;
            PlayerPrefs.SetInt("TutorialStatus", 1); 
        }
        else
        {
            TutorialIsDone = 0;
            PlayerPrefs.SetInt("TutorialStatus", 0); 
        }
    }

    public void SaveDifficultyLoading()
    {
        PlayerPrefs.SetInt("isActiveDifficulty", 1);
        CurrenDifficulty  = 1;
    }
    
    public void LoadDifficulty()
    {
        CurrenDifficulty = PlayerPrefs.GetInt("isActiveDifficulty"); // активировона  ли сложность
    }
    
    public void SaveDefaultDiff()
    {
        PlayerPrefs.SetInt("isActiveDifficulty", 0);
        CurrenDifficulty  = 0;
    }

    public void ActiveLoadingCanvas()
    {
        canvasLoading.gameObject.SetActive(true);
    }

    public void FadeOfMainStart()
    {
        buttonStart.FadeOf();
        fadeOn.alpha = 1;
        fadeOn.GetComponent<Image>().color = Color.white;
        fadeOn.DOFade(0, 0.6f).OnComplete(() =>
        {
            stars.SetActive(true);
            UnloadMainMenu();
            StartSlider();
        });
    }
    
    public void FadeLevelStart()
    {
        // buttonStart.FadeOf();
        // fadeOn.alpha = 1;
        // fadeOn.GetComponent<Image>().color = Color.white;
        fadeOn.DOFade(0, 0.6f).OnComplete(() =>
        {
            stars.SetActive(true);
            UnloadMainMenu();
            StartSlider();
        });
    }

    private void ButtonOn(float end)
    {
        buttonStart.FadeOn();
        buttonStart.StartBlinkBtn();
    }
    
    public void PressStartGame()
    {
        stars.SetActive(false);
        buttonStart.FadeOf();
        fadeOn.GetComponent<Image>().color = Color.black;

        fadeOn.DOFade(1, 0.5f).OnComplete(() =>
        {
            FindObjectOfType<StartGameControl>().CloseCurtain();
            canvasLoading.gameObject.SetActive(false);
        });
    }

    public void ShowСurtain(SceneIndex index)
    {
        LoadRandomTrick();
        slider.value = 0;
        canvasLoading.gameObject.SetActive(true);
        fadeOn.alpha = 1;
        fadeOn.GetComponent<Image>().color = Color.black;
        buttonStart.FadeOf();
        fadeOn.DOFade(0, 0.5f).OnComplete(() =>
        {
            stars.SetActive(true);
            StartSlider();
            UnloadLevel(index);
        });
    }
    
    public void ShowСurtainLoadMenu(SceneIndex index)
    {
        LoadRandomTrick();
        fadeOn.GetComponent<Image>().color = Color.black;
        canvasLoading.gameObject.SetActive(true);
        fadeOn.alpha = 1;
        UnloadStageFailForMenu(index);
        // fadeOn.DOFade(0, 0.5f);
    }
     
    public void ShowСurtainFirstLevel(SceneIndex index)
    {
        LoadRandomTrick();
        slider.value = 0;
        canvasLoading.gameObject.SetActive(true);
        fadeOn.alpha = 1;
        fadeOn.GetComponent<Image>().color = Color.black;
        buttonStart.FadeOf();
        fadeOn.DOFade(0, 0.5f).OnComplete(() =>
        {
            stars.SetActive(true);
            StartSlider();
            UnloadTutorial((SceneIndex)GameSettings.Instance.CurrentLevel+1);
        });
    }
    
    public void ShowСurtainReload(SceneIndex index)
    {
        LoadRandomTrick();
        slider.value = 0;
        canvasLoading.gameObject.SetActive(true);
        fadeOn.alpha = 1;
        fadeOn.GetComponent<Image>().color = Color.black;
        ReLoadStageFail(index);

        fadeOn.DOFade(1, 0.5f).OnComplete(() =>
        {
            FindObjectOfType<StartGameControl>().CloseCurtain();
            canvasLoading.gameObject.SetActive(false);
        });
    }
     
    public void ShowСurtainMainMenu(SceneIndex sceneIndex) // выгрузить мейн меню и загрузить левел
    {
        LoadRandomTrick();
        slider.value = 0;
        canvasLoading.gameObject.SetActive(true);
        fadeOn.alpha = 1;
        fadeOn.GetComponent<Image>().color = Color.black;
        buttonStart.FadeOf();
        fadeOn.DOFade(0, 1f).OnComplete(() =>
        {
            stars.SetActive(true);
            StartSlider();
            UnloadLevelForMainMenu(sceneIndex);
        });
    }
    
    public void ShowСurtainPvp(SceneIndex sceneIndex) // выгрузить PVP
    {
        LoadRandomTrick();
        slider.value = 0;
        canvasLoading.gameObject.SetActive(true);
        fadeOn.alpha = 1;
        fadeOn.GetComponent<Image>().color = Color.black;
        buttonStart.FadeOf();
        fadeOn.DOFade(0, 1f).OnComplete(() =>
        {
            stars.SetActive(true);
            StartSlider();
            UnloadLevelForPVP(sceneIndex);
        });
    }

    public void LoadRandomTrick()
    {
        List<string> tricks = new List<string>();
        tricks.Add(LeanLocalization.GetTranslationText("load.trick1"));
        tricks.Add(LeanLocalization.GetTranslationText("load.trick2"));
        tricks.Add(LeanLocalization.GetTranslationText("load.trick3"));
        tricks.Add(LeanLocalization.GetTranslationText("load.trick4"));
        tricks.Add(LeanLocalization.GetTranslationText("load.trick5"));
        tricks.Add(LeanLocalization.GetTranslationText("load.trick6"));
        tricks.Add(LeanLocalization.GetTranslationText("load.trick7"));
        tricks.Add(LeanLocalization.GetTranslationText("load.trick8"));
        
        int randomTrick = Random.Range(0, tricks.Count);
        trick.text = tricks[randomTrick];
    }
    
    public void ReLoadStageFail(SceneIndex sceneIndexLoad)  // выгрузить текущую сцену на свою же
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int) SceneIndex.LOADING_SCENE));
        
        if (SceneManager.GetSceneByBuildIndex((int) sceneIndexLoad).isLoaded) //
        {
            StartCoroutine(UnloadSceneOperationLevel((int)sceneIndexLoad,sceneIndexLoad)); 
        }
    }
    
    public void UnloadStageFailForMenu(SceneIndex sceneIndexLoad)  // выгрузить текущую сцену
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int) SceneIndex.LOADING_SCENE));
        if (SceneManager.GetSceneByBuildIndex((int) sceneIndexLoad).isLoaded) // если загружено меню
        {
            StartCoroutine(UnloadSceneOperationLevel((int)sceneIndexLoad,SceneIndex.MAIN_MENU)); // загрузить меин меню
        }
    }
     
    public void UnloadLevelForMainMenu(SceneIndex sceneIndexLoad)  // выгрузить из левел меню нужную сцену
    {

        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int) SceneIndex.LOADING_SCENE));
        if (SceneManager.GetSceneByBuildIndex((int) SceneIndex.MAIN_MENU).isLoaded) // если загружено меню
        {
            StartCoroutine(UnloadSceneOperationLevel((int)SceneIndex.MAIN_MENU,sceneIndexLoad)); // загрузить сцену и выгрузить предыдущую 
        }
    }
    
    public void UnloadLevelForPVP(SceneIndex sceneIndexLoad)  // выгрузить PVP
    {
        print("UnloadLevelForPVP");
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int) SceneIndex.LOADING_SCENE));
        if (SceneManager.GetSceneByBuildIndex((int) SceneIndex.MAIN_MENU).isLoaded) // если загружено меню
        {
            StartCoroutine(UnloadSceneOperationPVP((int)SceneIndex.MAIN_MENU,sceneIndexLoad)); // загрузить сцену и выгрузить предыдущую 
        }
    }

    public void UnloadLevel(SceneIndex sceneIndex)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int) SceneIndex.LOADING_SCENE));
        if (SceneManager.GetSceneByBuildIndex((int) sceneIndex).isLoaded) // если загружена
        {
            int currentIndex = (int) sceneIndex;
            SceneIndex sceneIndexNext = (SceneIndex) currentIndex + 1;
            StartCoroutine(UnloadSceneOperationLevel((int)sceneIndex,sceneIndexNext)); // загрузить сцену и выгрузить предыдущую 
        }
    }
    
    public void UnloadTutorial(SceneIndex nextLevel)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int) SceneIndex.LOADING_SCENE));
        if (SceneManager.GetSceneByBuildIndex((int) SceneIndex.TUTORIAL).isLoaded) // если загружена
        {
            StartCoroutine(UnloadSceneOperationLevel((int)SceneIndex.TUTORIAL,nextLevel)); // загрузить сцену и выгрузить предыдущую 
        }
    }

    private void UnloadMainMenu()
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex((int) SceneIndex.LOADING_SCENE));
        if (SceneManager.GetSceneByBuildIndex((int) SceneIndex.MAIN_MENU).isLoaded)
        {
            StartCoroutine(UnloadSceneOperation(SceneIndex.MAIN_MENU));
        }
    }

    IEnumerator LoadSceneOperation(SceneIndex sceneIndex)
    {
        loadOperation = SceneManager.LoadSceneAsync((int) sceneIndex, LoadSceneMode.Additive);

        while (!loadOperation.isDone)
        {
            yield return new WaitForSeconds(0.1f);
        }
    }
    
    IEnumerator LoadSceneOperationPVP(SceneIndex sceneIndex)
    {
        print("LoadSceneOperationPVP");
        // loadOperation = SceneManager.LoadSceneAsync((int) sceneIndex, LoadSceneMode.Additive);
        // PhotonNetwork.LoadLevelLk("LevelPvp1");
        
        // while (!loadOperation.isDone)
        // {
        //     yield return new WaitForSeconds(0.1f);
        // }
        yield return null;
    }
    
    public IEnumerator UnloadSceneOperationLevel(int sceneUnload, SceneIndex sceneIndexLoad)
    {
        AsyncOperation operation = SceneManager.UnloadSceneAsync(sceneUnload); // выгрузить меин меню

        while (!operation.isDone)
        {
            yield return null;
        }

        StartCoroutine(LoadSceneOperation(sceneIndexLoad)); // как все сделано загрузить первый левел
    }
    
    public IEnumerator UnloadSceneOperationPVP(int sceneUnload, SceneIndex sceneIndexLoad)
    {
        print("UnloadSceneOperationPVP");
        AsyncOperation operation = SceneManager.UnloadSceneAsync(sceneUnload); // выгрузить меин меню

        while (!operation.isDone)
        {
            yield return null;
        }

        StartCoroutine(LoadSceneOperationPVP(sceneIndexLoad)); // к
    }

    IEnumerator UnloadSceneOperation(SceneIndex sceneIndex)
    {
        AsyncOperation operation = SceneManager.UnloadSceneAsync((int) sceneIndex); // выгрузить меин меню

        while (!operation.isDone)
        {
            yield return null;
        }

        StartCoroutine(LoadSceneOperation(SceneIndex.LEVEL_ONE)); // как все сделано загрузить первый левел
    }
    
    public void ShowDelayСurtain(float time)
    {
        loadingBackGrounds.alpha = 0;
        canvasLoading.gameObject.SetActive(true);
        loadingBackGrounds.DOFade(1, time);
    }
    
    private bool isBase = false;
    private bool isStart = false;
    public Slider slider;
    public float speed = 0.5f;

    float time = 0f;

    private void StartSlider()
    {
        time = 0;
        isStart = true;
        isBase = true;
    }

    public void DefaultValue()
    {
        slider.value = 0;
    }

    void Update()
    {
        if (!isStart)
        {
            return;
        }
        
        if (isBase)
        {
            time += Time.deltaTime * speed;
            slider.value = time;

            if (time > 1)
            {
                ButtonBlink.Invoke(0);
                isStart = false;
                isBase = false;
                time = 0;
            }
        }
    }
}