﻿using UnityEngine;

public class CheatPanel : MonoBehaviour
{
    [SerializeField] private Transform parentMagic;
    [SerializeField] private MagicCheat cheatMagicPrefab;

    public void AddLife()
    {
        BonusManager.Instance.AddLife();
    }

    public void LostLife()
    {
        BonusManager.Instance.LostLife();
    }
    
    public void LostLife2()
    {
        GameManager.Instance.LostLifeActionClassic.Invoke(0);
    }
    
    public void AddBonus()
    {
        GameManager.Instance.AddBonusAction.Invoke(0);
    }
    
    public void LostBonus()
    {
        GameManager.Instance.LostBonusAction.Invoke(0);
    }

    public void ClearAll()
    {
        BonusManager.Instance.UnBlockAll();
    }

    public void FreezeRaw()
    {
        BonusManager.Instance.FreezeOneRaw(0);
    } 

    public void UnFreezeRaw()
    {
        BonusManager.Instance.UnFreezeOneRaw();
    }

    public void LockCell()
    {
        BonusManager.Instance.FreezeCells(4);
    }
 
    public void UnLockCell()
    {
        BonusManager.Instance.UnBlockCellBtn(0);
    }
    
    public void UnBlockMagic()
    {
        BonusManager.Instance.UnBlockMagicBtn(0);
    }

    public void ShowTimeBonus()
    {
        BonusManager.Instance.ActiveTimeBonusBtn();
    }

    public void AddExpLvl()
    {
        
        ExpManager.Instance.AddLevelExp(8);
    }
    
    public void AddExpTime()
    {
        ExpManager.Instance.AddTimeExp();
    }
    
    public void AddExpNum()
    {
        ExpManager.Instance.AddNumExp(6);
    }
    
    public void AddExpRaw()
    {
        ExpManager.Instance.AddRawExp();
    }

    public void ShowTLevelUp()
    {
        BonusManager.Instance.levelUp.Show();
    }
    
    public void BtnCancelDebaf() 
    {
        BonusManager.Instance.ButtonCancelDebuf(0);
    }
    
    public void BtnEmptyDebaf() 
    {
        BonusManager.Instance.ButtonEmptyDebuf(0);
    }
    
    public void ViewMagicNum()  // вызвать панель для выбора одного числа
    {
        GameManager.Instance.bonusMagicNum.Show();
    }

    public void StopTimer()
    {
        print("stop timer");
        TimeManager.Instance.StopTimer = true;
    }
    
    public void StartTimer()
    {
        print("stop timer");
        TimeManager.Instance.StopTimer = false;
    }

    public void ExpLevelPlus()
    {
        print("ExpLevelPlus");
        ExpManager.Instance.CurrentExpWandLvl++;
    }
    
    public void ExpLevelMinus()
    {
        print("ExpLevelMinus");
        ExpManager.Instance.CurrentExpWandLvl--;
    }
    
    public void ExpTimePlus()
    {
        print("ExpTimePlus");
        ExpManager.Instance.CurrentExpWandTime++;
    }
    
    public void ExpTimeMinus()
    {
        print("ExpTimeMinus");
        ExpManager.Instance.CurrentExpWandTime--;
    }
    
    public void ExpNumPlus()
    {
        print("ExpNumPlus");
        ExpManager.Instance.CurrentExpWandNum++;
    }
    
    public void ExpNumMinus()
    {
        print("ExpNumMinus");
        ExpManager.Instance.CurrentExpWandNum--;
    }
    
    public void ExpRawPlus()
    {
        print("ExpRawPlus");
        ExpManager.Instance.CurrentExpWandRaw++;
    }
    
    public void ExpRawMinus()
    {
        print("ExpRawMinus");
        ExpManager.Instance.CurrentExpWandRaw--;
    }
    
    public void ClearRawDebaf()
    {
        print("ClearRawDebaf");
        BonusManager.Instance.ClearRawDebaf(0);
    }
    
    // public void SetDefaultGame()
    // {
    //     // SaveManager.Instance.InitDefault();
    //     // // GameManager.Instance.isDefaultGame = true;
    //     // print("SetDefaultGame");
    //     // // GameManager.Instance.SetDefaultNewGame();
    // }

    public void ShowWinnerPanel()
    {
        GameManager.Instance.winnerPanel.ShowStatsPanel();
    }

    public void DebufChangeNun()
    {
        BonusManager.Instance.DebufChangeNun(0);
    }
    
    public void DebufColorWhite()
    {
        BonusManager.Instance.DebufColorWhite(0);
    }
    
    public void ShowBonusResultOneNum()
    {
        BonusManager.Instance.ShowBonusResultOneNum(0);
    }

    public void ShowAddTimeBonus()
    {
        BonusManager.Instance.ShowAddTimeBonus(0);
    }
    
    public void ShowAddRowBonus()
    {
        BonusManager.Instance.ShowAddRowBonus(0);
    }
    
    public void DebufCatastrohpe()
    {
        BonusManager.Instance.DebufCatastrohpe(0);
    }

    public void GetDada()
    {
        // GameManager.Instance.dataBaseRef.GetTableData(); //  загрузить из бд новые данные
    }
    
    public void AddScore()
    {
        ExpManager.Instance.CurrentRoundExp += 5000;
    }
    
    
    public void UnBlockClearAll()
    {
        BonusManager.Instance.UnBlockClearBtn(0);
    }
    
    public void ShowCellBTNLock()
    {
        BonusManager.Instance.BlockRawBtn(0);
    }
    
    public void ShowCellBTNUnLock()
    {
        BonusManager.Instance.UnBlockRawBtn(0);
    }
    
    public void VictoryPanel()
    {
        GameManager.Instance.victoryPanelRef.Show();
    }
    
    public void ShowMagicNum()
    {
        GameManager.ClearParent(parentMagic);
        
        int[] magic = GameManager.Instance.turnControl.magicNumber;

        foreach (int i in magic)
        {
            MagicCheat mcheat = Instantiate(cheatMagicPrefab, parentMagic);
            mcheat.num.text = i.ToString();
        }
    }

    public void InitGame()
    {
        // FindObjectOfType<StartGameControl>().CloseCurtain();
        GameManager.Instance.controlRankTable.tableMain.Show();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            gameObject.SetActive(false);
        }
    }
}