﻿
public enum ExpRawType 
{
    NONE,
    LEVEL_EXP,
    TIME_EXP,
    NUM_EXP,
    RAW_EXP
}
