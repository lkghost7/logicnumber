﻿public enum AchieveType
{
    ROW_BONUS,
    NUM_BONUS,
    TIME_BONUS,
    ROUND_BONUS,
    BLUE_BONUS,
    GREEN_BONUS,
    RED_BONUS,
    PVP
}
