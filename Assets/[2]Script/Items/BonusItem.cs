﻿public struct BonusItem
{
    public int Id { get; set; }
    public string Name { get; set; }
    
    public BonusEnum bonusType { get; set; }
    public int PositionRoulette { get; set;}
    public int PositionNum{ get; set;}
    public int Chance { get; set; }
    public string Description { get; set; }
    public string NameDescription { get; set; }
    public bool isBuf;
}
