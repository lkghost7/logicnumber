﻿using Lean.Localization;
using TMPro;
using UnityEngine;

public class BonusExp : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI bonus;
    
    public void Show()
    {
        gameObject.SetActive(true);
        int num = Random.Range(0, 5);
        
        switch (num)
        {
            case 1:
                AddTimeWand();
                bonus.text = LeanLocalization.GetTranslationText("rank.time");
                break;
            
            case 2:
                AddNumWand();
                bonus.text = LeanLocalization.GetTranslationText("rank.num");
                break;
            
            case 3:
                AddRawWand();
                bonus.text = LeanLocalization.GetTranslationText("rank.raw");
                break;
            
            case 4:
                AddRoundBonusWand();
                bonus.text = LeanLocalization.GetTranslationText("rank.round");
                break;
        }
        
    }
    
    public void AddTimeWand()
    {
        print("AddTimeWand");
        ExpManager.Instance.WandCountToLevelTime += 1;
        SaveManager.Instance.SaveRound("WandCountToLevelTime", ExpManager.Instance.WandCountToLevelTime);
    }

    public void AddNumWand()
    {
        print("AddNumWand");
        ExpManager.Instance.WandCountToLevelNum += 1;
        SaveManager.Instance.SaveRound("WandCountToLevelNum", ExpManager.Instance.WandCountToLevelNum);
    }

    public void AddRawWand()
    {
        print("AddRawWand");
        ExpManager.Instance.WandCountToLevelRaw += 1;
        SaveManager.Instance.SaveRound("WandCountToLevelRaw", ExpManager.Instance.WandCountToLevelRaw);

    }

    public void AddRoundBonusWand()
    {
        print("AddRoundBonusWand");
        ExpManager.Instance.WandCountToLevelRoundBonus +=1;
        SaveManager.Instance.SaveRound("WandCountToLevelRoundBonus", ExpManager.Instance.WandCountToLevelRoundBonus);
    }
}
