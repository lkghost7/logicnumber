﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using UnityEngine;
using UnityEngine.UI;

public class EnemyRowControl : MonoBehaviour
{
    [SerializeField] private Image check;
    [SerializeField] private Transform rowTransform;
    [SerializeField] private GameObject crossPrefab;
    [SerializeField] private GameObject checkMarkPrefab;
 
    public void Show(int mark, int cross)
    {
        for (int i = 0; i < mark; i++)
        {
            LeanPool.Spawn(checkMarkPrefab, rowTransform); 
        }

        for (int i = 0; i < cross; i++)
        {
            LeanPool.Spawn(crossPrefab, rowTransform);
        }

        check.gameObject.SetActive(true);
        
        GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.winPvpCheck, check.transform.position, Quaternion.identity);
        StartCoroutine( GameManager.Instance.effector.Despawner(effectBum, 2));
    }

}
