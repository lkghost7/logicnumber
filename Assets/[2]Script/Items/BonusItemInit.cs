﻿using System.Collections.Generic;
using System.Linq;
using Lean.Localization;
using UnityEngine;
 
public class BonusItemInit : MonoBehaviour
{
    private const int ONE_SECTOR = 2160;
    private const int TWO_SECTOR = 2205;
    private const int TREE_SECTOR = 2250;
    private const int FOUR_SECTOR = 2295;
    private const int FIVE_SECTOR = 2340;
    private const int SIX_SECTOR = 2385;
    private const int SEVEN_SECTOR = 2430;
    private const int EIGHT_SECTOR = 2475;

    public Dictionary<string, BonusItem> BonusItemDictionary { get; set; } = new Dictionary<string, BonusItem>(); // тут 16 проинитаных бонусов.

    public List<int> sectorList = new List<int>(); // лист с секторами

    private void SaveChanceBonus(string key)
    {
        foreach (KeyValuePair<string, BonusItem> pair in BonusItemDictionary)
        {
            SaveManager.Instance.SaveRound(pair.Key, pair.Value.Chance);
        }
    }

    private void InitBonus() // инитает в начале раунда и обновляет при переходе на следущий левел
    {
        BonusItem bonusItem1 = new BonusItem();
        bonusItem1.isBuf = true;
        bonusItem1.Name = "Life";
        bonusItem1.NameDescription = LeanLocalization.GetTranslationText("bonus.LifeDescriptionName");
        bonusItem1.Description = LeanLocalization.GetTranslationText("bonus.LifeDescription");
        bonusItem1.bonusType = BonusEnum.ADD_LIFE;
        bonusItem1.PositionRoulette = 0;
        bonusItem1.Chance = SaveManager.Instance.LoadRound("Life");
        bonusItem1.PositionNum = 0;

        BonusItem bonusItem2 = new BonusItem();
        bonusItem2.isBuf = false;
        bonusItem2.Name = "Lock cell";
        bonusItem2.NameDescription = LeanLocalization.GetTranslationText("bonus.LockCellDescriptionName");
        bonusItem2.Description = LeanLocalization.GetTranslationText("bonus.LockCellDescription");
        bonusItem2.bonusType = BonusEnum.LOCK_CELL;
        bonusItem2.PositionRoulette = 0;
        bonusItem2.Chance = SaveManager.Instance.LoadRound("Lock cell");
        bonusItem2.PositionNum = 0;

        BonusItem bonusItem3 = new BonusItem();
        bonusItem3.isBuf = true;
        bonusItem3.Name = "Unlock cell";
        bonusItem3.NameDescription = LeanLocalization.GetTranslationText("bonus.UnLockCellDescriptionName");
        bonusItem3.Description = LeanLocalization.GetTranslationText("bonus.UnLockCellDescription");
        bonusItem3.bonusType = BonusEnum.UNLOCK_CELL;
        bonusItem3.PositionRoulette = 0;
        bonusItem3.Chance = SaveManager.Instance.LoadRound("Unlock cell");
        bonusItem3.PositionNum = 0;

        BonusItem bonusItem4 = new BonusItem();
        bonusItem4.isBuf = false;
        bonusItem4.Name = "Freeze row";
        bonusItem4.NameDescription = LeanLocalization.GetTranslationText("bonus.FreezeRowDescriptionName");
        bonusItem4.Description = LeanLocalization.GetTranslationText("bonus.FreezeRowDescription");
        bonusItem4.bonusType = BonusEnum.FREEZE_ROW;
        bonusItem4.PositionRoulette = 0;
        bonusItem4.Chance = SaveManager.Instance.LoadRound("Freeze row");
        bonusItem4.PositionNum = 0;

        BonusItem bonusItem5 = new BonusItem();
        bonusItem5.isBuf = true;
        bonusItem5.Name = "Unfreeze row";
        bonusItem5.NameDescription = LeanLocalization.GetTranslationText("bonus.UnfreezeRowDescriptionName");
        bonusItem5.Description = LeanLocalization.GetTranslationText("bonus.UnfreezeRowDescription");
        bonusItem5.bonusType = BonusEnum.UNFREEZE_ROW;
        bonusItem5.PositionRoulette = 0;
        bonusItem5.Chance = SaveManager.Instance.LoadRound("Unfreeze row");
        bonusItem5.PositionNum = 0;

        BonusItem bonusItem6 = new BonusItem();
        bonusItem6.isBuf = false;
        bonusItem6.Name = "Lock dell";
        bonusItem6.NameDescription = LeanLocalization.GetTranslationText("bonus.LockDellBtnDescriptionName");
        bonusItem6.Description = LeanLocalization.GetTranslationText("bonus.LockDellBtnDescription");
        bonusItem6.bonusType = BonusEnum.LOCK_DELL;
        bonusItem6.PositionRoulette = 0;
        bonusItem6.Chance = SaveManager.Instance.LoadRound("Lock dell");
        bonusItem6.PositionNum = 0;

        BonusItem bonusItem7 = new BonusItem();
        bonusItem7.isBuf = true;
        bonusItem7.Name = "Magic num";
        bonusItem7.NameDescription = LeanLocalization.GetTranslationText("bonus.UnLockMagicBtnDescriptionName");
        bonusItem7.Description = LeanLocalization.GetTranslationText("bonus.UnLockMagicBtnDescription");
        bonusItem7.bonusType = BonusEnum.MAGIC_NUM;
        bonusItem7.PositionRoulette = 0;
        bonusItem7.Chance = SaveManager.Instance.LoadRound("Magic num");
        bonusItem7.PositionNum = 0;

        BonusItem bonusItem8 = new BonusItem();
        bonusItem8.isBuf = false;
        bonusItem8.Name = "Lock empty";
        bonusItem8.NameDescription = LeanLocalization.GetTranslationText("bonus.LockEmptyBtnDescriptionName");
        bonusItem8.Description = LeanLocalization.GetTranslationText("bonus.LockEmptyBtnDescription");
        bonusItem8.bonusType = BonusEnum.LOCK_EMPTY;
        bonusItem8.PositionRoulette = 0;
        bonusItem8.Chance = SaveManager.Instance.LoadRound("Lock empty");
        bonusItem8.PositionNum = 0;

        BonusItem bonusItem9 = new BonusItem();
        bonusItem9.isBuf = true;
        bonusItem9.Name = "Clear all";
        bonusItem9.NameDescription = LeanLocalization.GetTranslationText("bonus.UnLockClearBtnDescriptionName");
        bonusItem9.Description = LeanLocalization.GetTranslationText("bonus.UnLockClearBtnDescription");
        bonusItem9.bonusType = BonusEnum.CELAR_ALL;
        bonusItem9.PositionRoulette = 0;
        bonusItem9.Chance = SaveManager.Instance.LoadRound("Clear all");
        bonusItem9.PositionNum = 0;

        BonusItem bonusItem10 = new BonusItem();
        bonusItem10.isBuf = false;
        bonusItem10.Name = "Random num";
        bonusItem10.NameDescription = LeanLocalization.GetTranslationText("bonus.RandomNumDescriptionName");
        bonusItem10.Description = LeanLocalization.GetTranslationText("bonus.RandomNumDescription");
        bonusItem10.bonusType = BonusEnum.RANDOM_NUM;
        bonusItem10.PositionRoulette = 0;
        bonusItem10.Chance = SaveManager.Instance.LoadRound("Random num");
        bonusItem10.PositionNum = 0;

        BonusItem bonusItem11 = new BonusItem();
        bonusItem11.isBuf = true;
        bonusItem11.Name = "See number";
        bonusItem11.NameDescription = LeanLocalization.GetTranslationText("bonus.SeeAllDescriptionName");
        bonusItem11.Description = LeanLocalization.GetTranslationText("bonus.SeeAllDescription");
        bonusItem11.bonusType = BonusEnum.SEE_NUMBER;
        bonusItem11.PositionRoulette = 0;
        bonusItem11.Chance = SaveManager.Instance.LoadRound("See number");
        bonusItem11.PositionNum = 0;

        BonusItem bonusItem12 = new BonusItem();
        bonusItem12.isBuf = false;
        bonusItem12.Name = "White all";
        bonusItem12.NameDescription = LeanLocalization.GetTranslationText("bonus.WhiteAllDescriptionName");
        bonusItem12.Description = LeanLocalization.GetTranslationText("bonus.WhiteAllDescription");
        bonusItem12.bonusType = BonusEnum.WHITE_ALL;
        bonusItem12.PositionRoulette = 0;
        bonusItem12.Chance = SaveManager.Instance.LoadRound("White all");
        bonusItem12.PositionNum = 0;

        BonusItem bonusItem13 = new BonusItem();
        bonusItem13.isBuf = true;
        bonusItem13.Name = "Time exp+";
        bonusItem13.NameDescription = LeanLocalization.GetTranslationText("bonus.TimeExpDescriptionName");
        bonusItem13.Description = LeanLocalization.GetTranslationText("bonus.TimeExpDescription");
        bonusItem13.bonusType = BonusEnum.TIME_EXP;
        bonusItem13.PositionRoulette = 0;
        bonusItem13.Chance = SaveManager.Instance.LoadRound("Time exp+");
        bonusItem13.PositionNum = 0;

        BonusItem bonusItem14 = new BonusItem();
        bonusItem14.isBuf = false;
        bonusItem14.Name = "Hide row";
        bonusItem14.NameDescription = LeanLocalization.GetTranslationText("bonus.HideRowDescriptionName");
        bonusItem14.Description = LeanLocalization.GetTranslationText("bonus.HideRowDescription");
        bonusItem14.bonusType = BonusEnum.HIDE_ROW;
        bonusItem14.PositionRoulette = 0;
        bonusItem14.Chance = SaveManager.Instance.LoadRound("Hide row");
        bonusItem14.PositionNum = 0;

        BonusItem bonusItem15 = new BonusItem();
        bonusItem15.isBuf = true;
        bonusItem15.Name = "Row exp+";
        bonusItem15.NameDescription = LeanLocalization.GetTranslationText("bonus.RowExpDescriptionName");
        bonusItem15.Description = LeanLocalization.GetTranslationText("bonus.RowExpDescription");
        bonusItem15.bonusType = BonusEnum.ROW_EXP;
        bonusItem15.PositionRoulette = 0;
        bonusItem15.Chance = SaveManager.Instance.LoadRound("Row exp+");
        bonusItem15.PositionNum = 0;

        BonusItem bonusItem16 = new BonusItem();
        bonusItem16.isBuf = false;
        bonusItem16.Name = "Сatastrophe";
        bonusItem16.NameDescription = LeanLocalization.GetTranslationText("bonus.СatastropheDescriptionName");
        bonusItem16.Description = LeanLocalization.GetTranslationText("bonus.СatastropheDescription");
        bonusItem16.bonusType = BonusEnum.CATASTROPHE;
        bonusItem16.PositionRoulette = 0;
        bonusItem16.Chance = SaveManager.Instance.LoadRound("Сatastrophe");
        bonusItem16.PositionNum = 0;

        BonusItem bonusItem17 = new BonusItem();
        bonusItem17.isBuf = true;
        bonusItem17.Name = "Level+";
        bonusItem17.NameDescription = LeanLocalization.GetTranslationText("bonus.LevelUpDescriptionName");
        bonusItem17.Description = LeanLocalization.GetTranslationText("bonus.LevelUpDescription");
        bonusItem17.bonusType = BonusEnum.LEVEL_UP;
        bonusItem17.PositionRoulette = 0;
        bonusItem17.Chance = SaveManager.Instance.LoadRound("Level+");
        bonusItem17.PositionNum = 0;

        BonusItem bonusItem18 = new BonusItem();
        bonusItem18.isBuf = false;
        bonusItem18.Name = "bonus18";
        bonusItem18.NameDescription = "bonus18";
        bonusItem18.Description = "bonus18";
        bonusItem18.bonusType = BonusEnum.BONUS18;
        bonusItem18.PositionRoulette = 0;
        bonusItem18.Chance = SaveManager.Instance.LoadRound("bonus18");
        bonusItem18.PositionNum = 0;

        BonusItemDictionary.Add(bonusItem1.Name, bonusItem1);
        BonusItemDictionary.Add(bonusItem2.Name, bonusItem2);
        BonusItemDictionary.Add(bonusItem3.Name, bonusItem3);
        BonusItemDictionary.Add(bonusItem4.Name, bonusItem4);
        BonusItemDictionary.Add(bonusItem5.Name, bonusItem5);
        BonusItemDictionary.Add(bonusItem6.Name, bonusItem6);
        BonusItemDictionary.Add(bonusItem7.Name, bonusItem7);
        BonusItemDictionary.Add(bonusItem8.Name, bonusItem8);
        BonusItemDictionary.Add(bonusItem9.Name, bonusItem9);
        BonusItemDictionary.Add(bonusItem10.Name, bonusItem10);
        BonusItemDictionary.Add(bonusItem11.Name, bonusItem11);
        BonusItemDictionary.Add(bonusItem12.Name, bonusItem12);
        BonusItemDictionary.Add(bonusItem13.Name, bonusItem13);
        BonusItemDictionary.Add(bonusItem14.Name, bonusItem14);
        BonusItemDictionary.Add(bonusItem15.Name, bonusItem15);
        BonusItemDictionary.Add(bonusItem16.Name, bonusItem16);
        BonusItemDictionary.Add(bonusItem17.Name, bonusItem17);
        BonusItemDictionary.Add(bonusItem18.Name, bonusItem18);
    }

    public void Start()
    {
        sectorList.Add(ONE_SECTOR);
        sectorList.Add(TWO_SECTOR);
        sectorList.Add(TREE_SECTOR);
        sectorList.Add(FOUR_SECTOR);
        sectorList.Add(FIVE_SECTOR);
        sectorList.Add(SIX_SECTOR);
        sectorList.Add(SEVEN_SECTOR);
        sectorList.Add(EIGHT_SECTOR);

        InitBonus();
        BonusManager.Instance.UpdateBonus += SaveChanceBonus;
    }

    public Dictionary<int, BonusItem> Init()
    {
        int maxNum = 17;
        int amountNum = 4;

        Dictionary<int, BonusItem> bonusItems = new Dictionary<int, BonusItem>(); // временный для пула бафов
        List<BonusItem> currentBonusList = new List<BonusItem>();
        List<int> uniqueNumList = new List<int>();

        foreach (KeyValuePair<string, BonusItem> pair in BonusManager.Instance.bonusItemInit.BonusItemDictionary)
        {
            currentBonusList.Add(pair.Value);
        }

        int[] uniqueNumOdd = Helper.SetUniqueArrayOdd(amountNum, maxNum, 0); // нечетное то 1 3 5 итд // дебафы
        int[] uniqueNumEven = Helper.SetUniqueArrayEven(amountNum, maxNum, 0); // четное от 0 2 4 итд // бафы

        uniqueNumList.Add(uniqueNumEven[0]);
        uniqueNumList.Add(uniqueNumOdd[0]);
        uniqueNumList.Add(uniqueNumEven[1]);
        uniqueNumList.Add(uniqueNumOdd[1]);
        uniqueNumList.Add(uniqueNumEven[2]);
        uniqueNumList.Add(uniqueNumOdd[2]);
        uniqueNumList.Add(uniqueNumEven[3]);
        uniqueNumList.Add(uniqueNumOdd[3]);

        for (int i = 0; i < 8; i++)
        {
            BonusItem randomBonus = currentBonusList[uniqueNumList[i]];
            randomBonus.PositionRoulette = sectorList[i];
            bonusItems.Add(i, randomBonus);
        }

        // bonusItems = bonusItems.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

        return bonusItems;
    }
}

// 20 + 10% 30 + 20% 40 +30% 50 + 40% 60 + 50%
// 9 - 10% 8 - 20% 7 - 30% 6 - 40% 5 - 50%