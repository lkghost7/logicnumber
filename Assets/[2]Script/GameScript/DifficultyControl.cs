﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyControl : MonoBehaviour
{
    [SerializeField] private Toggle easy, normal, hard;
    
    public void Show()
    {
        switch (GameSettings.Instance.CurrentDifficulty)
        {
            case DifficultyEnum.EASY:
                easy.isOn = true;
                normal.isOn = false;
                hard.isOn = false;
                break;
            case DifficultyEnum.NORMAL:
                normal.isOn = true;
                easy.isOn = false;
                hard.isOn = false;
                break;
            case DifficultyEnum.HARD:
                hard.isOn = true;
                normal.isOn = false;
                easy.isOn = false;
                break;
        }
    }

    public void ToggleEasySelect()
    {
        if (easy.isOn)
        {
            GameSettings.Instance.SelectDifficulty(DifficultyEnum.EASY);
        }
    }

    public void ToggleNormalSelect()
    {
        if (normal.isOn)
        {
            GameSettings.Instance.SelectDifficulty(DifficultyEnum.NORMAL);
        }
    }

    public void ToggleHardSelect()
    {
        if (hard.isOn)
        {
            GameSettings.Instance.SelectDifficulty(DifficultyEnum.HARD);
        }
    }
}