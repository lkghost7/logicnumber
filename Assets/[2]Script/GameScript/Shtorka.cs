﻿using Coffee.UIExtensions;
using DG.Tweening;
using UnityEngine;

public class Shtorka : MonoBehaviour
{
  [SerializeField] private CanvasGroup shtorka;
  [SerializeField] private UIShiny uiShiny;

  public void Show()
  {
    shtorka.alpha = 1;
  }

  public void Hide()
  {
    shtorka.DOFade(0, 1.5f).OnComplete(() =>
    {
      uiShiny.Play();
    });
  }
  
}
