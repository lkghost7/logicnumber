﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Localization;
using UnityEngine;

public class TutorialManager : GenericSingletonClass<TutorialManager>
{
    [SerializeField] private StartGameControl startGameControl;
    [SerializeField] private TutorialPreparePanel tutorialPreparePanel;
    [SerializeField] public CanvasGroup buttonTurnEnd;
    [SerializeField] public CanvasGroup bonusRouletePanel;
    [SerializeField] public CanvasGroup winnerPanel;

    [SerializeField] private TutorControl[] acts;
    [SerializeField] private List<string> descriptionActs;

    public void HideWinnerPanel()
    {
        winnerPanel.DOFade(0, 1).OnComplete(() =>
        {
            winnerPanel.gameObject.SetActive(false);
        });
    }
 
    private void Start()
    {
        // InitDescription();
        Init();
        ShowTutor();
    }

    public void LoadFirstLevel() 
    {
        startGameControl.LoadFirstLevel();
    }
    
    public void LoadMainMenu() 
    {
        startGameControl.ExitMainMenu();
    }
 
    public void TutorialIsDone()
    {
        LoadingScene.Instance.SaveTutorialStatus(true);
    }

    public void ShowTutor()
    {
        tutorialPreparePanel.Show();
        
    }

    public void ShowNextAct(int actNum)
    {
        buttonTurnEnd.alpha = 0.65f;

        acts[actNum].Show(descriptionActs[actNum]);
    }
    
    private void Init()
    {
        for (int i = 1; i < 26; i++)
        {
            string sufix = "tutor.descriptionAct";
            string prefix = i.ToString();
            string tutor = sufix + prefix;
            string localization = LeanLocalization.GetTranslationText(tutor);
            descriptionActs.Add(localization);
        }
    }
}
