﻿using System;
using UnityEngine;
using GooglePlayGames;

public class AchiveManager : MonoBehaviour
{
    private string leaderBoard = "CgkInN-xw8IJEAIQAg";
    public string Score20 { get; private set; } = "CgkInN-xw8IJEAIQAw";
    public string Score50 { get; private set; } = "CgkInN-xw8IJEAIQAQ";
    public string Score80 { get; private set; } = "CgkInN-xw8IJEAIQBw";
    public string Score120 { get; private set; } = "CgkInN-xw8IJEAIQCA";
    public string Score150 { get; private set; } = "CgkInN-xw8IJEAIQCQ";

    private const string rowBonusId = "CgkInN-xw8IJEAIQBA";
    private const string numBonusId = "CgkInN-xw8IJEAIQBg";
    private const string timeBonusId = "CgkInN-xw8IJEAIQBQ";

    private const string blueBonusId = "CgkInN-xw8IJEAIQDQ";
    private const string greenBonusId = "CgkInN-xw8IJEAIQDg";
    private const string redBonusId = "CgkInN-xw8IJEAIQDw";

    private const string roundBonus1Id = "CgkInN-xw8IJEAIQCg";
    private const string roundBonus2Id = "CgkInN-xw8IJEAIQCw";
    private const string roundBonus3Id = "CgkInN-xw8IJEAIQDA";

    private const string pvpBonusId = "CgkInN-xw8IJEAIQGQ";
    private const string classicId = "CgkInN-xw8IJEAIQGg";

    private const string easyId = "CgkInN-xw8IJEAIQFg";
    private const string normalId = "CgkInN-xw8IJEAIQFw";
    private const string hardId = "CgkInN-xw8IJEAIQGA";

    private const string numRound7Id = "CgkInN-xw8IJEAIQEw";
    private const string numRound5Id = "CgkInN-xw8IJEAIQFA";
    private const string numRound3Id = "CgkInN-xw8IJEAIQFQ";

    private const string level10 = "CgkInN-xw8IJEAIQEA";
    private const string level20 = "CgkInN-xw8IJEAIQEQ";
    private const string level30 = "CgkInN-xw8IJEAIQEg";

    private int scoreGame;
    public Action<string> AchieveAction;
    public Action<int, AchieveType> AchieveStatusAction;

    public void ShowAchievement(string nameAchieve)
    {
        Social.ReportProgress(nameAchieve, 100.0, success => { });
    }

    private void Start()
    {
        AchieveAction += ShowAchievement;
        AchieveStatusAction += ShowAchieveStatus;

        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();

        Social.localUser.Authenticate(succes =>
        {
            if (succes)
            {
                print(" succes");
            }
            else
            {
                print("no socces");
            }
        });
    }

    public void AddLeaderBoard(int score)
    {
        Social.ReportScore(score, leaderBoard, (bool success) => { });
    }

    public void ShowLeader() // показать таблицу лидеров
    {
        print("ShowLeader");
        Social.ShowLeaderboardUI();
    }

    public void ShowAchive() // показать все ачивки
    {
        print("ShowAchive");
        Social.ShowAchievementsUI();
    }


    public void ShowAchieveStatus(int statAchieve, AchieveType achieveType)
    {
        switch (achieveType)
        {
            case AchieveType.ROW_BONUS:

                if (statAchieve >= 250)
                {
                    ShowAchievement(rowBonusId);
                }

                break;
            case AchieveType.NUM_BONUS:

                if (statAchieve >= 250)
                {
                    ShowAchievement(numBonusId);
                }

                break;
            case AchieveType.TIME_BONUS:

                if (statAchieve >= 250)
                {
                    ShowAchievement(timeBonusId);
                }

                break;

            case AchieveType.BLUE_BONUS:

                if (statAchieve >= 300)
                {
                    ShowAchievement(blueBonusId);
                }

                break;
            case AchieveType.GREEN_BONUS:

                if (statAchieve >= 300)
                {
                    ShowAchievement(greenBonusId);
                }

                break;
            case AchieveType.RED_BONUS:

                if (statAchieve >= 300)
                {
                    ShowAchievement(redBonusId);
                }

                break;
        }
    }

    public void ChekVictoryAchieve(DifficultyEnum status)
    {
        switch (status)
        {
            case DifficultyEnum.EASY:
                ShowAchievement(easyId);
                break;
            case DifficultyEnum.NORMAL:
                ShowAchievement(normalId);
                break;
            case DifficultyEnum.HARD:
                ShowAchievement(hardId);
                break;
        }
    }

    public void ChekLevelAchieve(int level, DifficultyEnum difficulty)
    {

        if (difficulty == DifficultyEnum.EASY || difficulty == DifficultyEnum.NORMAL)
        {
            return;
        }
  
        if (level >= 30)
        {
            ShowAchievement(level30);
            return;
        }

        if (level >= 20)
        {
            ShowAchievement(level20);
            return;
        }

        if (level >= 10)
        {
            ShowAchievement(level10);
        }
    }

    public void ChekRowNumAchieve(int rowNum, int numLimit)
    {
        print(numLimit + "numLimit");

        if (numLimit != 5)
        {
            print("numLimit return");
            return;
        }
        
        if (rowNum +1 <= 3)
        {
            ShowAchievement(numRound3Id);
            print("ShowAchievement1");
        }

        if (rowNum +1 <= 5)
        {
            ShowAchievement(numRound5Id);
            print("ShowAchievement2");
        }

        if (rowNum +1 <= 7)
        {
            ShowAchievement(numRound7Id);
            print("ShowAchievement3");
        }
    }

    public void ChekRoundBonusAchieve(int status)
    {
        if (status >= 50)
        {
            ShowAchievement(roundBonus3Id);
            return;
        }

        if (status >= 30)
        {
            ShowAchievement(roundBonus2Id);
            return;
        }

        if (status >= 10)
        {
            ShowAchievement(roundBonus1Id);
        }
    }

    public void CheckPvpStatus(int status)
    {
        if (status >= 5)
        {
            ShowAchievement(pvpBonusId);
        }
    }
    
    public void CheckClassicStatus()
    {
        ShowAchievement(classicId);
    }
}