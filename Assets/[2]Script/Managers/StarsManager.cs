﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class StarsManager : MonoBehaviour
{
    [SerializeField] private ParticleSystem starsEffect;
    [SerializeField] private CanvasGroup fadePanel;
    [SerializeField] private LoadingManager loadingManager;
    [SerializeField] public Camera _camera;
    [SerializeField] public CanvasGroup mainCanvas;
    [SerializeField] private LevelControlPanel levelMenu;
    [SerializeField] private GameObject levelMenuParent;
    [SerializeField] private LevelClassicPanel levelClassic;
    [SerializeField] private PhotonManager levelLobby;
    [SerializeField] private GameObject lobbyPvp;
    [SerializeField] private GameObject levelClassicParent;
    [SerializeField] private GameObject mainMenuParent;
    [SerializeField] private GameObject blockRaycastPanel;
     
    float time = 0f;
    private bool isLerpStart = false;
    private float speedLerp = 1f;

    private float lerpSpeedScale = 0f;
    private float minSpeedScale = 0.004f;
    private float maxSpeedScale = 0.1f;

    private float lerpStartSize = 0f;
    private float minStartSize = 0.2f;
    private float maxStartSize = 2f;

    private float lerpSimulationSpeed = 0;
    private float minSimulationSpeed = 0.06f;
    private float maxSimulationSpeed = 0.8f;
    

    private ParticleSystemRenderer velocityStars;
    private ParticleSystem.MainModule mainModule;

    private float delayStatCuratin = 0.3f;

    private void Start()
    {
        time = 0;
        velocityStars = starsEffect.GetComponent<ParticleSystemRenderer>();
        lerpSpeedScale = minSpeedScale;
        lerpStartSize = minStartSize;
        lerpSimulationSpeed = minSimulationSpeed;

        mainModule = starsEffect.main;
    }
    
    public void StartLevelClassic()
    {
        blockRaycastPanel.SetActive(true);
        ControlMainMenu.Instance.BlockMainBtn();
        isLerpStart = true;
        Sequence sequenceFade = DOTween.Sequence();
        sequenceFade.SetDelay(1.7f);

        fadePanel.GetComponent<Image>().color = Color.white;
        sequenceFade.Append(fadePanel.DOFade(1f, 0.7f)).OnComplete(() =>
        {
            DefaultEffect();
            ShowLevelClassic();
            mainMenuParent.SetActive(false);
            levelClassicParent.SetActive(true);
            levelClassic.Show();
        });
    }
    
    public void StartLobby()
    {
        lobbyPvp.SetActive(true);
        
        
        // blockRaycastPanel.SetActive(true);
        // ControlMainMenu.Instance.BlockMainBtn();
        // isLerpStart = true;
        // Sequence sequenceFade = DOTween.Sequence();
        // sequenceFade.SetDelay(1.7f);
        //
        // fadePanel.GetComponent<Image>().color = Color.white;
        // sequenceFade.Append(fadePanel.DOFade(1f, 0.7f)).OnComplete(() =>
        // {
        //     DefaultEffect();
        //     ShowLobby();
        //     mainMenuParent.SetActive(false);
        //     levelLobby.Show();
        // });
    }
    
    public void StartEffectStars()
    {
        blockRaycastPanel.SetActive(true);
        ControlMainMenu.Instance.BlockMainBtn();
        isLerpStart = true;
        Sequence sequenceFade = DOTween.Sequence();
        sequenceFade.SetDelay(1.7f);

        fadePanel.GetComponent<Image>().color = Color.white;
        sequenceFade.Append(fadePanel.DOFade(1f, 0.7f)).OnComplete(() =>
        {
           
            DefaultEffect();
            ShowLevelMenu();
            mainMenuParent.SetActive(false);
            levelMenuParent.SetActive(true);
            levelMenu.Show();
        });
    }
 
    private void ShowLevelMenu()
    {
        fadePanel.DOFade(0, 0.7f).OnComplete(() =>
        {
            levelMenu.ShowDifficulty();
            blockRaycastPanel.SetActive(false);
        });
    }
    
    private void ShowLevelClassic()
    {
        fadePanel.DOFade(0, 0.7f).OnComplete(() =>
        {
            // levelMenu.ShowDifficulty();
            blockRaycastPanel.SetActive(false);
        });
    }
    
    private void ShowLobby()
    {
        fadePanel.DOFade(0, 0.7f).OnComplete(() =>
        {
            // levelMenu.ShowDifficulty();
            blockRaycastPanel.SetActive(false);
        });
    }

    public void ReturnMainMenu()
    {
        ControlMainMenu.Instance.UnBlockMainBtn();
        // fadePanel.alpha = 1;
        fadePanel.GetComponent<Image>().color = Color.black;
        fadePanel.DOFade(1, 0.6f).OnComplete(() =>
        {
            fadePanel.DOFade(0, 0.6f);
            mainMenuParent.SetActive(true);
            levelMenuParent.SetActive(false);
            levelClassicParent.SetActive(false);
        });
    }

    public void LevelChose()
    {
        // LoadingScene.Instance.FadeOfMainStart();
        // _camera.gameObject.SetActive(false);
        // mainCanvas.gameObject.SetActive(false);

        fadePanel.DOFade(1, 0.6f).OnComplete(() =>
        {
            LoadingScene.Instance.FadeOfMainStart();
            _camera.gameObject.SetActive(false);
            mainCanvas.gameObject.SetActive(false);
        });
    }

    public void DefaultEffect()
    {
        time = 0;
        lerpSimulationSpeed = minSimulationSpeed;
        lerpStartSize = minStartSize;
        lerpSpeedScale = minSpeedScale;

        isLerpStart = false;

        mainModule.simulationSpeed = minSimulationSpeed;
        mainModule.startSize = new ParticleSystem.MinMaxCurve(minStartSize, 0.1f);
        velocityStars.velocityScale = minSpeedScale;
    }

    // public void StopForceStars()
    // {
    //     isLerpStart = false;
    //     lerpSpeedScale = minSpeedScale;
    //     lerpStartSize = minStartSize;
    //     lerpSimulationSpeed = minSimulationSpeed;
    // }

    private void Update()
    {
        if (!isLerpStart)
        {
            return;
        }

        if (lerpSpeedScale >= maxSpeedScale)
        {
            return;
        }

        time += speedLerp * Time.deltaTime;

        lerpSimulationSpeed = Mathf.Lerp(minSimulationSpeed, maxSimulationSpeed, time);
        mainModule.simulationSpeed = lerpSimulationSpeed;
        
        // lerpStartSize = Mathf.Lerp(minStartSize, maxStartSize, time2);
        mainModule.startSize = new ParticleSystem.MinMaxCurve(maxStartSize, 0.1f);
 
        //speed scale
        lerpSpeedScale = Mathf.Lerp(minSpeedScale, maxSpeedScale, time);
        velocityStars.velocityScale = lerpSpeedScale;
    }
}