﻿using System;
using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using Lean.Pool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BonusManager : GenericSingletonClass<BonusManager>
{
    [Header("Config")] [SerializeField] [Tooltip("время сколько вращается рулетка")]
    public float duration = 4;

    [SerializeField] private TurnControl turnControl;
    [Header("Magic Num")] [SerializeField] private Button[] magickButtons;
    [SerializeField] private Button[] unFreezeCellButtons;
    [SerializeField] private Button[] unFreezeRawButtons;
    [SerializeField] private Button unFreezeClearButton;

    [Header("Debaf Control")] public ShinyButtonControl buttonCancel;
    public ShinyButtonControl buttonEmpty;
    [Header("Bonus Panels")] public BonusItemInit bonusItemInit;
    public LevelUp levelUp;
    [SerializeField] private BonusResultOneNum _bonusResultOneNum;
    public Dictionary<int, BonusItem> bonusItems { get; private set; } = new Dictionary<int, BonusItem>(); // текущий словарь для ротации.
    public List<LuckyBonusItem> LuckyBonusItemsListMiddle { get; set; } = new List<LuckyBonusItem>();
    public List<LuckyBonusItem> LuckyBonusItemsListSmall { get; set; } = new List<LuckyBonusItem>();
    public List<LuckyBonusItem> LuckyBonusItemsListLarge { get; set; } = new List<LuckyBonusItem>();
    private bool isActiveMagicNumBonus;

    public int CellBtnAmount { get; set; } // сколько кнопок на начало раунда досутпно анблок целс
    public int RawBtnAmount { get; set; } // сколько кнопок на начало раунда досутпно рав анблок
    public int MagicBtnAmount { get; set; } // сколько кнопок на начало раунда досутпно меджик анблок
    public int ClearBtnAmount { get; set; } // кнопка очистеть все.

    public Action<BonusItem> BonusChose { get; private set; } //какой именно бонус выбран
    public Action<string> UpdateBonus { get; set; } //обновить бонусы из сейва, актуальные для текущей игры.
    
    public Action<int> FreezeCellAction { get; set; } // дебаф замарозить ячейку
    public Action<int> FreezeRawAction { get; set; } // дебаф замарозить ряд
    
    public Action<int> UnLockCellBtnAction { get; set; } // кнопка Cell разблокирована 
    public Action<int> UnLockRawBtnAction { get; set; } // кнопка Raw разблокирована 
    
    public Action<int> UnLockMagicBtnAction { get; set; } // кнопка Magic разблокирована 
    public Action<int> UnLockClearBtnAction { get; set; } // кнопка Clear разблокирована 
    
    public Action<int> LockDellBtnAction { get; set; } // кнопка Clear разблокирована 
    public Action<int> LockEmptyBtnAction { get; set; } // кнопка Clear разблокирована 
    
    public Action<int> RandomNumAction { get; set; } // рандомная цифра 
    public Action<int> WhiteAllAction { get; set; } // все цифры белые
    
    public Action<int> ResultNumAction { get; set; } // показать одну цифру случаную
    
    public Action<int> TimeExpAction { get; set; } // добавить к тайм бонкс +1
    public Action<int> RawExpAction { get; set; } // добавить к рав бонус +1
    public Action<int> RawLvlAction { get; set; } // добавить к lvl бонус +10
    public Action<int> HideRowAction { get; set; } // спрятать цифры в ряду
    public Action<int> CatastropheAction { get; set; } // спрятать цифры в ряду
 
    private void StartBonus(BonusItem bonusItem)
    {
        switch (bonusItem.bonusType)
        {
            case BonusEnum.ADD_LIFE:
                StartCoroutine(ShowLifeBonus(bonusItem)); // баф добавить жизнь. 1
                break;
            case BonusEnum.LOCK_CELL:
                StartCoroutine(ShowFreezeCell(bonusItem)); // Дебаф замарозить ячейку 2
                break;
            case BonusEnum.UNLOCK_CELL:
                StartCoroutine(ShowUnLockCell(bonusItem)); // баф размарозить ячейку 3
                break;
            case BonusEnum.FREEZE_ROW:
                StartCoroutine(ShowLockRaw(bonusItem)); // Дебаф замарозить ряд 4
                break;
            case BonusEnum.UNFREEZE_ROW:
                StartCoroutine(ShowUnLockRaw(bonusItem)); // баф разамарозить ряд 5
                break;
            case BonusEnum.LOCK_DELL:
                StartCoroutine(ShowLockDellBtn(bonusItem));   // Дебаф залокать кнопку удалить 6
                break;
            case BonusEnum.MAGIC_NUM:
                StartCoroutine(ShowUnLockMagic(bonusItem)); // баф анлокнуть кнопку мэджик 7
                break;
            case BonusEnum.LOCK_EMPTY:
                StartCoroutine(ShowLockEmptyBtn(bonusItem)); // Дебаф залокать кнопку пустышка 8
                break;
            case BonusEnum.CELAR_ALL:
                StartCoroutine(ShowUnClearMagic(bonusItem)); // баф анлокнуть кнопку клеар 9
                break;
            case BonusEnum.RANDOM_NUM:
                StartCoroutine(ShowRandomNum(bonusItem)); // Дебаф сделать цифру рандомной 10
                break;
            case BonusEnum.SEE_NUMBER:
                StartCoroutine(ShowResultNum(bonusItem)); // баф показать одну цифру 11
                break;
            case BonusEnum.WHITE_ALL:
                StartCoroutine(ShowWhiteAll(bonusItem)); // Дебаф сделать все цифры белыми 12
                break;
            case BonusEnum.TIME_EXP:
                StartCoroutine(ShowAddTimeExp(bonusItem)); // баф показать одну цифру 13
                break;
            case BonusEnum.HIDE_ROW:
                StartCoroutine(ShowHideRowNum(bonusItem)); // спрятать 1 ряд
                break;
            case BonusEnum.ROW_EXP:
                StartCoroutine(ShowAddRowExp(bonusItem)); // баф показать одну цифру 15
                break;
            case BonusEnum.CATASTROPHE:
                StartCoroutine(ShowCatastrohpe(bonusItem)); // Дебаф котострофа замарозить ряды и ячейки
                break;
            case BonusEnum.LEVEL_UP:
                StartCoroutine(ShowAddLevelExp(bonusItem)); // Добавить + 10 к уровню 17
                break;
            case BonusEnum.BONUS18:
                // StartCoroutine(ShowWhiteAll(bonusItem)); // 
                break;
        }
    }

    IEnumerator ShowLifeBonus(BonusItem bonusItem)
    {
        GreenBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, GameManager.Instance.AddLifeAction); 
        
        // if (GameManager.Instance.LifeAddUse)
        // {
        //     GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, GameManager.Instance.AddLifeActionFalse);
        // }
        // else
        // {
        //     GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, GameManager.Instance.AddLifeAction); 
        // }
        //
        // GameManager.Instance.LifeAddUse = true;
        yield return null;
    }

    public void BlueBonusAchieve()
    {
        GameSettings.Instance.BluesBonusAchieve++;
        GameSettings.Instance.SaveBlueBonusAchieve();
        GameSettings.Instance.achiveManager.ShowAchieveStatus(GameSettings.Instance.BluesBonusAchieve, AchieveType.BLUE_BONUS);
    }
    
    public void GreenBonusAchieve()
    {
        GameSettings.Instance.BluesBonusAchieve++;
        GameSettings.Instance.SaveBlueBonusAchieve();
        GameSettings.Instance.achiveManager.ShowAchieveStatus(GameSettings.Instance.BluesBonusAchieve, AchieveType.GREEN_BONUS);
    }
    
    public void RedBonusAchieve()
    {
        GameSettings.Instance.BluesBonusAchieve++;
        GameSettings.Instance.SaveBlueBonusAchieve();
        GameSettings.Instance.achiveManager.ShowAchieveStatus(GameSettings.Instance.BluesBonusAchieve, AchieveType.RED_BONUS);
    }

    IEnumerator ShowFreezeCell(BonusItem bonusItem)
    {
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, FreezeCellAction);
        RedBonusAchieve();
        
        yield return null;
    }

    IEnumerator ShowUnLockCell(BonusItem bonusItem)
    {
        BlueBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, UnLockCellBtnAction);
        yield return null;
    }

    IEnumerator ShowUnLockRaw(BonusItem bonusItem)
    {
        BlueBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, UnLockRawBtnAction);
        yield return null;
    }
    
    IEnumerator ShowLockRaw(BonusItem bonusItem)
    {
        RedBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, FreezeRawAction);
        yield return null;
    }
    
    IEnumerator ShowUnLockMagic(BonusItem bonusItem)
    {
        BlueBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, UnLockMagicBtnAction);
        yield return null;
    }
    
    IEnumerator ShowUnClearMagic(BonusItem bonusItem)
    {
        BlueBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, UnLockClearBtnAction);
        yield return null;
    }
    
    IEnumerator ShowLockDellBtn(BonusItem bonusItem)
    {
        RedBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, LockDellBtnAction);
        yield return null;
    }
    
    IEnumerator ShowLockEmptyBtn(BonusItem bonusItem)
    {
        RedBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, LockEmptyBtnAction);
        yield return null;
    }
    
    IEnumerator ShowRandomNum(BonusItem bonusItem)
    {
        RedBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, RandomNumAction);
        yield return null;
    }
    
    IEnumerator ShowWhiteAll(BonusItem bonusItem)
    {
        RedBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, WhiteAllAction);
        yield return null;
    }
    
    IEnumerator ShowResultNum(BonusItem bonusItem)
    {
        BlueBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, ResultNumAction);
        yield return null;
    }
    
    IEnumerator ShowAddTimeExp(BonusItem bonusItem)
    {
        GreenBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, TimeExpAction);
        yield return null;
    }
    
    IEnumerator ShowAddRowExp(BonusItem bonusItem)
    {
        GreenBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, RawExpAction);
        yield return null;
    }
    
    IEnumerator ShowAddLevelExp(BonusItem bonusItem)
    {
        GreenBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, RawLvlAction);
        yield return null;
    }
    
    IEnumerator ShowHideRowNum(BonusItem bonusItem)
    {
        RedBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, HideRowAction);
        yield return null;
    }
    
    IEnumerator ShowCatastrohpe(BonusItem bonusItem)
    {
        RedBonusAchieve();
        GameManager.Instance.timeBonusRoulette.ShowCurrentBonus(bonusItem, CatastropheAction);
        yield return null;
    }
    
    public BonusItem GetRandomReward(Dictionary<int, BonusItem> bonuses) // выбор рандом награды по весу  (reward)
    {
        List<int> rewardList = new List<int>(); //  лист текущих наград (индекс номер по порядку награды и вес в значении)

        foreach (KeyValuePair<int, BonusItem> bonusItem in bonuses)
        {
            rewardList.Add(bonusItem.Value.Chance);
        }

        int total = 0;
        int currentWeight = 0;
        int randomWieght = 0;

        foreach (int rewardWeight in rewardList) // собрать весь вес
        {
            total += rewardWeight;
        }

        randomWieght = Random.Range(1, total + 1);

        for (int i = 0; i < rewardList.Count; i++)
        {
            currentWeight += rewardList[i];

            if (currentWeight >= randomWieght)
            {
                // rezult = bonuses[i];
                return bonuses[i];
            }
        }

        Debug.LogWarning("bonus miss");
        return bonuses[0];
    }

    public void NewRandomBonus()
    {
        bonusItems.Clear();
        bonusItems = bonusItemInit.Init();
    }

    private void Start()
    {
        LoadInitParam();

        FreezeCellAction += FreezeCells; // замарозить ячейку 
        UnLockCellBtnAction += UnBlockCellBtn; // размарозить ячейку
        
        FreezeRawAction += FreezeOneRaw; // замарозить ряд
        UnLockRawBtnAction += UnBlockRawBtn; // размарозить ряд
        
        UnLockMagicBtnAction += UnBlockMagicBtn; // разблокировать мэджик
        UnLockClearBtnAction += UnBlockClearBtn; // разблокировать клеар

        LockDellBtnAction += ButtonCancelDebuf; // заблокировать кнопку дел
        LockEmptyBtnAction += ButtonEmptyDebuf; // заблокировать кнопку пустышка
        
        RandomNumAction += DebufChangeNun; // поменять цифру
        WhiteAllAction += DebufColorWhite; // сделать все белым
        
        ResultNumAction += ShowBonusResultOneNumBonus; // показать одну случаную цифру
        
        TimeExpAction += ShowAddTimeBonus; //добавить +1 к тайм бонус
        RawExpAction += ShowAddRowBonus; // добавить +1 к рав бонус
        
        RawLvlAction += ShowAddLevelUp; // добавить +10 к левелу
        
        HideRowAction += ClearRawDebaf; // спрятать ряд
        
        CatastropheAction += DebufCatastrohpe; // котострофа
        
        BonusChose += StartBonus;
        bonusItems.Clear();
        bonusItems = bonusItemInit.Init(); // инитнуть бафы на круг

        CheckUnlockCellBtnToStartGame(CellBtnAmount);
        CheckUnlockRawBtnToStartGame(RawBtnAmount);
        CheckUnlockMagicBtnToStartGame(MagicBtnAmount);
        CheckUnlockClearBtnToStartGame(ClearBtnAmount);

        InitLuckyBonusMiddle();
        InitLuckyBonusSmall();
        InitLuckyBonusLarge();
        
        InitBonusRound(GameManager.Instance.startGameControlRef.sceneIndex); // берем у текущей сцены индекс
    }

    private void LoadInitParam()
    {
        // кнопки разблокировок
        CellBtnAmount = SaveManager.Instance.LoadRound("CellBtnAmount");
        RawBtnAmount = SaveManager.Instance.LoadRound("RawBtnAmount");
        MagicBtnAmount = SaveManager.Instance.LoadRound("MagicBtnAmount");
        ClearBtnAmount = SaveManager.Instance.LoadRound("ClearBtnAmount");
    }

    private void InitBonusRound(SceneIndex sceneIndex)
    {
        switch (sceneIndex)
        {
            /// 333333333333 ////
          
            case SceneIndex.LEVEL_ONE: // 3
                int randomNumMagic1 = Random.Range(0, LuckyBonusItemsListSmall.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListSmall[randomNumMagic1]);
                break;
            
            case SceneIndex.LEVEL_FOUR: // 3
                int randomNumMagic4 = Random.Range(0, LuckyBonusItemsListSmall.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListSmall[randomNumMagic4]);
                break;
            
            case SceneIndex.LEVEL_11: // 3
                int randomNumMagic11 = Random.Range(0, LuckyBonusItemsListSmall.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListSmall[randomNumMagic11]);
                break;
            
            case SceneIndex.LEVEL_14: // 3
                int randomNumMagic14 = Random.Range(0, LuckyBonusItemsListSmall.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListSmall[randomNumMagic14]);
                break;
            
         /// 333333333333 /////
            
            /// 444444444444444444444   /////
 
            case SceneIndex.LEVEL_TWO: //4
                int randomNumMagic2 = Random.Range(0, LuckyBonusItemsListMiddle.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListMiddle[randomNumMagic2]);
                break;
            
            case SceneIndex.LEVEL_FIVE: //4
                int randomNumMagic5 = Random.Range(0, LuckyBonusItemsListMiddle.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListMiddle[randomNumMagic5]);
                break;
            
            case SceneIndex.LEVEL_12: //4
                int randomNumMagic12 = Random.Range(0, LuckyBonusItemsListMiddle.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListMiddle[randomNumMagic12]);
                break;
            
            case SceneIndex.LEVEL_15: //4
                int randomNumMagic15 = Random.Range(0, LuckyBonusItemsListMiddle.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListMiddle[randomNumMagic15]);
                break;

            /// 444444444444444444444   ///////
            
            
            /// 5555555555555555  ///////
         
            case SceneIndex.LEVEL_TREE: //5
                int randomNumMagic3 = Random.Range(0, LuckyBonusItemsListMiddle.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListMiddle[randomNumMagic3]);
                break;
            
            case SceneIndex.LEVEL_SIX: //5
                int randomNumMagic6 = Random.Range(0, LuckyBonusItemsListMiddle.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListMiddle[randomNumMagic6]);
                break;
            
            case SceneIndex.LEVEL_13: //5
                int randomNumMagic13 = Random.Range(0, LuckyBonusItemsListMiddle.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListMiddle[randomNumMagic13]);
                break;
            
            case SceneIndex.LEVEL_16: //5
                int randomNumMagic16 = Random.Range(0, LuckyBonusItemsListMiddle.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListMiddle[randomNumMagic16]);
                break;
            
            /// /// 5555555555555555  ///////

            /// /// 666666666666666  ///////
            
            case SceneIndex.LEVEL_7: //6
                int randomNumMagic7 = Random.Range(0, LuckyBonusItemsListLarge.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListLarge[randomNumMagic7]);
                break;
            
            case SceneIndex.LEVEL_8: //6
                int randomNumMagic8 = Random.Range(0, LuckyBonusItemsListLarge.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListLarge[randomNumMagic8]);
                break;
            
            case SceneIndex.LEVEL_17: //6
                int randomNumMagic17 = Random.Range(0, LuckyBonusItemsListLarge.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListLarge[randomNumMagic17]);
                break;
            
            case SceneIndex.LEVEL_18: //6
                int randomNumMagic18 = Random.Range(0, LuckyBonusItemsListLarge.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListLarge[randomNumMagic18]);
                break;
            
            /// /// 666666666666666  ///////
            
            
            /// /// 77777777777777  ///////
        
            case SceneIndex.LEVEL_9: // 7
                int randomNumMagic9 = Random.Range(0, LuckyBonusItemsListLarge.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListLarge[randomNumMagic9]);
                break;
            
            case SceneIndex.LEVEL_10: // 7
                int randomNumMagic10 = Random.Range(0, LuckyBonusItemsListLarge.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListLarge[randomNumMagic10]);
                break;
            
            case SceneIndex.LEVEL_19: // 7
                int randomNumMagic19 = Random.Range(0, LuckyBonusItemsListLarge.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListLarge[randomNumMagic19]);
                break;
            
            case SceneIndex.LEVEL_20: // 7
                int randomNumMagic20 = Random.Range(0, LuckyBonusItemsListLarge.Count);
                isActiveMagicNumBonus = false;
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListLarge[randomNumMagic20]);
                break;
            
            /// /// 77777777777777  ///////
            
            case SceneIndex.TUTORIAL:
                GameManager.Instance.turnControl.DrawLuckyBonusMagic(LuckyBonusItemsListSmall[0]);
                break;
            case SceneIndex.INTRO_PARALLAX:
                break;
        }
    }

    private void InitLuckyBonusMiddle()
    {
        LuckyBonusItem luckyBonusItem1 = new LuckyBonusItem();
        luckyBonusItem1.Green = 2;
        luckyBonusItem1.Red = 2;

        LuckyBonusItem luckyBonusItem2 = new LuckyBonusItem();
        luckyBonusItem2.Green = 1;
        luckyBonusItem2.Red = 3;

        LuckyBonusItem luckyBonusItem3 = new LuckyBonusItem();
        luckyBonusItem3.Green = 3;
        luckyBonusItem3.Red = 1;

        LuckyBonusItem luckyBonusItem4 = new LuckyBonusItem();
        luckyBonusItem4.Green = 3;
        luckyBonusItem4.Red = 0;

        LuckyBonusItem luckyBonusItem5 = new LuckyBonusItem();
        luckyBonusItem5.Green = 0;
        luckyBonusItem5.Red = 2;

        LuckyBonusItem luckyBonusItem6 = new LuckyBonusItem();
        luckyBonusItem6.Green = 0;
        luckyBonusItem6.Red = 3;

        LuckyBonusItem luckyBonusItem7 = new LuckyBonusItem();
        luckyBonusItem7.Green = 0;
        luckyBonusItem7.Red = 4;

        LuckyBonusItemsListMiddle.Add(luckyBonusItem1);
        LuckyBonusItemsListMiddle.Add(luckyBonusItem2);
        LuckyBonusItemsListMiddle.Add(luckyBonusItem3);
        LuckyBonusItemsListMiddle.Add(luckyBonusItem4);
        LuckyBonusItemsListMiddle.Add(luckyBonusItem5);
        LuckyBonusItemsListMiddle.Add(luckyBonusItem6);
        LuckyBonusItemsListMiddle.Add(luckyBonusItem7);
    }
    
    private void InitLuckyBonusLarge()
    {
        LuckyBonusItem luckyBonusItem1 = new LuckyBonusItem();
        luckyBonusItem1.Green = 2;
        luckyBonusItem1.Red = 2;

        LuckyBonusItem luckyBonusItem2 = new LuckyBonusItem();
        luckyBonusItem2.Green = 1;
        luckyBonusItem2.Red = 3;

        LuckyBonusItem luckyBonusItem3 = new LuckyBonusItem();
        luckyBonusItem3.Green = 3;
        luckyBonusItem3.Red = 1;

        LuckyBonusItem luckyBonusItem4 = new LuckyBonusItem();
        luckyBonusItem4.Green = 3;
        luckyBonusItem4.Red = 2;

        LuckyBonusItem luckyBonusItem5 = new LuckyBonusItem();
        luckyBonusItem5.Green = 2;
        luckyBonusItem5.Red = 2;

        LuckyBonusItem luckyBonusItem6 = new LuckyBonusItem();
        luckyBonusItem6.Green = 0;
        luckyBonusItem6.Red = 4;

        LuckyBonusItem luckyBonusItem7 = new LuckyBonusItem();
        luckyBonusItem7.Green = 4;
        luckyBonusItem7.Red = 0;
        
        LuckyBonusItem luckyBonusItem8 = new LuckyBonusItem();
        luckyBonusItem8.Green = 1;
        luckyBonusItem8.Red = 4;
        
        LuckyBonusItem luckyBonusItem9 = new LuckyBonusItem();
        luckyBonusItem9.Green = 2;
        luckyBonusItem9.Red = 3;
        
        LuckyBonusItem luckyBonusItem10 = new LuckyBonusItem();
        luckyBonusItem10.Green = 4;
        luckyBonusItem10.Red = 1;

        LuckyBonusItemsListLarge.Add(luckyBonusItem1);
        LuckyBonusItemsListLarge.Add(luckyBonusItem2);
        LuckyBonusItemsListLarge.Add(luckyBonusItem3);
        LuckyBonusItemsListLarge.Add(luckyBonusItem4);
        LuckyBonusItemsListLarge.Add(luckyBonusItem5);
        LuckyBonusItemsListLarge.Add(luckyBonusItem6);
        LuckyBonusItemsListLarge.Add(luckyBonusItem7);
        LuckyBonusItemsListLarge.Add(luckyBonusItem8);
        LuckyBonusItemsListLarge.Add(luckyBonusItem9);
        LuckyBonusItemsListLarge.Add(luckyBonusItem10);
    }
    
    private void InitLuckyBonusSmall()
    {
        LuckyBonusItem luckyBonusItem1 = new LuckyBonusItem();
        luckyBonusItem1.Green = 2;
        luckyBonusItem1.Red = 0;

        LuckyBonusItem luckyBonusItem2 = new LuckyBonusItem();
        luckyBonusItem2.Green = 0;
        luckyBonusItem2.Red = 2;

        LuckyBonusItem luckyBonusItem3 = new LuckyBonusItem();
        luckyBonusItem3.Green = 1;
        luckyBonusItem3.Red = 1;

        LuckyBonusItemsListSmall.Add(luckyBonusItem1);
        LuckyBonusItemsListSmall.Add(luckyBonusItem2);
        LuckyBonusItemsListSmall.Add(luckyBonusItem3);
    }

    public void DebufCatastrohpe(int amount)
    {
        AudioManagerGame.Instance.PlayDebafSound();
        int randomCell = Random.Range(3, 7);
        for (int i = 0; i < randomCell; i++)
        {
            FreezeCellAction.Invoke(4);
        }
    }

    public void RoundBonusComplete()
    {
        AudioManagerGame.Instance.PlayBafSound();
        if (isActiveMagicNumBonus)
        {
            return;
        }
 
        ExpManager.Instance.CurrentExperience += ExpManager.Instance.AddExpToScoreBonusLevel();
        ExpManager.Instance.CurrentExperienceRoundBonus += ExpManager.Instance.AddExpToScoreBonusLevel();
        ExpManager.Instance.ShowScore();
        
        ExpManager.Instance.AddLevelExpAction.Invoke(ExpManager.Instance.WandCountToLevelRoundBonus);

        // luckyMagicTransform
        GameManager.Instance.timeBonusRoulette.ActiveButtonRoulette();
        isActiveMagicNumBonus = true;
        turnControl.luckyMagicTransform.GetComponent<LuckyMagic>().CompleteRoundBonus();
        // GameManager.ClearParent(turnControl.luckyMagicTransform);
        GameManager.Instance.AddBonusAction.Invoke(0);

        GameSettings.Instance.RoundBonusAchieve++;
        GameSettings.Instance.SaveRoundBonusAchieve();
        GameSettings.Instance.achiveManager.ChekRoundBonusAchieve(GameSettings.Instance.RoundBonusAchieve);
    }
    // unFreezeCellButtons


    public void ActiveTimeBonusBtn()
    {
        AudioManagerGame.Instance.PlayTBonusCreate();
        if (GameManager.Instance.EndRoundTime)
        {
            return;
        }

        GameManager.Instance.timeBonusRoulette.ActiveButtonRoulette();
        GameManager.Instance.AddBonusAction.Invoke(0);
    }

    public void DebufChangeNun(int delay)
    {
        AudioManagerGame.Instance.PlayDebafSound();
        StartCoroutine(DebufDelay(delay));
    }

    IEnumerator DebufDelay(float delay) // меняет рандомную цифру
    {
        yield return new WaitForSecondsRealtime(delay);
        turnControl.ChangeMagicNum();
    }

    public void DebufColorWhite(int amount)
    {
        turnControl.ColorWhiteAllNum();
    }

    public void LostLife()
    {
        GameManager.Instance.LostLifeAction.Invoke(GameManager.Instance.CurrentLife);
    }

    public void AddLife()
    {
        GameManager.Instance.AddLifeAction.Invoke(0);
    }
    
    public void ClearRawDebaf(int amount) // спрятать ряд
    {
        AudioManagerGame.Instance.PlayDebafSound();
        
        if (turnControl.rawNumber == 0)
        {
            return;
        }

        int count = 0;

        for (int j = 0; j < turnControl.raws.Length; j++) //  ряды
        {
            RawControl rawControl = turnControl.raws[j].transform.GetComponent<RawControl>();

            if (rawControl.turnComplete && !rawControl.isFrozenRaw)
            {
                count++;
            }
        }

        int numRandom = Random.Range(0, count);

        for (int i = 0; i < turnControl.numLimit; i++)
        {
            Image cellImage = turnControl.raws[numRandom].transform.GetChild(i).GetComponent<Image>();
            cellImage.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = String.Empty;

            GameObject effectBum =
                LeanPool.Spawn(GameManager.Instance.effector.delEffect, cellImage.transform.position, Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
        }
    }

    // -----------CLEAR ---------------------- //

    public void CheckUnlockClearBtnToStartGame(int amountCellBtn) // проверить на начало игры и разблокировать нужное количество кнопок
    {
        if (amountCellBtn == 0)
        {
            unFreezeClearButton.interactable = false;
        }
        else
        {
            unFreezeClearButton.interactable = true;
        }
    }

    public void UnBlockClearBtn(int amount)
    {
        AudioManagerGame.Instance.PlayBafSound();
        unFreezeClearButton.interactable = true;

        GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.unblockBtn, unFreezeClearButton.transform.position, Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
        ClearBtnAmount++;
    }

    public void BlockClearBtn(int amount)
    {
        unFreezeClearButton.interactable = false;

        GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.blockBonusBtn, unFreezeClearButton.transform.position, Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
        ClearBtnAmount--;
    }
    
    public void UnBlockAll()
    {
        AudioManagerGame.Instance.PlayBafSound();
        BlockClearBtn(0);
        ButtonCancelDebuf(1);
        ButtonEmptyDebuf(1);

        List<CellsShiny> cellsLockList = new List<CellsShiny>();

        for (int j = 0; j < turnControl.raws.Length; j++) //  ряды
        {
            RawControl rawControl = turnControl.raws[j].transform.GetComponent<RawControl>();
            Transform raw = turnControl.raws[j].transform;

            if (rawControl.isFrozenRaw)
            {
                UnFreezeOneRaw(false);
                continue;
            }

            for (int i = 0; i < turnControl.numLimit; i++) // ячейки
            {
                CellsShiny cell = raw.GetChild(i).GetComponent<CellsShiny>();
                if (cell.isLock)
                {
                    cellsLockList.Add(cell);
                }
            }
        }

        foreach (CellsShiny cellsShiny in cellsLockList)
        {
            cellsShiny.UnLock();
            GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.cellsLock, cellsShiny.transform.position, Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
        }

        turnControl.shinyMainWindowControl.ShowMain();
    }


    // ----------- CLEAR ---------------------- //

    // ----------- MAGIC ---------------------- //

    public void CheckUnlockMagicBtnToStartGame(int amountCellBtn) // проверить на начало игры и разблокировать нужное количество кнопок
    {
        for (int i = 0; i < amountCellBtn; i++)
        {
            magickButtons[i].interactable = true;
        }
    }

    public void UnBlockMagicBtn(int amount) // разблокировать кнопку меджик + action
    {
        AudioManagerGame.Instance.PlayBafSound();
        for (int i = 0; i < magickButtons.Length; i++)
        {
            if (!magickButtons[i].interactable)
            {
                magickButtons[i].interactable = true;
                GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.unblockBtn, magickButtons[i].transform.position,
                    Quaternion.identity);
                StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
                MagicBtnAmount++;
                break;
            }
        }
    }

    public void BlockMagicBtn(int amount) // заблокировать одну кнопку лок целс после нажатия.
    {
        for (int i = magickButtons.Length - 1; i >= 0; i--)
        {
            if (magickButtons[i].interactable)
            {
                magickButtons[i].interactable = false;
                GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.blockBonusBtn, magickButtons[i].transform.position,
                    Quaternion.identity);
                StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
                MagicBtnAmount--;
                break;
            }
        }
    }

    public void ControlBonusMagic(int magicNum) // открытие цифры при меджик боунсе
    {
        AudioManagerGame.Instance.PlayBafSound();
        BlockMagicBtn(0);
        StartCoroutine(Delay(magicNum));
    }

    private IEnumerator Delay(int num)
    {
        yield return new WaitForSecondsRealtime(1f);
        GameManager.Instance.turnControl.rawWinner[num].text = GameManager.Instance.turnControl.magicNumber[num].ToString();
        GameObject winEffect = LeanPool.Spawn(GameManager.Instance.effector.numEffectView,
            GameManager.Instance.turnControl.rawWinner[num].transform.position, Quaternion.identity);

        StartCoroutine(GameManager.Instance.effector.Despawner(winEffect, 2));
    }
    // ----------- MAGIC ---------------------- //

    // ----------- CELLS ---------------------- //

    public void CheckUnlockCellBtnToStartGame(int amountCellBtn) // проверить на начало игры и разблокировать нужное количество кнопок
    {
        for (int i = 0; i < amountCellBtn; i++)
        {
            unFreezeCellButtons[i].interactable = true;
        }
    }

    public void UnBlockCellBtn(int amount) // разблокировать кнопку лок целс + action
    {
        AudioManagerGame.Instance.PlayBafSound();
        for (int i = 0; i < unFreezeCellButtons.Length; i++)
        {
            if (!unFreezeCellButtons[i].interactable)
            {
                unFreezeCellButtons[i].interactable = true;
                GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.unblockBtn, unFreezeCellButtons[i].transform.position,
                    Quaternion.identity);
                StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
                CellBtnAmount++;
                break;
            }
        }
    }

    public void BlockFreezeCellBtn(int amount) // заблокировать одну кнопку лок целс после нажатия.
    {
        for (int i = unFreezeCellButtons.Length - 1; i >= 0; i--)
        {
            if (unFreezeCellButtons[i].interactable)
            {
                unFreezeCellButtons[i].interactable = false;
                GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.blockBonusBtn, unFreezeCellButtons[i].transform.position,
                    Quaternion.identity);
                StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
                CellBtnAmount--;
                break;
            }
        }
    }

    public void UnLockCellBaf() // разблокировать ячейки от 1 до 3 баф кнопки
    {
        AudioManagerGame.Instance.PlayBafSound();
        BlockFreezeCellBtn(0);
        int count = Random.Range(1, 4);

        for (int k = 0; k < count; k++)
        {
            List<CellsShiny> cellsLockList = new List<CellsShiny>();

            for (int j = 0; j < turnControl.raws.Length; j++) //  ряды
            {
                RawControl rawControl = turnControl.raws[j].transform.GetComponent<RawControl>();
                Transform raw = turnControl.raws[j].transform;

                if (rawControl.isFrozenRaw)
                {
                    continue;
                }

                for (int i = 0; i < turnControl.numLimit; i++)
                {
                    CellsShiny cell = raw.GetChild(i).GetComponent<CellsShiny>();
                    if (cell.isLock)
                    {
                        cellsLockList.Add(cell);
                    }
                }
            }

            if (cellsLockList.Count == 0)
            {
                return;
            }

            int randomLock = Random.Range(0, cellsLockList.Count);

            CellsShiny cellLock = cellsLockList[randomLock];
            cellLock.UnLock();

            GameObject effectBum =
                LeanPool.Spawn(GameManager.Instance.effector.cellsLock, cellLock.transform.position, Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
        }
    }
    
    public void FreezeMaxCell(int maxcell) // замарозить ячейки по запросу
    {
        if (turnControl.rawNumber >= turnControl.rawLimit)
        {
            return;
        }

        int cellFreezeCount = maxcell;

        for (int i = 0; i < cellFreezeCount; i++)
        {
            int count = 0;
            int countComplete = 0;

            for (int j = 0; j < turnControl.raws.Length; j++) //  ряды
            {
                RawControl rawControl = turnControl.raws[j].transform.GetComponent<RawControl>();

                if (!rawControl.isFrozenRaw)
                {
                    count++;
                }

                if (rawControl.turnComplete)
                {
                    countComplete++;
                }
            }

            int rawRandom = Random.Range(countComplete, count - 1);
            int cellRandom = Random.Range(1, turnControl.numLimit - 1);

            Image cellImage = turnControl.raws[rawRandom].transform.GetChild(cellRandom).GetComponent<Image>();
            cellImage.GetComponent<CellsShiny>().Lock();

            GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.cellsLock, cellImage.transform.position, Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
        }
    }

    public void FreezeCells(int cell) // замарозить ячейки от 1 до 3 дебаф
    {
        AudioManagerGame.Instance.PlayDebafSound();
        if (turnControl.rawNumber >= turnControl.rawLimit)
        {
            return;
        }
        
        int cellFreezeCount = Random.Range(1, 4);

        for (int i = 0; i < cellFreezeCount; i++)
        {
            int count = 0;
            int countComplete = 0;

            for (int j = 0; j < turnControl.raws.Length; j++) //  ряды
            {
                RawControl rawControl = turnControl.raws[j].transform.GetComponent<RawControl>();

                if (!rawControl.isFrozenRaw)
                {
                    count++;
                }

                if (rawControl.turnComplete)
                {
                    countComplete++;
                }
            }

            int rawRandom = Random.Range(countComplete, count - 1);
            int cellRandom = Random.Range(1, turnControl.numLimit - 1);

            Image cellImage = turnControl.raws[rawRandom].transform.GetChild(cellRandom).GetComponent<Image>();
            cellImage.GetComponent<CellsShiny>().Lock();

            GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.cellsLock, cellImage.transform.position, Quaternion.identity);
            StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
        }
    }

    // ----------- CELLS ---------------------- //


    // ----------- ROWS ---------------------- //


    public void CheckUnlockRawBtnToStartGame(int amountCellBtn) // проверить на начало игры и разблокировать нужное количество кнопок
    {
        for (int i = 0; i < amountCellBtn; i++)
        {
            unFreezeRawButtons[i].interactable = true;
        }
    }

    public void UnBlockRawBtn(int amount) // разблокировать кнопку лок целс + action
    {
        AudioManagerGame.Instance.PlayBafSound();
        for (int i = 0; i < unFreezeRawButtons.Length; i++)
        {
            if (!unFreezeRawButtons[i].interactable)
            {
                unFreezeRawButtons[i].interactable = true;
                GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.unblockBtn, unFreezeRawButtons[i].transform.position,
                    Quaternion.identity);
                StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
                RawBtnAmount++;
                break;
            }
        }
    }

    public void BlockRawBtn(int amount) // заблокировать одну кнопку лок целс после нажатия.
    {
        for (int i = unFreezeRawButtons.Length - 1; i >= 0; i--)
        {
            if (unFreezeRawButtons[i].interactable)
            {
                unFreezeRawButtons[i].interactable = false;
                GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.blockBonusBtn, unFreezeRawButtons[i].transform.position,
                    Quaternion.identity);
                StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
                RawBtnAmount--;
                break;
            }
        }
    }
 
    public void FreezeOneRaw(int amount) // замарозить один свободный ряд сверху
    {
        AudioManagerGame.Instance.PlayDebafSound();
        
        if (turnControl.rawNumber >= turnControl.rawLimit)
        {
            return;
        }
        
        for (int i = turnControl.raws.Length - 1; i >= 0; i--)
        {
            if (turnControl.raws[i].GetComponent<RawControl>().isFrozenRaw)
            {
                continue;
            }
            else
            {
                turnControl.raws[i].GetComponent<RawControl>().isFrozenRaw = true;

                turnControl.raws[i].GetComponent<CanvasGroup>().alpha = 0.2f;
                foreach (Transform trans in turnControl.raws[i].GetComponentInChildren<Transform>())
                {
                    trans.GetComponent<CellsShiny>().Lock();
                    GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.cellsLock, trans.position,
                        Quaternion.identity);
                    StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
                }

                turnControl.rawLimit--;

                break;
            }
        }
    }

    public void UnFreezeOneRaw(bool isDoneBtn = true)
    {
        AudioManagerGame.Instance.PlayBafSound();
        if (isDoneBtn)
        {
            BlockRawBtn(0); 
        }
       
        for (int i = 0; i < turnControl.raws.Length; i++)
        {
            if (turnControl.raws[i].GetComponent<RawControl>().isFrozenRaw)
            {
                turnControl.raws[i].GetComponent<RawControl>().isFrozenRaw = false;

                foreach (Transform trans in turnControl.raws[i].GetComponentInChildren<Transform>())
                {
                    trans.GetComponent<CellsShiny>().UnLock();
       
                    GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.cellsLock, trans.position,
                        Quaternion.identity);
                    StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
                }

                turnControl.raws[i].GetComponent<CanvasGroup>().alpha = 1f;
                turnControl.rawLimit++;

                break;
            }
        }
    }

    // ----------- ROWS ---------------------- //

    
    // ----------- Buttons ---------------------- //
    public void ButtonCancelDebuf(int set)
    {
        bool isActive;
        
        if (set == 1) // true
        {
            isActive = true;
        }
        else // false
        {
            AudioManagerGame.Instance.PlayDebafSound();
            isActive = false; 
        }
        
        buttonCancel.GetComponent<Button>().interactable = isActive;
        buttonCancel.borderImage.gameObject.SetActive(isActive);
        buttonCancel.buttonImage.gameObject.SetActive(isActive);
        buttonCancel.lockImage.gameObject.SetActive(!isActive);
        
        GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.blockEmpty, GameManager.Instance.dellBtnIcon.position,
            Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
    }

    public void ButtonEmptyDebuf(int set)
    {
        bool isActive;
        
        if (set == 1) // true
        {
            isActive = true;
        }
        else // false
        {
            AudioManagerGame.Instance.PlayDebafSound();
            isActive = false; 
        }
        
        buttonEmpty.GetComponent<Button>().interactable = isActive;
        buttonEmpty.borderImage.gameObject.SetActive(isActive);
        buttonEmpty.buttonImage.gameObject.SetActive(isActive);
        buttonEmpty.lockImage.gameObject.SetActive(!isActive);
        
        GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.blockEmpty, GameManager.Instance.emptyBtnIcon.position,
            Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
    }
    
    // ----------- Buttons ---------------------- //
    
    public void ShowBonusResultOneNum(int amount)
    {
        _bonusResultOneNum.Show();
        // StartHide(delayHide);
        StartCoroutine(_bonusResultOneNum.Hide(1));
    }
    
    public void ShowBonusResultOneNumBonus(int amount)
    {
        GameManager.Instance.SeeNumberUse = true;
    }
     
    public void ShowAddTimeBonus(int amount)
    {
        AudioManagerGame.Instance.PlayAchive();
        print("ShowAddTimeBonus");
        ExpManager.Instance.CurrentExpWandTime ++;
        GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.bonusUp, ExpManager.Instance.timeBonusIcon.position,
            Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
    }
    
    public void ShowAddRowBonus(int amount)
    {
        AudioManagerGame.Instance.PlayAchive();
        print("ShowAddRowBonus");
        ExpManager.Instance.CurrentExpWandRaw++;
        GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.bonusUp, ExpManager.Instance.rowBonusIcon.position,
            Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
    }
    
    public void ShowAddLevelUp(int amount)
    {
        AudioManagerGame.Instance.PlayAchive();
        print("ShowAddLevelUp");
        ExpManager.Instance.levelExp.AddExp(10);
        
        // ExpManager.Instance.CurrentExpWandLvl++;
        GameObject effect = LeanPool.Spawn(GameManager.Instance.effector.bonusUp, ExpManager.Instance.levelBonusIcon.position,
            Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effect, 2));
    }
}