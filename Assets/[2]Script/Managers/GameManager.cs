﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : GenericSingletonClass<GameManager>
{
    [Header("cheat")] [SerializeField] private CheatPanel cheatPanel;

    [Header("win panels")] [SerializeField] [Tooltip("панель победы")]
    public WinnerPanel winnerPanel;

    public WinnerPanelClassic winnerPanelClassic;

    [SerializeField] [Tooltip("панель гейм овера")]
    private GameOverPanel gameOverPanel;

    [SerializeField] [Tooltip("панель стейдж фейл")]
    private StageFailPanel stageFailPanel;

    [SerializeField] [Tooltip("PrepareStart")]
    public PrepareStart prepareStart;

    [SerializeField] private VictoryPanel victoryPanel;

    [SerializeField] [Tooltip("Go")] public GoPanel goPanel;
    [SerializeField] private GameObject blockRayCast;

    [Header("bonus panels")] [SerializeField]
    public BonusMagicNum bonusMagicNum;

    [SerializeField] public BonusTimeRoulette timeBonusRoulette;

    [Header("Buttons transform")] [SerializeField]
    public Transform dellBtnIcon;

    [SerializeField] public Transform emptyBtnIcon;

    [Header("reference")] public Effector effector;
    [SerializeField] public StartGameControl startGameControlRef;
    [SerializeField] public Avatar avatarRef;
    [SerializeField] public VictoryPanel victoryPanelRef;
    [SerializeField] public ControlRankTable controlRankTable;
    [SerializeField] public DataBase dataBaseRef;
    [SerializeField] public GameNetManager gameNetManager;

    [Header("control")] [SerializeField] public TurnControl turnControl;
    [SerializeField] public InitData initData;

    [SerializeField] public LifeBar lifeBar;
    [SerializeField] public BonusBar bonusBar;

    [SerializeField] public HelperBtn helperBtn;

    // [Tooltip("старт с дефолтными настройками")]
    // public bool isDefaultGame = false;

    public int CurrentLife { get; set; } // контроль жизней на игру.
    public int CurrentLifeClassic { get; set; } // контроль жизней на игру Classic.
    public int CurrentBonus { get; set; } // накопленные бонусы  на начало игры
    public int CurrenLevelRank { get; set; } // какой ранг получен на уровне
    public int CurrenLevelRankClassic { get; set; } // какой ранг получен на уровне classic

    public string NameTable { get; set; } // имя для записи в таблицу

    public List<int> CurrentRank { get; set; } = new List<int>(); // хранится ранг для левела
    public List<int> CurrentRankClassic { get; set; } = new List<int>(); // хранится ранг для левела classic

    // private DifficultyEnum _difficultyEnum;
    public int MaxLife { get; private set; } = 7;

    public Action<int> LostLifeAction { get; set; } //убрать жизнь
    public Action<int> LostLifeActionClassic { get; set; } //убрать жизнь Classic
    public Action<int> AddLifeAction { get; set; } // добавить жизнь
    public Action<int> AddLifeActionFalse { get; set; } // не добавить жизнь
    public Action<int> LostBonusAction { get; set; }
    public Action<int> AddBonusAction { get; set; } // добавить +1 к бонусу
    public bool EndRoundTime { get; set; } // конец раунда
    public const string LifeNums = "lifeCount";
    public const string LifeNumsClassic = "lifeCountClassic";
    public const string BonusNums = "varCount";
    public bool SeeNumberUse { get; set; }
    public bool LifeAddUse { get; set; }


    // private void Update()
    // {
    //     if (Input.GetKeyDown(KeyCode.F1))
    //     {
    //         cheatPanel.gameObject.SetActive(true);
    //     }
    // }

    public static void ClearParent(Transform clearTransform)
    {
        int count = clearTransform.childCount;
        for (int i = 0; i < count; i++)
        {
            GameObject obj = clearTransform.GetChild(i).gameObject;
            Destroy(obj);
        }
    }

    public void ShowTable()
    {
        controlRankTable.tableMain.Show();
    }

    private void InitLife()
    {
        Instance.EndRoundTime = false;
        var lifeCount = PlayerPrefs.GetInt(LifeNums);
        lifeBar.InitRound(lifeCount);
        CurrentLife = lifeCount;
    }

    private void InitLifeClassic()
    {
        Instance.EndRoundTime = false;
        var lifeCount = PlayerPrefs.GetInt(LifeNumsClassic);
        lifeBar.InitRound(lifeCount);
        CurrentLifeClassic = lifeCount;
    }

    private void InitBonus(int bonus)
    {
        Instance.EndRoundTime = false;

        bonusBar.InitRound(bonus);
        CurrentBonus = bonus;

        if (CurrentBonus <= 0)
        {
            timeBonusRoulette.shinyButtonRoulette.ButtonAcceptDisable();
        }
        else
        {
            timeBonusRoulette.shinyButtonRoulette.ButtonAcceptEnable();
        }
    }

    public void InitStart(bool isTutor = false)
    {
        int barCount = PlayerPrefs.GetInt(BonusNums);

        if (startGameControlRef.isClassic)
        {
            InitLifeClassic();
        }
        else
        {
            InitLife();
        }

        if (isTutor)
        {
            InitBonus(0);
        }
        else
        {
            InitBonus(barCount);
        }

        // if (isDefaultGame)
        // {
        //     // SetDefaultNewGame();
        // }
    }

    public void InitLifeFirstStartGame()
    {
        CurrentBonus = 0;
        // _difficultyEnum = DifficultyEnum.NORMAL;
        switch (GameSettings.Instance.CurrentDifficulty)
        {
            case DifficultyEnum.EASY:
                CurrentLife = 7;
                break;
            case DifficultyEnum.NORMAL:
                CurrentLife = 5;
                break;
            case DifficultyEnum.HARD:
                CurrentLife = 3;
                break;
        }

        PlayerPrefs.SetInt(LifeNums, CurrentLife);
        PlayerPrefs.SetInt(BonusNums, CurrentBonus);
    }

    public IEnumerator StageFail()
    {
        BlockRayCast();
        
        if (startGameControlRef.isPVP)
        {
            TimeManager.Instance.StopMainTimer = true;
            yield break;
        }

        if (startGameControlRef.isClassic)
        {
            LostLifeActionClassic.Invoke(0); // потеря жизни
            SaveManager.Instance.SaveLifeClassic(CurrentLifeClassic);

            if (turnControl.chekRawEndWinner)
            {
                yield break;
            }

            if (CurrentLifeClassic <= 0)
            {
                yield return new WaitForSeconds(2.1f);
                gameOverPanel.Show();
            }
            else
            {
                yield return new WaitForSeconds(2.1f);
                stageFailPanel.Show();
            }
        }

        else
        {
            avatarRef.IsDone = true;
            LostLifeAction.Invoke(0); // потеря жизни
            SaveManager.Instance.SaveLife(CurrentLife);
            BtnClick.Instance.OfBtn = false; // блокируем кнопки от нажатия

            if (turnControl.chekRawEndWinner)
            {
                yield break;
            }

            if (CurrentLife <= 0)
            {
                yield return new WaitForSeconds(2.1f);
                gameOverPanel.Show();
            }
            else
            {
                yield return new WaitForSeconds(2.1f);
                stageFailPanel.Show();
            }
        }

        print("stageFailPanel");
        yield return new WaitForSecondsRealtime(3.2f);
        
        UnBlockRayCast();
        print("UnBlockRayCast");

    }

    private void WinPanelActive()
    {
        avatarRef.IsDone = true;

        if (startGameControlRef.isPVP)
        {
            return;
        }

        if (startGameControlRef.isClassic)
        {
            if (startGameControlRef.sceneIndex == SceneIndex.CLASSIC_LVL10)
            {
                victoryPanel.Show();
            }
            else
            {
                winnerPanelClassic.ShowWinnClassic();
            }

            return;
        }

        if (startGameControlRef.sceneIndex == SceneIndex.LEVEL_20)
        {
            victoryPanel.Show();
        }
        else
        {
            winnerPanel.ShowStatsPanel();
        }
    }

    public IEnumerator GameRoundWin() // победа
    {
        BlockRayCast();
        BtnClick.Instance.OfBtn = false;
        turnControl.blockButton = true;
        turnControl.chekRawEndWinner = true;
        yield return new WaitForSeconds(2.1f);
        WinPanelActive();
    }

    public void BlockRayCast()
    {
        blockRayCast.SetActive(true);
    }

    public void UnBlockRayCast()
    {
        blockRayCast.SetActive(false);
    }

    public void StartDiffucultyDebuff(DebuffLevel debuffLevel)
    {
        if (startGameControlRef.isClassic)
        {
            StageEasyDebafLevel(debuffLevel);
            return;
        }

        switch (GameSettings.Instance.CurrentDifficulty)
        {
            case DifficultyEnum.EASY:
                StageEasyDebafLevel(debuffLevel);
                break;
            case DifficultyEnum.NORMAL:
                StageNormalDebafLevel(debuffLevel);
                break;
            case DifficultyEnum.HARD:
                StageHardDebafLevel(debuffLevel);
                break;
        }
    }

    public void StageEasyDebafLevel(DebuffLevel debuffLevel)
    {
        switch (debuffLevel)
        {
            case DebuffLevel.ONE:
                //easy лвл 1

                break;
            case DebuffLevel.TWO:
                //easy лвл 2 - 1 ряд
                BonusManager.Instance.FreezeOneRaw(0);
                break;
            case DebuffLevel.TREE:
                //easy лвл 3 - 1 ячейка
                BonusManager.Instance.FreezeMaxCell(1);
                break;
            case DebuffLevel.FOUR:
                //easy лвл 4 - 2 ячейка
                BonusManager.Instance.FreezeMaxCell(2);
                break;
            case DebuffLevel.FIVE:
                //easy лвл 5 - 1 ячейка
                BonusManager.Instance.FreezeMaxCell(1);
                break;
            case DebuffLevel.SIX:
                //easy лвл 6 - 1 ячейка 1 ряд
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(1);
                break;
            case DebuffLevel.SEVEN:
                //easy лвл 7 - 1 ячейка 1 ряд
                BonusManager.Instance.FreezeMaxCell(3);
                break;
            case DebuffLevel.EIGHT:
                //easy лвл 8 - 1 ячейка 1 ряд

                BonusManager.Instance.FreezeMaxCell(5);
                break;
        }
    }

    public void StageNormalDebafLevel(DebuffLevel debuffLevel)
    {
        switch (debuffLevel)
        {
            case DebuffLevel.ONE:
                BonusManager.Instance.FreezeOneRaw(0);
                //normal лвл 1
                break;
            case DebuffLevel.TWO:
                //normal лвл 2 - 1 ряд 1 cell
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(1);
                break;
            case DebuffLevel.TREE:
                //normal лвл 3 - 3 ячейка 1 ряд
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(3);
                break;
            case DebuffLevel.FOUR:
                //normal лвл 4 - 4 ячейка 1 ряд
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(4);
                break;

            case DebuffLevel.FIVE:
                //normal лвл 5 - 1 ячейка
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(1);
                break;
            case DebuffLevel.SIX:
                //normal лвл 6 - 1 ячейка 2 ряд
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(1);
                break;
            case DebuffLevel.SEVEN:
                //normal лвл 7 - 7 ячейка 1 ряд
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(5);
                break;
            case DebuffLevel.EIGHT:
                //normal лвл 8 - 7 ячейка 1 ряд
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(7);
                break;
        }
    }

    public void StageHardDebafLevel(DebuffLevel debuffLevel)
    {
        switch (debuffLevel)
        {
            case DebuffLevel.ONE:
                //хард лвл 1 - 2 ряда
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                break;
            case DebuffLevel.TWO:
                //хард лвл 2 - 3 ряда
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                break;
            case DebuffLevel.TREE:
                //хард лвл 3 - 2 ряда 3 ячейки
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(3);
                break;
            case DebuffLevel.FOUR:
                //хард лвл 4 - 2 ряда 4 ячейки
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(4);
                break;
            case DebuffLevel.FIVE:
                //хард лвл 5 - 2 ряда 1 ячейки
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(1);
                break;
            case DebuffLevel.SIX:
                //хард лвл 6 - 3 ряда 2 ячейки
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(2);
                break;
            case DebuffLevel.SEVEN:
                //хард лвл 7 - 2 ряда 5 ячейки
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(5);
                break;
            case DebuffLevel.EIGHT:
                //хард лвл 8 - 3 ряда 5 ячеек
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeOneRaw(0);
                BonusManager.Instance.FreezeMaxCell(7);
                break;
        }
    }
}