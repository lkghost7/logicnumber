﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class TimeManager : GenericSingletonClassDA<TimeManager>
{
    [SerializeField] public TextMeshProUGUI timeMain;
    [SerializeField] private TextMeshProUGUI leftTimeRaw;
    [SerializeField] private TextMeshProUGUI leftTimeRound;

    [SerializeField] private int timeLeftRoundEasy;
    [SerializeField] private int timeLeftRawEasy;
    
    [SerializeField] private int timeLeftRoundNormal;
    [SerializeField] private int timeLeftRawNormal;
    
    [SerializeField] private int timeLeftRoundHard;
    [SerializeField] private int timeLeftRawHard;
 
    public float AmountRoundTimeLeft { get; set; } // таймер для роунда
    public float AmountRawTimeLeft { get; set; } // таймер для ряда в секундах
    public bool IsRawTimeStart { get; set; }  // автивен ли таймер ряда
    public bool IsRoundTimeStart { get; set; }   // активен ли таймер раунда
    public bool StopTimer { get; set; } 
    public bool StopMainTimer { get; set; } 
     
    private float RawTimeLeft;
    private float RoundTimeLeft;
    private float timerRaw = 0;
    private float timerRound = 0;
    private TimeSpan _timeSpan;
    int second = 0;
    int minute = 0;

    public float countTimeLeft { get; set; }
    public int countRoundLeft { get; set; }

    void UpdateTotalRealTime()
    {
        int hours = Mathf.FloorToInt(Time.unscaledTime / 3600);
        int mins = Mathf.FloorToInt((Time.unscaledTime / 60) % 60);
        int secs = Mathf.FloorToInt(Time.unscaledTime % 60);
        // timeMain.text = hours.ToString("D2") + ":" + mins.ToString("D2") + ":" + secs.ToString("D2");
        timeMain.text = mins.ToString("D2") + ":" + secs.ToString("D2");
    }

    private void Update()
    {
        if (GameManager.Instance.startGameControlRef.isPVP)
        {
            return;
        }
        UpdateTotalRealTime();
        
        if (StopTimer)
            return;
        
        StartTimeLeftRaw();
        StartTimeLeftRound();
    }
    
    IEnumerator TimeRound()
    {
        if (StopMainTimer)
        {
            yield break;
        }

        if (second >= 60 || minute >= 60)
        {
            second = 0;
            minute++;
        }
        
        second++;
        
        // _timeSpan = TimeSpan.FromSeconds(Time.time);
        // print(_timeSpan.Seconds + "  sec");
        // print(_timeSpan.Minutes + " min");
        // String timeSec = _timeSpan.Seconds.ToString("00");
        // String timeMin = _timeSpan.Minutes.ToString("00");

        timeMain.text = minute.ToString("D2") + ":" + second.ToString("D2");
        yield return new WaitForSeconds(1f);
        StartCoroutine(TimeRound());
    }

    public void StartTimerPvp()
    {
        StartCoroutine(TimeRound());
    }

    void Start()
    {
        if (GameManager.Instance.startGameControlRef.isPVP)
        {
            return;
        }
        
        switch (GameSettings.Instance.CurrentDifficulty) // устоновить текущее время в зависимости от сложности
        {
            case DifficultyEnum.EASY:
                AmountRoundTimeLeft = timeLeftRoundEasy;
                AmountRawTimeLeft = timeLeftRawEasy;
                break;
            case DifficultyEnum.NORMAL:
                AmountRoundTimeLeft = timeLeftRoundNormal;
                AmountRawTimeLeft = timeLeftRawNormal;
                break;
            case DifficultyEnum.HARD:
                AmountRoundTimeLeft = timeLeftRoundHard;
                AmountRawTimeLeft = timeLeftRawHard;
                break;
        }
        
        // AmountRawTimeLeft = SaveManager.Instance.LoadRound("AmountRawTimeLeft"); 
        // AmountRoundTimeLeft = SaveManager.Instance.LoadRound("AmountRoundTimeLeft"); 
         
        StopTimer = false;
        IsRawTimeStart = true;
        IsRoundTimeStart = true;
        RoundTimeLeft = AmountRoundTimeLeft;
        RawTimeLeft = AmountRawTimeLeft;
        
        StartTimeLeftRound();
    }
 
    public void CheckRawTimeCompleteClassic() // если тру то успел раскрыть ряд за время и нужно добавить очки в тайм бонус
    {
        if (IsRawTimeStart)
        {
            ExpManager.Instance.CurrentExperienceClassic += 350;
            countTimeLeft++;
            StartNewRawTimeLeft();
        }
        else
        {
            StartNewRawTimeLeft();
        }
        
        ExpManager.Instance.ShowScore();
    }
    
    public void CheckRawTimeComple() // если тру то успел раскрыть ряд за время и нужно добавить очки в тайм бонус
    {
        if (IsRawTimeStart)
        {
            ExpManager.Instance.AddTimeExp();
            StartNewRawTimeLeft();
        }
        else
        {
            StartNewRawTimeLeft();
        }
    }
    
    public void CheckRoundTimeComplete() // если тру то успел за раунд
    {
        if (IsRoundTimeStart)
        {
            ExpManager.Instance.CurrentExperienceClassic += 2000;
            countRoundLeft =2000;
        }
    }

    private void StartNewRawTimeLeft()
    {
        IsRawTimeStart = true;
        RawTimeLeft = AmountRawTimeLeft;
        StartTimeLeftRaw();
    } 

    void StartTimeLeftRaw()
    {
        if (!IsRawTimeStart) 
            return;
        
        int minute = (int) (RawTimeLeft / 60);
        float second = RawTimeLeft % 60;
        timerRaw += Time.deltaTime;
        if (timerRaw >= 1f)
        {
            timerRaw = 0;
            RawTimeLeft--;
            leftTimeRaw.text = (minute.ToString("0,0") + ":" + second.ToString("0,0"));
            if (minute <= 0 & second <= 0)
            {
                IsRawTimeStart = false;
                leftTimeRound.text = (minute.ToString("0,0") + ":" + second.ToString("0,0"));
            }
        }
    }
    
    void StartTimeLeftRound()
    {
        if (!IsRoundTimeStart) 
            return;
        
        int minute = (int) (RoundTimeLeft / 60);
        float second = RoundTimeLeft % 60;
        timerRound += Time.deltaTime;
        if (timerRound >= 1f)
        {
            timerRound = 0;
            RoundTimeLeft--;
            leftTimeRound.text = (minute.ToString("0,0") + ":" + second.ToString("0,0"));
            if (minute <= 0 & second <= 0)
            {
                IsRoundTimeStart = false;
                leftTimeRound.text = (minute.ToString("0,0") + ":" + second.ToString("0,0"));
            }
        }
    }
}