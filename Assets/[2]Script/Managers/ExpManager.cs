﻿using System;
using System.Collections;
using Lean.Pool;
using TMPro;
using UnityEngine;

public class ExpManager : GenericSingletonClass<ExpManager>
{
    [SerializeField] public RawBarUi levelExp;
    [SerializeField] private RawBarUi timeExp;
    [SerializeField] private RawBarUi numExp;
    [SerializeField] private RawBarUi rawExp;
    [SerializeField] private TextMeshProUGUI bonusNumText;
    [SerializeField] private TextMeshProUGUI bonusLevelText;
    [SerializeField] private TextMeshProUGUI difficultyText;

    [SerializeField] private TextMeshProUGUI experienceText;
    [SerializeField] private TextMeshProUGUI experienceAllText;

    [SerializeField] public Transform timeBonusIcon;
    [SerializeField] public Transform rowBonusIcon;
    [SerializeField] public Transform levelBonusIcon;

    private int bonusNum = 0;
    public int ControlLife { get; set; } // контроль добавления жизней

    public int CurrentLevelExp { get; set; } // текущий уровень по лвлу
    // public int CurrentLevelExpClassic { get; set; } // текущий уровень по лвлу classic

    public int CurrentRoundExp { get; set; } // опыт для всех уровней
    public int CurrentRoundExpClassic { get; set; } // опыт для всех уровней classic

    public int CurrentExperience { get; set; } // текущий опыт  уровня
    public int CurrentExperienceClassic { get; set; } // текущий опыт  уровня classic
    public int CurrentExperienceX { get; set; } // текущий опыт х
    public int countExperienceX { get; set; } // счетчки по х
    public int CurrentExperienceY { get; set; } // текущий опыт Y
    public int countExperienceY { get; set; } //счетчик опыт Y
    public int CurrentExperienceBonus { get; set; } // все бонусы
    public int countExperienceBonus { get; set; } // счетчик бонусы
    public int CurrentExperienceRoundBonus { get; set; } // бонус уровня

    public int CurrentExperienceTimeRound { get; set; } // успел за время
    // public int CurrentExperienceRow { get; set; } // текущий опыт

    public int AmountRawWandLvl { get; set; } // сколько есть на данный момент в ряду делений
    public int CurrentExpWandLvl { get; set; } //какое количество  добовлять к делениям level

    public int AmountRawWandTime { get; set; } // сколько есть на данный момент в ряду делений
    public int CurrentExpWandTime { get; set; } //какое количество  добовлять к делениям Time bonus

    public int AmountRawWandNum { get; set; } // сколько есть на данный момент в ряду делений
    public int CurrentExpWandNum { get; set; } //какое количество  добовлять к делениям num bonus

    public int AmountRawWandRaw { get; set; } // сколько есть на данный момент в ряду делений
    public int CurrentExpWandRaw { get; set; } //какое количество  добовлять к делениям Row bonus

    public int WandCountToLevelTime { get; set; } // сколко палочек от тайм бонуса  добавить к левелу
    public int WandCountToLevelNum { get; set; } // сколко палочек от нам бонуса  добавить к левелу
    public int WandCountToLevelRaw { get; set; } // сколко палочек от бонуса ряда добавить к левелу
    public int WandCountToLevelRoundBonus { get; set; } // сколко палочек от раунд бонуса добавить к левелу

    public Action<ExpRawType> AddRawWandAction;

    public Action<int> AddLevelExpAction; // добавить к уровню какие то количество ячеек
    public Action<int> AddExpAction; // добавить к опыту

    public Action<string> SaveExpAction { get; set; } // сохранить бонусы опыта при завершении ранудна 

    private bool isfreezeLevelUp;

    public void AddLevelExp(int wandCount)
    {
        levelExp.AddExp(wandCount);
    }

    public void AddTimeExp()
    {
        timeExp.AddExp(CurrentExpWandTime);
        
        GameSettings.Instance.TimeBonusAchieve++;
        GameSettings.Instance.SaveTimeBonusAchieve();
        GameSettings.Instance.achiveManager.ShowAchieveStatus(GameSettings.Instance.TimeBonusAchieve, AchieveType.TIME_BONUS);
    }

    public void AddNumExp(int wandCount)
    {
        numExp.AddExp(wandCount);
        
        GameSettings.Instance.NumBonusAchieve++;
        GameSettings.Instance.SaveNumBonusAchieve();
        GameSettings.Instance.achiveManager.ShowAchieveStatus(GameSettings.Instance.NumBonusAchieve, AchieveType.NUM_BONUS);
    }

    public void AddRawExp()
    {
        rawExp.AddExp(CurrentExpWandRaw);
        
        GameSettings.Instance.RowBonusAchieve++;
        GameSettings.Instance.SaveRowBonusAchieve();
        GameSettings.Instance.achiveManager.ShowAchieveStatus(GameSettings.Instance.RowBonusAchieve, AchieveType.ROW_BONUS);
    }

    private void Start()
    {
        SaveManager.Instance.LoadStartGamePreferences();

        AddRawWandAction += ExpPanelLevelUp;
        SaveExpAction += SaveManager.Instance.SaveEndRoundGame;
        AddLevelExpAction += AddLevelExp;

        if (GameManager.Instance.startGameControlRef.sceneIndex == SceneIndex.TUTORIAL)
        {
            print("tutorial start exp");
            ShowStartRoundPositionTutor();
            SetNumBonusRound();
        }
        else
        {
            LoadStartExpParam();

            SetNumBonusRound();
            SetLevelStartRound();
            SetScoreStartRound();

            ShowStartRoundPosition();
        }

        difficultyText.text = GameSettings.Instance.CurrentDifficulty.ToString();
    }

    private void LoadStartExpParam() // начальные параметры опыта
    {
        CurrentExpWandLvl = SaveManager.Instance.LoadRound("CurrentExpWandLvl");
        CurrentExpWandTime = SaveManager.Instance.LoadRound("CurrentExpWandTime");
        CurrentExpWandNum = SaveManager.Instance.LoadRound("CurrentExpWandNum");
        CurrentExpWandRaw = SaveManager.Instance.LoadRound("CurrentExpWandRaw");

        WandCountToLevelTime = SaveManager.Instance.LoadRound("WandCountToLevelTime");
        WandCountToLevelNum = SaveManager.Instance.LoadRound("WandCountToLevelNum");
        WandCountToLevelRaw = SaveManager.Instance.LoadRound("WandCountToLevelRaw");
        WandCountToLevelRoundBonus = SaveManager.Instance.LoadRound("WandCountToLevelRoundBonus");
    }

    private void ShowStartRoundPosition() // показать при старте раунда текузие позиции 
    {
        levelExp.ShowRawStart(AmountRawWandLvl);
        timeExp.ShowRawStart(AmountRawWandTime);
        numExp.ShowRawStart(AmountRawWandNum);
        rawExp.ShowRawStart(AmountRawWandRaw);
    }

    private void ShowStartRoundPositionTutor() // показать при старте раунда текузие позиции для тутора 
    {
        CurrentExpWandLvl = 3;
        CurrentExpWandTime = 7;
        CurrentExpWandNum = 5;
        CurrentExpWandRaw = 4;
 
        WandCountToLevelTime = 1;
        WandCountToLevelNum = 1;
        WandCountToLevelRaw = 1;
        WandCountToLevelRoundBonus = 1;

        CurrentLevelExp = 0;
        bonusLevelText.text = 0.ToString();

        CurrentExperience = 0;
        experienceText.text = 0.ToString();

        levelExp.ShowRawStart(0);
        timeExp.ShowRawStart(0);
        numExp.ShowRawStart(0);
        rawExp.ShowRawStart(0);
    }

    public void ExpPanelLevelUp(ExpRawType expRawType)
    {
        switch (expRawType)
        {
            case ExpRawType.NONE:
                break;
            case ExpRawType.LEVEL_EXP:
                LevelUp();

                break;
            case ExpRawType.TIME_EXP:
                AddLevelExpAction.Invoke(WandCountToLevelTime);
                BonusManager.Instance.ActiveTimeBonusBtn();
                break;
            case ExpRawType.NUM_EXP:
                AddLevelExpAction.Invoke(WandCountToLevelNum);
                BonusManager.Instance.ActiveTimeBonusBtn();
                break;
            case ExpRawType.RAW_EXP:
                AddLevelExpAction.Invoke(WandCountToLevelRaw);
                BonusManager.Instance.ActiveTimeBonusBtn();
                break;
        }
    }

    private void LevelUp()
    {
        if (isfreezeLevelUp)
        {
            return;
        }
        
        AudioManagerGame.Instance.PlayAchive();

        isfreezeLevelUp = true;
        StartCoroutine(FreezeLevelUp()); // замарозить получение уровня на пару секунд

        CurrentLevelExp++;
        GameObject effectBum =
            LeanPool.Spawn(GameManager.Instance.effector.bonusUp, bonusLevelText.transform.position, Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
        ShowLevel();
        StartCoroutine(DelayShowRoulette());
        BtnClick.Instance.OfBtn = false;
    }

    IEnumerator DelayShowRoulette()
    {

        yield return new WaitForSecondsRealtime(0.5f);
        BonusManager.Instance.levelUp.Show();
        BtnClick.Instance.OfBtn = true;
    }

    IEnumerator FreezeLevelUp()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        isfreezeLevelUp = false;
    }

    public void ShowScore() // при каждом добовлении показываем очки
    {
        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            experienceText.text = CurrentExperienceClassic.ToString();
            experienceAllText.text = CurrentRoundExpClassic.ToString();
        }
        else
        {
            experienceText.text = CurrentExperience.ToString();
            experienceAllText.text = CurrentRoundExp.ToString();
        }
        
        // добавить эффекты
    }
 
    private void ShowLevel()
    {
        bonusLevelText.text = CurrentLevelExp.ToString();
    }

    private void SetLevelStartRound()
    {
        CurrentLevelExp = SaveManager.Instance.LoadRound("CurrentLevel");
        bonusLevelText.text = CurrentLevelExp.ToString();
    }

    private void SetScoreStartRound()
    {
        // CurrentExperience = SaveManager.Instance.LoadRound("CurrentExperience");
        CurrentExperience = 0; // на начала раунда ставим 0
        CurrentExperienceClassic = 0; // на начала раунда ставим 0
        CurrentRoundExp = SaveManager.Instance.LoadRound("CurrentRoundExp");
        CurrentRoundExpClassic = SaveManager.Instance.LoadRound("CurrentRoundExpClassic");

        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            experienceText.text = CurrentExperienceClassic.ToString();
            experienceAllText.text = CurrentRoundExpClassic.ToString();
        }
        else
        {
            experienceText.text = CurrentExperience.ToString();
            experienceAllText.text = CurrentRoundExp.ToString();
        }
    }

    private void SetNumBonusRound() // на начало раунда устоновить число
    {
        bonusNum = Helper.SetRandomNum(1, 9);
        bonusNumText.text = bonusNum.ToString();
    }

    public void CountBonusNum()
    {
        int countNum = GameManager.Instance.turnControl.CountBonusNum(bonusNum.ToString());
        AddNumExp(countNum);
    }

    public int AddExpToScoreX()
    {
        int result = 0;
        
        switch (GameSettings.Instance.CurrentDifficulty)
        {
            case DifficultyEnum.EASY:
                result = 10;
                break;
            case DifficultyEnum.NORMAL:
                result = 15;
                break;
            case DifficultyEnum.HARD:
                result = 20;
                break;
        }

        return result;
    }
    
    public int AddExpToScoreY()
    {
        int result = 0;
        
        switch (GameSettings.Instance.CurrentDifficulty)
        {
            case DifficultyEnum.EASY:
                result = 20;
                break;
            case DifficultyEnum.NORMAL:
                result = 30;
                break;
            case DifficultyEnum.HARD:
                result = 40;
                break;
        }

        return result;
    }
    
    public int AddExpToScoreBonusLevel()
    {
        int result = 0;
        
        switch (GameSettings.Instance.CurrentDifficulty)
        {
            case DifficultyEnum.EASY:
                result = 1000;
                break;
            case DifficultyEnum.NORMAL:
                result = 1200;
                break;
            case DifficultyEnum.HARD:
                result = 1500;
                break;
        }

        return result;
    }
    
    public int AddExpToScoreBonusTime()
    {
        int result = 0;
        
        switch (GameSettings.Instance.CurrentDifficulty)
        {
            case DifficultyEnum.EASY:
                result = 700;
                break;
            case DifficultyEnum.NORMAL:
                result = 850;
                break;
            case DifficultyEnum.HARD:
                result = 1000;
                break;
        }

        return result;
    }
    
    public int AddExpToScoreBonus(int bonusNum)
    {
        int result = 0;
        
        switch (GameSettings.Instance.CurrentDifficulty)
        {
            case DifficultyEnum.EASY:
                
                if (bonusNum == 1)
                {
                    result = 500;
                }
                
                if (bonusNum == 2)
                {
                    result = 250;
                }
                
                if (bonusNum == 3)
                {
                    result = 200;
                }
                
                if (bonusNum == 4)
                {
                    result = 100;
                }
                
                break;
            case DifficultyEnum.NORMAL:
                
   
                if (bonusNum == 1)
                {
                    result = 600;
                }
                
                if (bonusNum == 2)
                {
                    result = 280;
                }
                
                if (bonusNum == 3)
                {
                    result = 230;
                }
                
                if (bonusNum == 4)
                {
                    result = 120;
                }
                
                break;
            case DifficultyEnum.HARD:
                
                if (bonusNum == 1)
                {
                    result = 700;
                }
                
                if (bonusNum == 2)
                {
                    result = 320;
                }
                
                if (bonusNum == 3)
                {
                    result = 260;
                }
                
                if (bonusNum == 4)
                {
                    result = 140;
                }
             
                break;
        }

        return result;
    }
}