﻿
using System;
using System.Collections;
using UnityEngine;

public class AudioManagerGame : GenericSingletonClassDA<AudioManagerGame>
{
    [SerializeField] private AudioSource audioSourceSound;
    [SerializeField] private AudioSource audioSourceMusic;
    
    [SerializeField] private AudioClip clickButton;
    [SerializeField] private AudioClip fullRaw;
    [SerializeField] private AudioClip clickDel;
    [SerializeField] private AudioClip clickEmpty;
    [SerializeField] private AudioClip clickEndTurn;
    [SerializeField] private AudioClip findNumber;
    [SerializeField] private AudioClip testSound;
    
    [Header("game sound")]
    [SerializeField] private AudioClip prepareSound;
    [SerializeField] private AudioClip bonusBtnSound;
    [SerializeField] private AudioClip showBonusSound;
    [SerializeField] private AudioClip debufSound;
    [SerializeField] private AudioClip bufSound;
    [SerializeField] private AudioClip useBufSound;
    [SerializeField] private AudioClip createBonusSound;
    
    [SerializeField] private AudioClip winSound;
    [SerializeField] private AudioClip failSound;
    [SerializeField] private AudioClip achiveSound;
    [SerializeField] private AudioClip achiveSound2;

    bool isPlay;
    
    
    public void PlayPrepareSound()
    {
        audioSourceSound.PlayOneShot(prepareSound);
    }
    
    public void EndTurnSound2()
    {
        audioSourceSound.PlayOneShot(clickEndTurn);
    }
    
    public void PlayDebafSound()
    {
        if (isPlay)
        {
            return;
        }
        
        StartCoroutine(DeleyDebaf());
    }

    IEnumerator DeleyDebaf()
    {
        isPlay = true;
        audioSourceSound.PlayOneShot(debufSound);
        yield return new WaitForSecondsRealtime(0.1f);
        isPlay = false;
    }

    public void PlayBafSound()
    {
        audioSourceSound.PlayOneShot(bufSound);
    }
    
    public void PlayShowBonus()
    {
        audioSourceSound.PlayOneShot(showBonusSound);
    }
    
    public void BonusSound()
    {
        audioSourceSound.PlayOneShot(bonusBtnSound);
    }
    
    
    public void PlayButtonSound()
    {
        audioSourceSound.PlayOneShot(clickButton);
    }
    
    public void FullRawSound()
    {
        audioSourceSound.PlayOneShot(fullRaw);
    }

    public void DelSound()
    {
        audioSourceSound.PlayOneShot(clickDel);
    }
    
    public void EmptySound()
    {
        audioSourceSound.PlayOneShot(clickEmpty);
    }
    
    public void FindNumSound()
    {
        audioSourceSound.PlayOneShot(findNumber);
    }
    
    public void PlayTestSound()
    {
        audioSourceSound.PlayOneShot(testSound);
    }
    
    public void PlayTBonusCreate()
    {
        audioSourceSound.PlayOneShot(createBonusSound);
    }
    
    public void PlayWin()
    {
        audioSourceSound.PlayOneShot(winSound);
    }
    
    public void PlayFail()
    {
        audioSourceSound.PlayOneShot(failSound);
    }
    
    public void PlayAchive ()
    {
        print("PlayAchive");
        audioSourceSound.PlayOneShot(achiveSound);
    }
    
    public void PlayAchive2 ()
    {
        audioSourceSound.PlayOneShot(achiveSound2);
    }
}
