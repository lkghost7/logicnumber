﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
 
public class SaveManager : GenericSingletonClass<SaveManager>
{
    private int firstInitGame;
    
    private List<string> bonusSaveKeyName = new List<string>(); // ключи для бонуса для сейва

    private const string START_DIFFICULTY = "DifficultyStart";
    private const string RANK_KEY = "Rank";
    private const string RANK_KEY_CLASSIC = "RankClassic";
    
    private void Start()
    { 
        if (GameSettings.Instance.First == 0)
        {
            LoadingScene.Instance.SaveTutorialStatus(false); // сбросить тутор
            InitDefault();
            InitClassic();
        }
        
        SaveFirstGame();
    }
    
    private void SaveFirstGame()
    {
        PlayerPrefs.SetInt("firstInitGame", 1);
    }

    public void InitClassic()
    {
        GameSettings.Instance.SaveLevelClassic(23); // ставим classic
        SaveLifeClassic(5); // жизни для классик
        InitDefaultRankClassic();
        SetDefaultExpItemClassic();
    }
    
    public void InitDefault()
    {
        print("InitDefault");
        GameSettings.Instance.SaveLevel(1); // ставим первый левел
        LoadingScene.Instance.SaveDefaultDiff();
        PlayerPrefs.SetInt("isActiveDifficulty", 0);
        // SaveRound("firstInitGame", 0);
        SaveBonus(0);
        GetBonusName();
        DefaultValueBonus();
        SetDefaultExpItem();
        GameManager.Instance.InitLifeFirstStartGame();
        SetDefaultAddLevelWand();
        InitDefaultRank(); //ранг у всех 0
        
        // DefaultTimeManagerValue(DifficultyEnum.NORMAL);
    }
 
    private void DefaultValueBonus()
    {
        foreach (string key in bonusSaveKeyName)
        {
            if (key == "Life")
            {
                SaveRound(key, 3);
                continue;
            }
            
            if (key == "Сatastrophe")
            {
                SaveRound(key, 3);
                continue;
            }
            SaveRound(key, 10);
        }
    } 

    private void SetDefaultExpItem()
    {
        SaveRound("AmountRawWandLvl", 0);
        SaveRound("AmountRawWandTime", 0);
        SaveRound("AmountRawWandNum", 0);
        SaveRound("AmountRawWandRaw", 0);

        SaveRound("CurrentExpWandLvl",100);
        SaveRound("CurrentExpWandTime", 4);
        SaveRound("CurrentExpWandNum", 100);
        SaveRound("CurrentExpWandRaw", 3);
        
        // уровень
        SaveRound("CurrentLevel", 0);
        SaveRound("CurrentExperience", 0);
        
        // кнопки разблокировок
        SaveRound("CellBtnAmount", 0);
        SaveRound("RawBtnAmount", 0);
        SaveRound("MagicBtnAmount",0);
        SaveRound("ClearBtnAmount",0);
        
        SaveRound("CurrentRoundExp",  0);
        
        SaveRound("ControlLife",  0);
    }
    
    private void SetDefaultExpItemClassic()
    {
        // уровень
        SaveRound("CurrentExperienceClassic",0);
        SaveRound("CurrentRoundExpClassic",0);
    }

    private void SetDefaultAddLevelWand() // дефолтные параметры (доработать)
    {
        SaveRound("WandCountToLevelTime", 5);
        SaveRound("WandCountToLevelRaw", 4); 
        SaveRound("WandCountToLevelNum", 7);
        SaveRound("WandCountToLevelRoundBonus",12);
    }

    public void LoadStartGamePreferences() // загрузить начальные параметры
    {
        ExpManager expManager = ExpManager.Instance;
        BonusManager bonusManager = BonusManager.Instance;

        expManager.AmountRawWandLvl = LoadRound("AmountRawWandLvl");
        expManager.AmountRawWandTime = LoadRound("AmountRawWandTime");
        expManager.AmountRawWandNum = LoadRound("AmountRawWandNum");
        expManager.AmountRawWandRaw = LoadRound("AmountRawWandRaw");

        expManager.CurrentLevelExp = LoadRound("CurrentLevel");
        // expManager.CurrentExperience = LoadRound("CurrentExperience");
        expManager.CurrentRoundExp = LoadRound("CurrentRoundExp");
        expManager.CurrentRoundExpClassic = LoadRound("CurrentRoundExpClassic");

        expManager.ControlLife = LoadRound("ControlLife");
    }

    public void SaveEndRoundGame(string key) // сохранить текущие параметры при прохождении всего увровня
    {
        ExpManager expManager = ExpManager.Instance;
        BonusManager bonusManager = BonusManager.Instance;
        // сколько делений на начало раунда
        SaveRound("AmountRawWandLvl", expManager.AmountRawWandLvl);
        SaveRound("AmountRawWandTime", expManager.AmountRawWandTime);
        SaveRound("AmountRawWandNum", expManager.AmountRawWandNum);
        SaveRound("AmountRawWandRaw", expManager.AmountRawWandRaw);

        // сколько добовлять к каждому лвлу
        SaveRound("CurrentExpWandLvl", expManager.CurrentExpWandLvl);
        SaveRound("CurrentExpWandTime", expManager.CurrentExpWandTime);
        SaveRound("CurrentExpWandNum", expManager.CurrentExpWandNum);
        SaveRound("CurrentExpWandRaw", expManager.CurrentExpWandRaw);

        //текущие уровни опыта и лвла
        SaveRound("CurrentLevel", expManager.CurrentLevelExp);
        // SaveRound("CurrentExperience", expManager.CurrentExperience);

        // + к  лвлу от бонусов
        SaveRound("WandCountToLevelRaw", expManager.WandCountToLevelRaw);
        SaveRound("WandCountToLevelTime", expManager.WandCountToLevelTime);
        SaveRound("WandCountToLevelNum", expManager.WandCountToLevelNum);
        SaveRound("WandCountToLevelRoundBonus", expManager.WandCountToLevelRoundBonus);
        
        // кнопки разблокировок
        SaveRound("CellBtnAmount", bonusManager.CellBtnAmount);
        SaveRound("RawBtnAmount", bonusManager.RawBtnAmount);
        SaveRound("MagicBtnAmount", bonusManager.MagicBtnAmount);
        SaveRound("ClearBtnAmount", bonusManager.ClearBtnAmount);
    }
    
    public void SaveLife(int lifeCount)
    {
        PlayerPrefs.SetInt(GameManager.LifeNums, lifeCount);
    }
    
    public void SaveLifeClassic(int lifeCount)
    {
        PlayerPrefs.SetInt(GameManager.LifeNumsClassic, lifeCount);
    }
 
    public void SaveBonus(int bonusCount)
    {
        PlayerPrefs.SetInt(GameManager.BonusNums, bonusCount);
    }

    public void LoadLife()
    {
        PlayerPrefs.GetInt(GameManager.LifeNums);
    }

    public void LoadBonus()
    {
        PlayerPrefs.GetInt(GameManager.BonusNums);
    }

    public void SaveRound(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
    }

    public int LoadRound(string key)
    {
        return PlayerPrefs.GetInt(key);
    }

    private void GetBonusName()
    {
        bonusSaveKeyName.Add("Life");       //  1
        bonusSaveKeyName.Add("Lock cell");  //  2
        bonusSaveKeyName.Add("Unlock cell");//  3
        bonusSaveKeyName.Add("Freeze row"); //  4
        bonusSaveKeyName.Add("Unfreeze row");// 5
        bonusSaveKeyName.Add("Lock dell");  //  6
        bonusSaveKeyName.Add("Magic num");  //  7
        bonusSaveKeyName.Add("Lock empty"); //  8 
        bonusSaveKeyName.Add("Clear all");  //  9
        bonusSaveKeyName.Add("Random num"); //  10
        bonusSaveKeyName.Add("See number"); //  11
        bonusSaveKeyName.Add("White all");  //  12
        bonusSaveKeyName.Add("Time exp+");  //  13
        bonusSaveKeyName.Add("Hide row");    //  14
        bonusSaveKeyName.Add("Row exp+");    // 15
        bonusSaveKeyName.Add("Сatastrophe");    //  16
        bonusSaveKeyName.Add("Level+");    //  17
        bonusSaveKeyName.Add("bonus18");    //  18
    }

    public void InitDefaultRank()
    {
        for (int i = 0; i < 20; i++)
        {
            // GameManager.Instance.CurrentRank[i] = 0;
            PlayerPrefs.SetInt(RANK_KEY+i, 0);
        }
    }
    
    public void InitDefaultRankClassic()
    {
        for (int i = 0; i < 10; i++)
        {
            // GameManager.Instance.CurrentRank[i] = 0;
            PlayerPrefs.SetInt(RANK_KEY_CLASSIC+i, 0);
        }
    }

    public void SaveRank(int level, int rank) // записать ранг
    {
        // GameManager.Instance.CurrentRank[level] = rank;
        PlayerPrefs.SetInt(RANK_KEY+level, rank);
    }
    
    public void SaveRankClassic(int level, int rank) // записать ранг classic
    {
        // GameManager.Instance.CurrentRank[level] = rank;
        PlayerPrefs.SetInt(RANK_KEY_CLASSIC+level, rank);
    }

    // public int LoadRank(int level) // получить ранг из сейва
    // {
    //     return PlayerPrefs.GetInt(RANK_KEY + level);
    // }
    
    public void LoadAllRank() // получить все ранги
    {
        GameManager.Instance.CurrentRank.Clear();
        
        for (int i = 0; i < 20; i++)
        {
            int item = PlayerPrefs.GetInt(RANK_KEY + i);
            GameManager.Instance.CurrentRank.Add(item);
        }
    }
    
    public void LoadAllRankClassic() // получить все ранги
    {
        GameManager.Instance.CurrentRankClassic.Clear();
        
        for (int i = 0; i < 10; i++)
        {
            int item = PlayerPrefs.GetInt(RANK_KEY_CLASSIC + i);
            GameManager.Instance.CurrentRankClassic.Add(item);
        }
    }
    
    public void SaveVersionToFile(string version) 
    {
        SaveData data = new SaveData();
        data.version = version;
        File.WriteAllText(Application.persistentDataPath+"/version.json",JsonUtility.ToJson(version));
    }
    
    public string LoadVersionInLocalData() // загрузка данных из джейсона
    {
        SaveData data  = JsonUtility.FromJson<SaveData>(File.ReadAllText(Application.persistentDataPath +"/version.json"));
        return data.version;
    }
}

[Serializable] 
class SaveData 
{
    public string version;
}