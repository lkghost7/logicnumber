﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankItemUi : MonoBehaviour
{
    [SerializeField] public Text rankName;
    [SerializeField] public Text rankDifficulty;
    [SerializeField] public Text rankScore;
    [SerializeField] public Text rankNum;
    [SerializeField] public Text level;
}
