﻿using System;

[Serializable] 

public class RangItem
{
    public string id;
    public string name;
    public string difficulty;
    public int score;
    public string scoreText;
    public int num;
    public int level;
    
    public RangItem(string id, string name, string difficulty, int score, string scoreText, int num, int level)
    {
        this.id = id;
        this.name = name;
        this.difficulty = difficulty;
        this.score = score;
        this.scoreText = scoreText;
        this.num = num;
        this.level = level;
    }
    
    public RangItem()
    {
    }
}
