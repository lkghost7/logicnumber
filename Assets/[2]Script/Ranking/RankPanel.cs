﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RankPanel : MonoBehaviour
{
    [SerializeField] private RankItemUi[] ranks;
    
    private RangItem[] baseRangs = new RangItem[100]; 
    
    public void Show(bool isMenu = false)
    {
        if (isMenu)
        {
            gameObject.SetActive(true);
            GetComponent<CanvasGroup>().alpha = 1;
        }
        else
        {
            gameObject.SetActive(true);
            GetComponent<CanvasGroup>().alpha = 0;
            GetComponent<CanvasGroup>().DOFade(1, 1);
        }

        for (int i = 0; i < 100; i++)
        {
            RangItem item = new RangItem();
            item.name = "-----";
            item.difficulty = "-----";
            item.scoreText = 0.ToString();
            item.num = i+1;
            item.level = 0;

            baseRangs[i] = item;
        }

        StartCoroutine(RefreshRank());
    }
 
    IEnumerator RefreshRank()
    {
        if (GameSettings.Instance.RangUsers.Count == 0 || GameSettings.Instance.RangUsers == null)
        {
            GameManager.Instance.dataBaseRef.LoadTableData();
        }
        
        for (int i = 0; i < 10; i++)
        {
            ShowRanks();
            yield return new WaitForSecondsRealtime(0.7f);
        }
    }

    public int GetRank()
    {
        List<RangItem> rankUser = GameSettings.Instance.RangUsers;

        int countSnap = 0;
        int result = 0;


        foreach (RangItem rangItem in rankUser)
        {
            countSnap++;
            string id = rangItem.id;
            
            if (GameSettings.Instance.CurrentBdName != null && id == GameSettings.Instance.CurrentBdName)
            {
                print(" мое место" + countSnap);
                result = countSnap;
                break;
            }
        }
        return result;
    }

    private void ShowRanks()
    {
        List<RangItem> rangList = GameSettings.Instance.RangUsers;

        if (rangList.Count == 0)
        {
            return;
        }
    
        int countRank = 0;


        foreach (RangItem rangItem in rangList)
        {
            baseRangs[countRank].name = rangItem.name;
            baseRangs[countRank].difficulty = rangItem.difficulty;
            baseRangs[countRank].scoreText = rangItem.score.ToString();
            baseRangs[countRank].level = rangItem.level;
            
            countRank++;
            if (countRank >= 100)
            {
                break;
            }
        }
        
        int count = 0;
        
        foreach (RankItemUi rankItemUi in ranks) // вывести в таблицу
        {
            count++;
            RangItem rang = baseRangs[count - 1];
            rankItemUi.rankNum.text = count.ToString();
            rankItemUi.rankName.text = rang.name;
            rankItemUi.rankDifficulty.text = rang.difficulty;
            
            rankItemUi.level.text = rang.level.ToString();
            rankItemUi.rankScore.text = rang.scoreText;
        }
    }
  
    public void Hide()
    {
        GetComponent<CanvasGroup>().DOFade(0, 1).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }

    public void Exit()
    {
        GameManager.Instance.startGameControlRef.LoadMainMenu();
    }
}
