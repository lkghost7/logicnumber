﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Helper

{
    public static int[] SetUniqueArray(int arrayCount, int maxNum, int minNum)
    {
        HashSet<int> set = new HashSet<int>();
        while (set.Count < arrayCount)
        {
            set.Add(SetRandomNum(maxNum, minNum));
        }

        return set.ToArray();
    }

    public static int SetRandomNum(int maxNum, int minNum)
    {
        return Random.Range(minNum, maxNum);
    }

    public static int[] SetUniqueArrayEven(int arrayCount, int maxNum, int minNum)
    {
        HashSet<int> set = new HashSet<int>();
        while (set.Count < arrayCount)
        {
            int num = SetRandomNum(maxNum, minNum);
            if (num % 2 == 0)
            {
                set.Add(num);
            }
        }

        return set.ToArray();
    }

    public static int[] SetUniqueArrayOdd(int arrayCount, int maxNum, int minNum)
    {
        HashSet<int> set = new HashSet<int>();
        while (set.Count < arrayCount)
        {
            int num = SetRandomNum(maxNum, minNum);
            if (num % 2 != 0)
            {
                set.Add(num);
            }
        }

        return set.ToArray();
    }

    public static int[] SetRepeatNum(int arrayCount)
    {
        int[] result = new int[] { };

        int numRandom = Random.Range(1, 10);

        switch (numRandom)
        {
            case 1:
                result = SetRandomRepeat(arrayCount);
                break;
            
            case 2 :
                result = SetTwoRepeat(arrayCount, 1,4);
                break;
            
            case 3 :
                result = SetTwoRepeat(arrayCount, 1,6);
                break;

            case 4:
                result = SetRandomRepeat(arrayCount);
                break;
            
            case 5:
                result = SetRandomRepeat(arrayCount);
                break;
            
            case 6:
                result = SetTwoRepeat(arrayCount,4 ,10);
                break;
            
            case 7:
                result = SetTwoRepeat(arrayCount, 7, 10);
                break;
            
            case 8:
                result = SetRandomRepeat(arrayCount);
                break;
            
            case 9:
                result = SetRandomRepeat(arrayCount);
                break;
        }

        return result;
    }

    private static int[] SetOne(int arrayCount) // выдаст рандомно полностью повторяющейся число
    {
        List<int> list = new List<int>();

        int repeatNum = Random.Range(1, 10);

        for (int i = 0; i < arrayCount; i++)
        {
            int num = Random.Range(repeatNum, repeatNum);
            list.Add(num);
        }
        return list.ToArray();
    }
    
    private static int[] SetRandomRepeat(int arrayCount) // выдаст просто ранлдом где может повторятся
    {
        List<int> list = new List<int>();
        
        for (int i = 0; i < arrayCount; i++)
        {
            int num = Random.Range(1, 10);
            list.Add(num);
        }
        return list.ToArray();
    }
    
    private static int[] SetTwoRepeat(int arrayCount, int firs, int end) // выдаст ограниченный рандом 
    {
        List<int> list = new List<int>();
        for (int i = 0; i < arrayCount; i++)
        {
            int num = Random.Range(firs, end);
            list.Add(num);
        }
        return list.ToArray();
    }

}