﻿using UnityEngine;

public class HelperBtn : MonoBehaviour
{
    [SerializeField] private GameObject helperBtns;
    
    public void LoadHelper()
    {
        if (GameSettings.Instance.isHelperOn)
        {
            helperBtns.SetActive(true);
            return;
        }
        
        helperBtns.SetActive(false);
    }
}
