﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [Tooltip("Показывать тултип только если кнопка недоступна")] public bool disabledOnly = false;
    public GameObject tooltip;
    public GameObject tooltipOnlyDisable;

    Button btn;

    private void Awake()
    {
        btn = GetComponent<Button>();
    }

    private void Start()
    {
        tooltip.SetActive(false);
        if (tooltipOnlyDisable != null)
        {
            tooltipOnlyDisable.SetActive(false);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (btn != null)
        {
            if (!btn.interactable && tooltipOnlyDisable != null)
            {
                tooltip.SetActive(false);
                tooltipOnlyDisable.SetActive(true);
            }
        
            if (btn.interactable && disabledOnly)
            {
                //don't show for enabled button;
                return;
            }

            if (btn.interactable)
            {
                tooltip.SetActive(true);
            }
        }
        else
        {
            tooltip.SetActive(true);
        }
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltip.SetActive(false);
        if (tooltipOnlyDisable != null)
        {
            tooltipOnlyDisable.SetActive(false);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (btn == null || !btn.interactable)
        {
            return;
        }
        
        tooltip.SetActive(false);
        if (tooltipOnlyDisable != null)
        {
            tooltipOnlyDisable.SetActive(false);
        }
    }

    private void OnDisable()
    {
        tooltip.SetActive(false);
        if (tooltipOnlyDisable != null)
        {
            tooltipOnlyDisable.SetActive(false);
        }
    }
}