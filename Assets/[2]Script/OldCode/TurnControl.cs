﻿using System;
using System.Collections;
using Lean.Pool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class TurnControl : MonoBehaviour
{
    [Header("Winner panel")] public int[] magicNumber; //назначить из уровня - само загаданое число
    [SerializeField] [Tooltip("green")] private GameObject greenWin;
    [SerializeField] [Tooltip("red")] private GameObject redWin;
    public TextMeshProUGUI[] rawWinner; // верхнняя строчка для вывод результатов, по умолчанию ? ? ? ? ?
    public Transform[] parentSpawn; // панель для вывода результата слева
    public GameObject[] rawsRezult; // панель для вывода результата слева
    [SerializeField] public Transform luckyMagicTransform;
    
    [Header("Nums")]
    public TextMeshProUGUI[] userKeyboard = new TextMeshProUGUI[5]; // в userKeyboard  записаны данные введеные с клавиатуры

    [SerializeField] public int numLimit = 0; // лимит для каждой сцены
    [SerializeField] public int rawLimit = 0; // лимит для рядов, можно заморозить верхние
    public GameObject[] raws; // количество строк для сцены
    
    [Header("Buttons")] [SerializeField] private GameObject holdBtn;
    [SerializeField] private InitData initData;
    
    [Header("Shiny")] [SerializeField] private RawResultShiny rawResultShiny;
    [SerializeField] private ShinyButtonControl shinyButtonControl;
    [SerializeField] public RawResultShiny shinyMainWindowControl;

    private bool[] repeatMagicNums = new bool[9]; // массив булов для вычесления повторов
    private int[] repaetCountMagicNum = new int[9] {1, 2, 3, 4, 5, 6, 7, 8, 9}; //  для записи повторов из magicNumber
    private int[] repeatNumKeyboard = new int[9]; // для записи повторов из  userKeyboard
    private int[] userKeyboardInts = new int[9] {0, 0, 0, 0, 0, 0, 0, 0, 0}; // для перелива цифр в этот массив, по умолчанию 0

    private int countExelentNum; // количество удачных попаданий
    private int countFailNum; // колличество фейлов
    private int countEndTurn; // для подсчета индексов для перехода на слудщий турн
    private int currentRawView = 0; // переменная для отслеживание текущей строки

    public int markNumber { get; set; } = 0; // переменная для перемещения по ячейкам
    public int rawNumber { get; set; } = 0; // перменная для отслеживания текущей строки
    private TextMeshProUGUI numText; // переменная для записи текущего числа из трансформа
    private TextMeshProUGUI numTextLightNum; // переменная для записи эффект для подсветки
    public bool chekRawEndWinner { get; set; } // проверка на последнюю строку

    private GameObject effect2;
    private InitEffects _initEffects;

    private Effector effector; // все эффекты

    private GameObject effect; // записать эффекты

    public bool blockButton { get; set; }
 
    public LuckyBonusItem currentLuckyBonus { get; set; }
    public LuckyBonusItem currentWinLuckyBonus { get; set; } = new LuckyBonusItem();

    private int scoreWinnerPanel;

    private bool isWinner;

    private void Start()
    {
        // StartCoroutine(initData.SetStartNum(numLimit));
        //
        // chekRawEndWinner = false;
        // effector = FindObjectOfType<Effector>();
        // initData.InitUnicMagicNum(numLimit);
        // StartCoroutine(SetQuestionToWinner()); // только для сцены
    }

    public int GetFreeRow()
    {
        int row = 11 - currentRawView;

        if (row != 0)
        {
            return row;
        }
        else
        {
            return 1;
        }
    }

    public void StartInit()
    {
        chekRawEndWinner = false;
        effector = FindObjectOfType<Effector>();
        initData.InitUniqMagicNum(numLimit);
        StartCoroutine(SetQuestionToWinner()); // только для сцены
    } 

    public void Init()
    {
        StartCoroutine(SetQuestionToWinner());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            LightEffectStop();
        }
    }

    private void ControlRaw()
    {
        raws[currentRawView].GetComponent<RawControl>().turnComplete = true;
    }

    public void EndTurn() // нажатие кнопки конец тура
    {
        AudioManagerGame.Instance.EndTurnSound2();
        blockButton = false; // заблокировать все кнопки для нажатия на вермя проверок
        ControlRaw();
        GetNumForUserkeyboard(); // получить из трансформа данные введеные с клавиатуры и поместить в userKeyboard
        if (CheckEndCell()) return; // проверка на последнюю ячейку - если пустая не переходить на турн
        NextTurn(); // запустить следущий турн

        CheckPvpRow();
        
        if (isWinner)
        {
            return;
        }
        
        CheckEndRaw(); // проверка на последнюю строку в сцене
        FirstCells(); // включить первую ячейку в ряду и выключить кнопку акцепт
    }

    private void CheckPvpRow()
    {
        if (GameManager.Instance.startGameControlRef.isPVP)
        {
            if (GameManager.Instance.startGameControlRef.isPVPSolo)
            {
                print(" no connect");
            }
            else
            {
                GameManager.Instance.gameNetManager.SendRowStatus(rawNumber, currentWinLuckyBonus.Green,currentWinLuckyBonus.Red);
            }
            
        }
    }

    // private void 
    private void FirstCells()
    {
        rawResultShiny.Show();
        shinyButtonControl.ButtonAcceptDisable();
        Image imageSecond = raws[rawNumber].transform.GetChild(markNumber).GetComponent<Image>();
        imageSecond.GetComponent<CellsShiny>().Show();
        // StartCoroutine(BlockBtnRouletteDelay());

    }

    // IEnumerator BlockBtnRouletteDelay()
    // {
    //     BtnClick.Instance.isFreezeRoulette = true;
    //     yield return new WaitForSecondsRealtime(2f);
    //     BtnClick.Instance.isFreezeRoulette = false;
    // }

    private void NextTurn()
    {
        Clear(); // очистить переменные и массив перед следущим ходом
        SetUserNumber(); // вычеслить нужную цифру и записать в  userKeyboardInts
        SetStarCountMagic(); // тру для массива
        SetRepeatNum(); // добавить все повторы в каунт
        CheckTurn(); //  запустить проверку хода
    }
 
    public void PressBtn(String getBtn) // обрработка нажатия клавиши
    {
        if (markNumber == numLimit)
        {
            blockButton = true;
            return;
        }

        Image currentCels = raws[rawNumber].transform.GetChild(markNumber).GetComponent<Image>();

        if (currentCels.GetComponent<CellsShiny>().isLock) // если заблокирована
        {
            print("currentCels btn" + currentCels);
            currentCels.GetComponent<CellsShiny>().Hide();
            markNumber++;

            PressBtn(getBtn);
            return;
        }

        blockButton = false;

        numText = currentCels.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        numText.text = getBtn;
        initData.SetColorNumber(getBtn, numText); // метод для установки конкретного цвета

        GameObject effect = LeanPool.Spawn(effector.RawEffect, numText.transform.position, Quaternion.identity);
        StartCoroutine(effector.Despawner(effect, 1));

        currentCels.GetComponent<CellsShiny>().Hide();

        if (markNumber + 1 != numLimit) // лимит не достигнут при добавлении
        {
            Image secondCells = raws[rawNumber].transform.GetChild(markNumber + 1).GetComponent<Image>();

            if (secondCells.GetComponent<CellsShiny>().isLock)
            {
                Image secondTwoCells = raws[rawNumber].transform.GetChild(markNumber + 2).GetComponent<Image>();
                secondTwoCells.GetComponent<CellsShiny>().Show();
            }
            else
            {
                secondCells.GetComponent<CellsShiny>().Show();
            }

            shinyButtonControl.ButtonAcceptDisable(); // выключить перевод строки
        }
        else
        {
            shinyButtonControl.ButtonAcceptEnable(); // включить перевод строки кнопку
        }

        if (markNumber < numLimit)
        {
            markNumber++;
        }

        if (markNumber == numLimit)
        {
            blockButton = true;
        }
    }

    public void Del() // метод для удаления цифр
    {
        blockButton = false;
        if (markNumber > 0) // проверить индекс и отнять только если цифра есть
        {
            markNumber--;
        }
        
        for (int i = 0; i < numLimit; i++)
        {
            Image cell = raws[rawNumber].transform.GetChild(i).GetComponent<Image>();
            cell.GetComponent<CellsShiny>().Hide();
        }

        Image imageFirst = raws[rawNumber].transform.GetChild(markNumber).GetComponent<Image>();
        TextMeshProUGUI indexForDel = imageFirst.transform.GetChild(0).GetComponent<TextMeshProUGUI>();

        if (imageFirst.GetComponent<CellsShiny>().isLock) // если заблокирована
        {
            Image imageSecond = raws[rawNumber].transform.GetChild(markNumber + 1).GetComponent<Image>();
            imageSecond.GetComponent<CellsShiny>().Hide();
            Del();

            return;
        }

        if (indexForDel.text.Equals("")) // проверить если в ячейка пустая ничего не делать
        {
            return;
        }

        // создать эффект удаления и назначить в эту ячейку "" пустой символ
        GameObject effect = LeanPool.Spawn(effector.delEffect, indexForDel.transform.position, Quaternion.identity);
        StartCoroutine(effector.Despawner(effect, 1));
        indexForDel.text = "";

        imageFirst.GetComponent<CellsShiny>().Show();

        if (markNumber + 1 != numLimit) // лимит не достигнут при добавлении
        {
            Image imageSecond = raws[rawNumber].transform.GetChild(markNumber + 1).GetComponent<Image>();
            imageSecond.GetComponent<CellsShiny>().Hide();

            shinyButtonControl.ButtonAcceptDisable(); // выключить перевод строки
        }
        else
        {
            shinyButtonControl.ButtonAcceptEnable(); // включить перевод строки кнопку
        }

        if (markNumber != numLimit) // лимит не достигнут при добавлении
        {
            shinyButtonControl.ButtonAcceptDisable(); // выключить перевод строки
        }
        else
        {
            shinyButtonControl.ButtonAcceptEnable(); // включить перевод строки кнопку
        }
    }
 
    public void EmptyBtn()
     {
        if (markNumber == numLimit)
        {
            blockButton = true;
            return;
        }

        Image imageFirst = raws[rawNumber].transform.GetChild(markNumber).GetComponent<Image>();
        
        if (imageFirst.GetComponent<CellsShiny>().isLock) // если ячейка заблочена
        {
            imageFirst.GetComponent<CellsShiny>().Hide(); // спрятать ее  прибавить +1
            print(imageFirst + " imageFirst");
            markNumber++;
            EmptyBtn();
            return;
        }

        blockButton = false;

        numText = imageFirst.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        // numText.text = " ";
        numText.text = "<sprite name=Empty3>";
        numText.color = initData.colorUser[Random.Range(0, 8)];

        GameObject effect = LeanPool.Spawn(effector.RawEffect, numText.transform.position, Quaternion.identity);
        StartCoroutine(effector.Despawner(effect, 1));

        imageFirst.GetComponent<CellsShiny>().Hide();

        if (markNumber + 1 != numLimit) // лимит не достигнут при добавлении
        {
            Image imageSecond = raws[rawNumber].transform.GetChild(markNumber + 1).GetComponent<Image>();

            if (imageSecond.GetComponent<CellsShiny>().isLock)
            {
                Image secondTwoCells = raws[rawNumber].transform.GetChild(markNumber + 2).GetComponent<Image>();
                secondTwoCells.GetComponent<CellsShiny>().Show();
            }
            else
            {
                imageSecond.GetComponent<CellsShiny>().Show();
            }

            shinyButtonControl.ButtonAcceptDisable(); // выключить перевод строки
        }
        else
        {
            shinyButtonControl.ButtonAcceptEnable(); // включить перевод строки кнопку
        }

        if (markNumber < numLimit)
        {
            markNumber++;
        }

        if (markNumber == numLimit)
        {
            blockButton = true;
        }
    }

    private void GetNumForUserkeyboard()
    {
        for (int i = 0; i < numLimit; i++) // запустить цикл на количество ячеек - базовое 5
        {
            // записать  в endBtn  текущий номер ячейки rawNumber, каждую итерацию  записать из трансформы raws
            // каждый компонент текста (цифры полученные с клавиатуры) 1-9 в  userKeyboard
            Image image = raws[rawNumber].transform.GetChild(i).GetComponent<Image>();
            TextMeshProUGUI endBtn = image.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            userKeyboard[i].text = endBtn.text; // в userKeyboard  записаны данные из клавиатуры
        }
    }

    private bool CheckEndCell()
    {
        // если в  userKeyboard последняя ячейка пустая то кнопка не сработает и на турн не перейдет
        if (userKeyboard[numLimit - 1].text.Equals("")) // в userKeyboard  записаны данные из клавиатуры
        {
            return true;
        }

        return false;
    }

    private void SetUserNumber() // вычеслить нужную цифру и записать от 1 до 9 в userKeyboardInts
    {
        // валидиция цифр
        for (int i = 0; i < numLimit; i++) // запустить 5 раз
        {
            for (int j = 1; j < 10; j++) // запустить 9 раз для 9 цифр
            {
                string equalsNum;
                equalsNum = j.ToString(); // записать число от 1 до 10 в equalsNum
                // в userKeyboard  записаны данные из клавиатуры

                if (userKeyboard[i].text.Equals(equalsNum)) // если  то что ввели с клавиатура равно 1 до 10 то 
                {
                    userKeyboardInts[i] = int.Parse(equalsNum); // если нашел число от 1 до 10 записать в userKeyboardInts
                }
            }
        }
    }

    private void Clear() // очистить все переменные перед ходом
    {
        countEndTurn = 0; // для подсчета индексов для перехода на слудщий турн
        countFailNum = 0; // количесвто фейлов на ноль
        countExelentNum = 0; // количество удачных попаданий
        // winnerGreen = "";
        // winnerRed = "";

        for (int i = 0; i < 9; i++)
        {
            repaetCountMagicNum[i] = 0; // сбросить переменную для повтров
            repeatNumKeyboard[i] = 0; // посчитанные повторы сбросить на ноль
            userKeyboardInts[i] = 0;
        }
    }

    private void CheckTurn() // запуск турна для проверки - кнопка эндтурн
    {
        for (int currentIndex = 0; currentIndex < numLimit; currentIndex++)
        {
            ChekNumber(currentIndex); // запустить вычисление цифр 5 раз
        }

        CheckRepeatCount(); // проверить на повторы
        CheckWin(); // проверить на победу

        CheckCompleteBonuses(); // проверить на выполнение бонусов
        CheckScores(); // проверить сколько очков
    }

    private void CheckEndRaw() // GameOver
    {
        if (rawNumber >= rawLimit) // если равнамбер больше больше  то не добовлять rawNumber++; и это проигрыш
        {
            StartCoroutine(WinnerEffectAndNumber());
          
            BonusManager.Instance.UpdateBonus.Invoke("");
            ExpManager.Instance.SaveExpAction.Invoke("");
            StartCoroutine(GameManager.Instance.StageFail());
            markNumber = 0;
            
            if (GameManager.Instance.startGameControlRef.isPVP)
            {
                if (GameManager.Instance.startGameControlRef.isPVPSolo)
                {
                    // вызвать панель проиграл
                    print(" game over");
                    SoloGameManager.Instance.Show(0);
                    
                }
                else
                {
                    GameManager.Instance.gameNetManager.SendFailStatus(1);
                    print("SendWinStatus");
                }
            }
            
            return;
        }

        rawNumber++; // добавить для перехода на следущую строчку
        markNumber = 0; // обнулить номера для перехода по индексу

        if (GameManager.Instance.EndRoundTime)
        {
            print("   if (GameManager.Instance.EndRoundTime)");
            return;
        }

        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            TimeManager.Instance.CheckRawTimeCompleteClassic();
            return;
        }
        
        TimeManager.Instance.CheckRawTimeComple();
        ExpManager.Instance.AddRawExp(); // добавить к рав бонусу
        ExpManager.Instance.CountBonusNum();
    }

    private void CheckWin() // проверка на победу - все числа совпали
    {
        if (countEndTurn >= numLimit) // если countEndTurn равен входящему лимиту numLimit 5 = 5 то проверить на вин 
        {
            if (GameManager.Instance.startGameControlRef.sceneIndex == SceneIndex.TUTORIAL)
            {
                print("LeftWinPanel");
                StartCoroutine(LeftWinPanel());
                return;
            }
              
            GameManager.Instance.EndRoundTime = true;

            if (GameManager.Instance.startGameControlRef.isClassic)
            {
                TimeManager.Instance.CheckRoundTimeComplete();
            }
            
            if (GameManager.Instance.startGameControlRef.isPVP)
            {
                if (GameManager.Instance.startGameControlRef.isPVPSolo)
                {
                    SoloGameManager.Instance.Show(1);
                }
                else
                {
                    GameManager.Instance.gameNetManager.SendWinStatus(1);
                }
            }
            
            print("win");
            isWinner = true;
            BonusManager.Instance.UpdateBonus.Invoke("");
            ExpManager.Instance.SaveExpAction.Invoke("");
            StartCoroutine(WinnerEffectAndNumber());
            StartCoroutine(LeftWinPanel());
            ChekRank();
            StartCoroutine(GameManager.Instance.GameRoundWin());
            markNumber = 0;
            return;
        }

        StartCoroutine(LeftWinPanel());
    }

    private void ChekRank()
    {
        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            ClassicRank();
        }
        else
        {
            GameRank();
        }
    }

    private void GameRank()
    {
        if (currentRawView <= 7)
        {
            GameManager.Instance.CurrenLevelRank = 3; // если вложился в 8 рядов, то ранг 3
        }
        
        if (currentRawView == 8)
        {
            GameManager.Instance.CurrenLevelRank = 2; // если вложился в 9 рядов, то ранг 2
        }
        
        if (currentRawView >= 9)
        {
            GameManager.Instance.CurrenLevelRank = 1; // если вложился в 10 рядов, то ранг 1
        }
    }

    private void ClassicRank()
    {
        if (currentRawView <= 7)
        {
            GameManager.Instance.CurrenLevelRankClassic = 3; // если вложился в 8 рядов, то ранг 3
        }
        
        if (currentRawView == 8)
        {
            GameManager.Instance.CurrenLevelRankClassic = 2; // если вложился в 9 рядов, то ранг 2
        }
        
        if (currentRawView >= 9)
        {
            GameManager.Instance.CurrenLevelRankClassic = 1; // если вложился в 10 рядов, то ранг 1
        }
    }
    
    IEnumerator LeftWinPanel() // отоброзить в левой панели результат
    {
        scoreWinnerPanel = 0;
        GameObject rezultTextObj = rawsRezult[currentRawView];

        // GameObject burst = LeanPool.Spawn(effector.burstEffect, rezultTextObj.transform.position, effector.burstEffect.transform
        // .rotation);

        rezultTextObj.GetComponent<Shtorka>().Hide();
        // StartCoroutine(effector.Despawner(burst, 1));
        StartCoroutine(GoodShot()); // нарисовать удачные
        StartCoroutine(FailShot()); // нарисовать фейлы
        currentRawView++;
       
        yield return null;
    }

    IEnumerator GoodShot() // нарисовать удачные
    {
        int countGreen = 0;
        currentWinLuckyBonus.Green = 0;
        Transform rezultRaw = parentSpawn[currentRawView]; // получать текущий парент спауна для результата
        for (int i = 0; i < countExelentNum; i++) //нарисовать удачные попадания такого же колличества как countExelentNum
        {
            // winnerGreen = winnerGreen + "✔";
            LeanPool.Spawn(greenWin, rezultRaw);
            scoreWinnerPanel += ExpManager.Instance.AddExpToScoreY();
            ExpManager.Instance.CurrentExperienceClassic += 30;
            
            ExpManager.Instance.CurrentExperienceY += ExpManager.Instance.AddExpToScoreY();
            ExpManager.Instance.countExperienceY++;
            countGreen++;
        }

        // rezultText.text = winnerGreen;
        currentWinLuckyBonus.Green = countGreen;
        yield return null;
    }

    IEnumerator FailShot() // нарисовать фейлы 
    {
        int countRed = 0;
        currentWinLuckyBonus.Red = 0;
        Transform rezultRaw = parentSpawn[currentRawView]; // получать текущий парент спауна для результата
        for (int i = 0; i < countFailNum; i++) // нарисовать столько фейлов сколько в countFailNum
        {
            // winnerRed = winnerRed + "✘";  //дорисовать в виннер нужное количество фейлов
            LeanPool.Spawn(redWin, rezultRaw);
            scoreWinnerPanel += ExpManager.Instance.AddExpToScoreX();
            ExpManager.Instance.CurrentExperienceX += ExpManager.Instance.AddExpToScoreX();
            ExpManager.Instance.CurrentExperienceClassic += 15;
            ExpManager.Instance.countExperienceX ++;
            
            countRed++;
        }

        currentWinLuckyBonus.Red = countRed;
        // rezultText.text = winnerRed; 
        yield return null;
    }

    public void DrawLuckyBonusMagic(LuckyBonusItem luckyBonusItem) // нарисовать lucky Bonus magic
    {
        currentLuckyBonus = luckyBonusItem;

        for (int i = 0; i < luckyBonusItem.Green; i++)
        {
            LeanPool.Spawn(greenWin, luckyMagicTransform);
        }

        for (int i = 0; i < luckyBonusItem.Red; i++)
        {
            LeanPool.Spawn(redWin, luckyMagicTransform);
        }
    }

    private void CheckRepeatCount() // посчитать количество повторов и убрать в фейлах лишние если есть.
    {
        for (int i = 0; i < 9; i++)
        {
            if (repeatNumKeyboard[i] < 0)
            {
                // если в repeatNumSave 0 или -1 то ничего не делать 
            }
            else
            {
                if (repeatMagicNums[i]) // если в итерации повтор тру то то отнять от фейлов
                {
                    // из повторов клавиатуры отнять повторы числа
                    int rezultCount1 = repeatNumKeyboard[i] - repaetCountMagicNum[i];
                    countFailNum = countFailNum - rezultCount1; // скорректировать фейлы - отнять лишние повторы
                }
            }
        }
    }

    private void SetStarCountMagic() //стартовые значения true для массива булов
    {
        for (int i = 0; i < 9; i++)
        {
            repeatMagicNums[i] = true;
        }
    }

    // arrayIndex на вход
    private void ChekNumber(int arrayIndex) //проверяет на совпадение цифры, если попал +1 +1
    {
        FindRepeatNum(arrayIndex); // вычеслить повторы - дать на вход текущий индекс
        int parseTextInput = userKeyboardInts[arrayIndex]; // записать в parseTextInput  из  userKeyboardInts [номер  текущего индекса]
        if (magicNumber[arrayIndex] == parseTextInput) // если magicNumber[текущий индекс]  равен  parseTextInput
            // то добавить   countEndTurn++ для подсчета всех индексов для хода 5 по умолчанию
        {
            // количество удачных плпаданий такое же как количесвто индексов конца тура и автоматически вин.
            countEndTurn++; // добавить к концу турна
            countExelentNum++; // количество удачных попаданий
            return;
        }

        FindOtherNumber(arrayIndex); // если не попал в сравнение то проверить на фейл
    }

    // отнимает в цикле турна от цифры -1 если попал на цифру - для исключения повторов
    private void FindRepeatNum(int arrayIndex) // вычеслить повторы пробежаться по magicNumber
    {
        for (int i = 0; i < 9; i++)
        {
            if (magicNumber[arrayIndex] == i + 1) // если magicNumber равен  цифер от 1 до 9 то отнять
                // от     repaetCount  и   repeatNumSave  текущего индекса  
            {
                repaetCountMagicNum[i]--;
                repeatNumKeyboard[i]--;
            }
        }
    }

    private void SetRepeatNum() //   пробежатся по массиву и найти все повторы цифр и записать их  колличество
    {
        for (int i = 0; i < numLimit; i++)
        {
            for (int k = 0; k < 9; k++)
            {
                int parse = userKeyboardInts[i];
                if (k + 1 == parse)
                {
                    repeatNumKeyboard[k]++; // повторы для  userKeyboardInts
                }

                if (magicNumber[i] == k + 1)
                {
                    repaetCountMagicNum[k]++; // повторы для  magicNumber
                }
            }
        }

        SetRepeatMagicNums();
    }

    private void SetRepeatMagicNums() // устоновить фолс в тех числах что не поапали в массив
    {
        for (int index = 0; index < 9; index++)
        {
            if (repaetCountMagicNum[index] == 0)
            {
                repeatMagicNums[index] = false;
            }
        }
    }

    // метод для проверки фейла, если есть хоть 1 цифра во всем
    // массиве то добавить к фейлу, если нету то ничего не добовлять
    private void FindOtherNumber(int arrayIndex)
    {
        int inputParse = userKeyboardInts[arrayIndex];

        for (int i = 0; i < numLimit; i++)
        {
            if (magicNumber[i] == inputParse)
            {
                countFailNum++; // колличеыство фейлов ++
                break;
            }
        }
    }

    // ------------------------------- БОНУСЫ -----------------------------//
    public void ChangeMagicNum() // поменять цифру (дебаф)
    {
        int placeNum = Random.Range(0, magicNumber.Length);
        int randomNum = Random.Range(1, 9);

        magicNumber[placeNum] = randomNum;

        StartCoroutine(GameManager.Instance.initData.SetRandomNum(placeNum));

        GameObject winEffect = LeanPool.Spawn(GameManager.Instance.effector.numEffectView,
            GameManager.Instance.turnControl.rawWinner[placeNum].transform.position, Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(winEffect, 2));
    }

    private void CheckCompleteBonuses()
    {
        if (GameManager.Instance.startGameControlRef.isClassic)
        {
            return;
        }
        
        if (currentWinLuckyBonus.Green == currentLuckyBonus.Green && currentWinLuckyBonus.Red == currentLuckyBonus.Red)
        {
            BonusManager.Instance.RoundBonusComplete();
            AudioManagerGame.Instance.PlayAchive();
        }
    }
    
    private void CheckScores()
    {
        ExpManager.Instance.CurrentExperience += scoreWinnerPanel;
        ExpManager.Instance.ShowScore();
    }

    // ------------------------------- БОНУСЫ -----------------------------//

    // ------------------------------- ЭФФЕКТЫ -----------------------------//

    IEnumerator WinnerEffectAndNumber() // эффект появления цифр при победе
    {
        for (int j = 0; j < numLimit; j++)
        {
            rawWinner[j].text = magicNumber[j].ToString();
            GameObject winEffect = LeanPool.Spawn(effector.numEffectView, rawWinner[j].transform.position, Quaternion
                .identity);
            StartCoroutine(effector.Despawner(winEffect, 2));
            yield return new WaitForSeconds(0.18f);
        }
    }

    public IEnumerator SetQuestionToWinner() // эффект звезд и установка вопросов в поле 
    {
        blockButton = true;
        BtnClick.Instance.OfBtn = false;
        yield return new WaitForSecondsRealtime(1.5f);
        shinyMainWindowControl.ShowMain();
        for (int j = 0; j < numLimit; j++)
        {
            // GameObject smokeEffect = LeanPool.Spawn(effector.smokeEffect, rawWinner[j].transform.position, Quaternion.identity);
            rawWinner[j].text = "?";
            // StartCoroutine(effector.Despawner(smokeEffect , 2));
        }

        blockButton = false;
        BtnClick.Instance.OfBtn = true;
    }

    // эффект подсветки числа
    public void LightNumber(string lightNum)
    {
        for (int j = 0; j < 10; j++)
        {
            for (int i = 0; i < numLimit; i++)
            {
                Image imageFirst = raws[j].transform.GetChild(i).GetComponent<Image>();
                numTextLightNum = imageFirst.transform.GetChild(0).GetComponent<TextMeshProUGUI>();

                if (numTextLightNum.text.Equals(lightNum))
                {
                    // imageFirst.GetComponent<CellsShiny>().Show();
                    effect2 = LeanPool.Spawn(effector.LightGlowEffect, imageFirst.transform.position, Quaternion.identity);
                    effect2.transform.parent = holdBtn.transform;
                }
            }
        }
    }

    public void LightEffectStop()
    {
        int efeectCount = holdBtn.transform.childCount;
        for (int i = 0; i < efeectCount; i++)
        {
            Transform lightEffect = holdBtn.transform.GetChild(i);
            Destroy(lightEffect.gameObject);
        }
    }
 
    public void ColorWhiteAllNum()
    {
        AudioManagerGame.Instance.PlayDebafSound();
        
        for (int j = 0; j < 10; j++)
        {
            for (int i = 0; i < numLimit; i++)
            {
                Image imageFirst = raws[j].transform.GetChild(i).GetComponent<Image>();
                if (imageFirst.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text == String.Empty)
                {
                    continue;
                }

                imageFirst.transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = Color.white;
                imageFirst.transform.GetChild(0).GetComponent<TextMeshProUGUI>().colorGradientPreset = GameManager.Instance.initData.gradients[0];
                GameObject effectBum = LeanPool.Spawn(effector.winnerEffect, imageFirst.transform.position, Quaternion.identity);
                StartCoroutine(effector.Despawner(effectBum, 2));
            }
        } 
    }

    public int CountBonusNum(string bonusNum)
    {
        int count = 0;
        
        for (int i = 0; i < numLimit; i++)
        {
            Image imageFirst = raws[rawNumber-1].transform.GetChild(i).GetComponent<Image>();
            numTextLightNum = imageFirst.transform.GetChild(0).GetComponent<TextMeshProUGUI>();

            if (numTextLightNum.text.Equals(bonusNum))
            {
                count++;
                // imageFirst.GetComponent<CellsShiny>().Show();
                // effect2 = LeanPool.Spawn(effector.LightGlowEffect, imageFirst.transform.position, Quaternion.identity);
                // effect2.transform.parent = holdBtn.transform;
            }
        }

        return count;
    }
}
// ------------------------------- ЭФФЕКТЫ -----------------------------//