﻿using System;
using System.Collections;
using UnityEngine;

public class HoldBtn : GenericSingletonClass<HoldBtn>
{
    public bool StopEffect { set; get; } = true;
    private Effector effector; // все эффекты
    private float futureTime;
    private TurnControl _turnControl;

    private void Start()
    {
        _turnControl = FindObjectOfType<TurnControl>();
    }

    public void HoldBtnDown()
    {
        StartCoroutine(Hold1("1"));
    }

    public void HoldBtnDown2()
    {
        StartCoroutine(Hold1("2"));
    }

    public void HoldBtnDown3()
    {
        StartCoroutine(Hold1("3"));
    }

    public void HoldBtnDown4()
    {
        StartCoroutine(Hold1("4"));
    }

    public void HoldBtnDown5()
    {
        StartCoroutine(Hold1("5"));
    }

    public void HoldBtnDown6()
    {
        StartCoroutine(Hold1("6"));
    }

    public void HoldBtnDown7()
    {
        StartCoroutine(Hold1("7"));
    }

    public void HoldBtnDown8()
    {
        StartCoroutine(Hold1("8"));
    }

    public void HoldBtnDown9()
    {
        StartCoroutine(Hold1("9"));
    }

    public void HoldBtnUp()
    {
        StopAllCoroutines();
        StartCoroutine(DelayOfBtn());
        _turnControl.LightEffectStop();
    }

    IEnumerator DelayOfBtn()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        BtnClick.Instance.OfBtn = true;
    }

    public IEnumerator Hold1(String holdNum)
    {
        yield return new WaitForSeconds(0.3f);
        AudioManagerGame.Instance.FindNumSound();
        BtnClick.Instance.OfBtn = false;
        StopEffect = true;
        _turnControl.LightNumber(holdNum);
    }
}