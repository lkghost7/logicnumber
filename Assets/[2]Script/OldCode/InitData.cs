﻿using System;
using System.Collections;
using Lean.Pool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InitData : MonoBehaviour
{
    private int countCicle = 40;

    [SerializeField] [Tooltip("время ожидания между цифрами")]
    private float nunWhite = 0.015f;

    [SerializeField] public TMP_ColorGradient[] gradients;

    public Color[] colorUser; // цветовая палитра
    private TurnControl turnControl;
    private Effector effector;

    private void Awake()
    {
        effector = FindObjectOfType<Effector>();
        turnControl = FindObjectOfType<TurnControl>();
    }

    public IEnumerator SetRandomNum(int numPlace) // установить рандомную цифру в конкретную ячейку
    {
        for (int k = 0; k < countCicle; k++)
        {
            yield return new WaitForSeconds(nunWhite);
            turnControl.rawWinner[numPlace].text = Helper.SetRandomNum(10,1).ToString();
        }

        turnControl.rawWinner[numPlace].text = "?";
    }

    public IEnumerator SetStartNum(int numLimit)
    {
        yield return new WaitForSeconds(0.1f);
        BtnClick.Instance.OfBtn = false;

        if (numLimit == 3)
        {
            for (int k = 0; k < countCicle; k++)
            {
                yield return new WaitForSeconds(nunWhite);
                for (int f = 0; f < 3; f++)
                {
                    turnControl.rawWinner[f].text = Helper.SetRandomNum(10,1).ToString();
                }
            }

            // StartCoroutine(turnControl.WinnersmokeEffect());
            BtnClick.Instance.OfBtn = true;
        }

        if (numLimit == 4)
        {
            for (int k = 0; k < countCicle; k++)
            {
                yield return new WaitForSeconds(nunWhite);
                for (int f = 0; f < 4; f++)
                {
                    turnControl.rawWinner[f].text = Helper.SetRandomNum(10,1).ToString();
                }
            }

            // StartCoroutine(turnControl.WinnersmokeEffect());
            BtnClick.Instance.OfBtn = true;
        }

        if (numLimit == 5)
        {
            for (int k = 0; k < countCicle; k++)
            {
                yield return new WaitForSeconds(nunWhite);
                for (int f = 0; f < 5; f++)
                {
                    turnControl.rawWinner[f].text = Helper.SetRandomNum(10,1).ToString();
                }
            }

            // StartCoroutine(turnControl.WinnersmokeEffect());
            BtnClick.Instance.OfBtn = true;
        }

        if (numLimit == 6)
        {
            for (int k = 0; k < countCicle; k++)
            {
                yield return new WaitForSeconds(nunWhite);
                for (int f = 0; f < 6; f++)
                {
                    turnControl.rawWinner[f].text = Helper.SetRandomNum(10,1).ToString();
                }
            }

            // StartCoroutine(turnControl.WinnersmokeEffect());
            BtnClick.Instance.OfBtn = true;
        }

        if (numLimit == 7)
        {
            // for(int k = 0; k < countCicle; k++) 
            // {
            //     yield return new WaitForSeconds(nunWhite);
            //     for(int f = 0; f < 7; f++) {
            //         turnControl.rawWinner[f].text = Helper.SetRandomNum().ToString();
            //     }
            // }

            // StartCoroutine(turnControl.WinnersmokeEffect());
            BtnClick.Instance.OfBtn = true;
        }
    }
    
    public void InitMagicNum(int numLimit)
    {
        turnControl.magicNumber = new int[numLimit];
        for (int i = 0; i < numLimit; i++)
        {
            turnControl.magicNumber[i] = Helper.SetRandomNum(10,1);
        }
    }

    public void InitUniqMagicNum(int numLimit)
    {
        int[] uniqSet = Helper.SetUniqueArray(numLimit, 10, 1);
        int[] repeatSet = Helper.SetRepeatNum(numLimit);
        turnControl.magicNumber = GameManager.Instance.startGameControlRef.uniq ? uniqSet : repeatSet;
    }

// метод для установки конкретного цвета
    public void SetColorNumber(String getBtn, TextMeshProUGUI numText)
    {
        for (int i = 0; i < 10; i++)
        {
            if (getBtn.Equals(i.ToString()))
            {
                numText.color = colorUser[i];
                numText.colorGradientPreset = gradients[i];
            }
        }
    }
}