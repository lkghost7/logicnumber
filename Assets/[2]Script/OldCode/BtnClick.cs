﻿using System.Collections;
using UnityEngine;

public class BtnClick : GenericSingletonClass<BtnClick>
{
    public bool OfBtn { set; get; } = true;
    public bool isFreezeRoulette { set; get; } = false;
    public TurnControl _turnControl { get; set; }
    private InitBtn _initBtn;

    private bool isTurnLock;

    private void Start()
    {
        _turnControl = FindObjectOfType<TurnControl>();
    }

    public void btn1()
    {
        if (!OfBtn)
        {
            return;
        }

        if (_turnControl.blockButton)
        {
            return;
        }

        _turnControl.PressBtn("1");
        AudioManagerGame.Instance.PlayButtonSound();
    }

    public void btn2()
    {
        if (!OfBtn)
        {
            return;
        }
        
        if (_turnControl.blockButton)
        {
            return;
        }

        _turnControl.PressBtn("2");
        AudioManagerGame.Instance.PlayButtonSound();
    }

    public void btn3()
    {
        if (!OfBtn)
        {
            return;
        }
        
        if (_turnControl.blockButton)
        {
            return;
        }

        _turnControl.PressBtn("3");
        AudioManagerGame.Instance.PlayButtonSound();
    }

    public void btn4()
    {
        if (!OfBtn)
        {
            return;
        }
        
        if (_turnControl.blockButton)
        {
            return;
        }

        _turnControl.PressBtn("4");
        AudioManagerGame.Instance.PlayButtonSound();
    }

    public void btn5()
    {
        if (!OfBtn)
        {
            return;
        }
        
        if (_turnControl.blockButton)
        {
            return;
        }

        _turnControl.PressBtn("5");
        AudioManagerGame.Instance.PlayButtonSound();
    }

    public void btn6()
    {
        if (!OfBtn)
        {
            return;
        }
        
        if (_turnControl.blockButton)
        {
            return;
        }

        _turnControl.PressBtn("6");
        AudioManagerGame.Instance.PlayButtonSound();
    }

    public void btn7()
    {
        if (!OfBtn)
        {
            return;
        }
        
        if (_turnControl.blockButton)
        {
            return;
        }

        _turnControl.PressBtn("7");
        AudioManagerGame.Instance.PlayButtonSound();
    }

    public void btn8()
    {
        if (!OfBtn)
        {
            return;
        }
        
        if (_turnControl.blockButton)
        {
            return;
        }

        _turnControl.PressBtn("8");
        AudioManagerGame.Instance.PlayButtonSound();
    }

    public void btn9()
    {
        if (!OfBtn)
        {
            return;
        }
        
        if (_turnControl.blockButton)
        {
            return;
        }

        _turnControl.PressBtn("9");
        AudioManagerGame.Instance.PlayButtonSound();
    }
    
    public void del()
    {
        if (!OfBtn)
        {
            return;
        }

        if (_turnControl.markNumber == 0)
        {
            return;
        }

        _turnControl.Del();
        AudioManagerGame.Instance.DelSound();
    }

    public void endTurn()
    {
        if (!OfBtn)
        {
            return;
        }

        if (isTurnLock)
        {
            return;
        }

        _turnControl.EndTurn();
        
        StartCoroutine(WaitTurn());
    }

    IEnumerator WaitTurn() // после турна подождать 1 секунду
    {
        isTurnLock = true;
        yield return new WaitForSecondsRealtime(1f);
        isTurnLock = false;
    }

    public void StartRoulette()
    {
        if (!OfBtn)
        {
            return;
        }

        if (isFreezeRoulette)
        {
            return;
        }
        
        GameManager.Instance.LostBonusAction.Invoke(0);
        GameManager.Instance.timeBonusRoulette.StartRoulette();
    }

    public void Empty()
    {
        if (!OfBtn)
        {
            return;
        }
        
        if (_turnControl.blockButton)
        {
            return;
        }

        _turnControl.EmptyBtn();
        AudioManagerGame.Instance.EmptySound();
    }

    public void RestartRound()
    {
        // GameManager.LoadCurrentScene();
    }

    public void NextRound()
    {
        // GameManager.LoadNextScene();
    }
}