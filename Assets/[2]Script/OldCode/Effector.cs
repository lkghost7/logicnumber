﻿using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using UnityEngine;

public class Effector : MonoBehaviour
{ 
    [Tooltip("эффект левой панели")]     public  GameObject RawEffect; //партикл эффекты
    [Tooltip("эффект при выйгрыше")]     public  GameObject winnerEffect;
    [Tooltip("эффект добовления цифры")] public  GameObject burstEffect;
    [Tooltip("эффект добовления цифры")] public  GameObject burstEffect2;
    [Tooltip("эффект удаления цифры")] public  GameObject delEffect;
    [Tooltip("эффект звездочек")] public  GameObject starsEffect;
    [Tooltip("эффект  дыма")] public  GameObject smokeEffect;
    [Tooltip("эффект  подсветки")] public GameObject LightGlowEffect;
    
    [Tooltip("эффект  показа цифр")] public GameObject numEffectView;
    [Tooltip("эффект  показа цифр")] public GameObject winEffectNumAll;
    [Tooltip("эффект  показа цифр")] public GameObject winEffectNum;
    [Tooltip("эффект  показа цифр")] public GameObject magicNumFlash;
    [Tooltip("эффект  показа цифр")] public GameObject winPvpCheck;
    [Tooltip("эффект  показа цифр")] public GameObject winPvpCheck2;
    
    [Tooltip("cellsLock")] public GameObject cellsLock;
    [Tooltip("unblockBtn")] public GameObject unblockBtn;
    [Tooltip("blockBonusBtn")] public GameObject blockBonusBtn;
    [Tooltip("bonusUp")] public GameObject bonusUp;
    [Tooltip("blockEmpty")] public GameObject blockEmpty;
    [Tooltip("addHeart")] public GameObject addHeart;
 
    public  IEnumerator  Despawner(GameObject effect, float time) // деспаунит эффекты 
    {
        yield return new WaitForSeconds(time);
        LeanPool.Despawn(effect);
    }
}
