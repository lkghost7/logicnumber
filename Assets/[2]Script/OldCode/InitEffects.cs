﻿using System.Collections;
using Lean.Pool;
using UnityEngine;
using UnityEngine.UI;

public class InitEffects : MonoBehaviour
{
    private Effector effector; // все эффекты
 
    void Start()
    {
        effector = FindObjectOfType<Effector>();
    }

    void Update()
    {
        
    }

    public IEnumerator WinnersmokeEffect(int numLimit, Text[] rawWinner) {
        
        for (int j = 0; j < numLimit; j++)
        {
            GameObject smokeEffect = LeanPool.Spawn(effector.smokeEffect, rawWinner[j].transform.position, Quaternion.identity);
            rawWinner[j].text = "?";
            StartCoroutine(effector.Despawner(smokeEffect , 2));
            yield return null;
        }
    }
}
