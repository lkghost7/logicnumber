﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class WellControl : MonoBehaviour
{
    [SerializeField] private RouletteItemUi[] rouletteItemUis;
    [SerializeField] private Button spinBtn;
    
    float time = 0f;
    private float speedRotation = 2f;
    private float endAngle = 0;
    private float duration = 4;

    public void Rotated()
    {
        spinBtn.interactable = false;
        time = 0;
        BonusItem bonusItem = BonusManager.Instance.GetRandomReward(BonusManager.Instance.bonusItems); // крутить награду по весу
        transform.DORotate(new Vector3(0,0,bonusItem.PositionRoulette), BonusManager.Instance.duration, RotateMode.FastBeyond360).SetEase(Ease.OutQuart);
        BonusManager.Instance.BonusChose.Invoke(bonusItem);
    }

    public void Show()
    {
        spinBtn.interactable = true;
        transform.rotation = new Quaternion(0, 0, 0, 0);
        BonusManager.Instance.NewRandomBonus();
        InitWell();
    }

    public void ResetRulet()
    {
        transform.DORotate(new Vector3(0, 0, 0), 0, RotateMode.FastBeyond360);
    }

    public void InitWell() // загрузить рандомные данные для рулетки
    {
        for (int i = 0; i < rouletteItemUis.Length; i++)
        {
            // rouletteItemUis[i].description.text = BonusManager.Instance.bonusItems[i].Name;
            rouletteItemUis[i].percent.text = BonusManager.Instance.bonusItems[i].Chance + "%";
            rouletteItemUis[i].icon.sprite = Resources.Load<Sprite>("BufItem/" + BonusManager.Instance.bonusItems[i].Name);

            if ( BonusManager.Instance.bonusItems[i].isBuf)
            {
                rouletteItemUis[i].percent.color = Color.green;
                rouletteItemUis[i].description.color = Color.green;
            }
            else
            {
                rouletteItemUis[i].percent.color = Color.red;
                rouletteItemUis[i].description.color = Color.red;
            }
        }
    }
}