﻿using System;
using DG.Tweening;
using Lean.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ViewBonus : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI titleDescription;
    [SerializeField] private Image bonusIcon;
    [SerializeField] private TextMeshProUGUI bonusName;
 
    public void Show(BonusItem bonusItem)
    {  
        gameObject.SetActive(true);
        AudioManagerGame.Instance.PlayShowBonus();
        titleDescription.text = bonusItem.Description;
        bonusName.text = bonusItem.NameDescription;
        bonusIcon.sprite = Resources.Load<Sprite>("BufItem/" + bonusItem.Name); 
        
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(1, 0.5f).SetDelay(3.2f).SetEase(Ease.OutCubic);
 
        if (bonusItem.bonusType == BonusEnum.SEE_NUMBER)
        {
            string descr = LeanLocalization.GetTranslationText("bonus.addLife.descr");
            string num = String.Format(descr, GetCurrentNum());
            titleDescription.text = num;

            // if (GameManager.Instance.SeeNumberUse)
            // {
            //     titleDescription.text = "Тут был предсказетель, табличка гласит (только 1 раз за раунд) ";
            // }
        }

        // if (bonusItem.bonusType == BonusEnum.ADD_LIFE && GameManager.Instance.LifeAddUse)
        // {
        //     titleDescription.text = "Жизнь можно получить только 1 раз за раунд (стоят ли шансы того)";
        // }
    }

    public void Hide()
    {
        AudioManagerGame.Instance.PlayTestSound();
        transform.DOScale(0, 0.5f).SetDelay(0).SetEase(Ease.InBack).OnComplete(() =>
        {
            transform.gameObject.SetActive(false);
        });
    }
      
    private int GetCurrentNum()
    {
        int[] magicNum = GameManager.Instance.turnControl.magicNumber;
        int resultNum = Random.Range(0, magicNum.Length);
        return magicNum[resultNum];
    }
}