﻿using System;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class BonusResultOneNum : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textNum;
    [SerializeField] private CanvasGroup _canvasGroup;
    public int CurrentNum { get; set; } = 0;
    public void Show()
    { 
        GetCurrentNum();
        gameObject.SetActive(true);
        GetComponent<CanvasGroup>().alpha = 0;
        string num = String.Format("Вероятно есть число {0}, но это не точно", CurrentNum);
        textNum.text = num;
        _canvasGroup.DOFade(0.9f, 1);
    }

    public IEnumerator Hide(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        transform.GetComponent<CanvasGroup>().DOFade(0, 1).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }

    private void GetCurrentNum()
    {
        int[] magicNum = GameManager.Instance.turnControl.magicNumber;
        int resultNum = Random.Range(0, magicNum.Length);
        CurrentNum = magicNum[resultNum];
    }
}
