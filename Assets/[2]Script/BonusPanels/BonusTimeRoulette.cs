﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
  
public class BonusTimeRoulette : MonoBehaviour
{
    [SerializeField][Tooltip("задержка хайда для бонуса угадать число")] private float delayHide = 0;
    private List<BonusItem> _bonusItems = new List<BonusItem>();
    [SerializeField] private WellControl wellControl;
    [SerializeField][Tooltip("кнопка рулетки")] public ShinyButtonControl shinyButtonRoulette;
    
    [SerializeField][Tooltip("окно с выбранным бонусом")] public ViewBonus parentViewBonus;

    private Action<int> currentAction = null;
    private bool isClickBtn;
    public void StartRoulette()
    {
        AudioManagerGame.Instance.BonusSound();
        Show();
    }
       
    private void Show()
    {
        gameObject.SetActive(true);
        isClickBtn = false;
        GetComponent<CanvasGroup>().alpha = 0;
        GetComponent<CanvasGroup>().DOFade(1, 1);
        wellControl.Show();
        TimeManager.Instance.StopTimer = true;
 
        if (GameManager.Instance.CurrentBonus <= 0)
        {
            shinyButtonRoulette.ButtonAcceptDisable();
        }
        else
        {
            shinyButtonRoulette.ButtonAcceptEnable();
        }
    }
 
    public void StartHide(float delay)
    {
        if (isClickBtn)
        {
            return;
        }
        
        isClickBtn = true;
        StartCoroutine(Hide(delay));
        parentViewBonus.Hide();
    }

    private IEnumerator Hide(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        GetComponent<CanvasGroup>().DOFade(0, 0.7f).OnComplete(() =>
        {
            gameObject.SetActive(false);
            TimeManager.Instance.StopTimer = false;
            // wellControl.Hide();
            currentAction.Invoke(0);
        });
    }
    
    public void ShowCurrentBonus(BonusItem bonusItem, Action<int> action) 
    {
        parentViewBonus.Show(bonusItem);
        currentAction = action;
    }

    public void ActiveButtonRoulette()
    {
        shinyButtonRoulette.ButtonAcceptEnable();
    }
}
