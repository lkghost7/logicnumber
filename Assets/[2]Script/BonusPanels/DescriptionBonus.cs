﻿
public static class DescriptionBonus
{
    public const string LifeDescription = "Добавить одну жизнь";
    public const string LifeDescriptionName = "+1 жизнь";
    
    public const string LockCellDescription = "Будут заблокированы ячейки от 1 до 3";
    public const string LockCellDescriptionName = "Заблокировать ячейку";
    
    public const string UnLockCellDescription = "Будет открыта кнопка для разблокировки ячеек от 1 до 3";
    public const string UnLockCellDescriptionName = "Кнопка разблокировать ячейку";
    
    public const string FreezeRowDescription = "Будет заблокирован ряд";
    public const string FreezeRowDescriptionName = "заморозить ряд";
    
    public const string UnfreezeRowDescription = "Будет открыта кнопка для разблокировка ряда";
    public const string UnfreezeRowDescriptionName = "Разморозить ряд";
    
    public const string LockDellBtnDescription = "Будет заблокировона кнопка удалить";
    public const string LockDellBtnDescriptionName = "Заблокировать удалить";
    
    public const string LockEmptyBtnDescription = "Будет заблокировона кнопка пустышка";
    public const string LockEmptyBtnDescriptionName = "Заблокировать пустышку";
    
    public const string UnLockMagicBtnDescription = "Будет разблокирована кнопка (открыть цифру)";
    public const string UnLockMagicBtnDescriptionName = "Кнопка узнать цифру";
    
    public const string UnLockClearBtnDescription = "Будет разблокирована кнопка (очистить все)";
    public const string UnLockClearBtnDescriptionName = "Кнопка очистить все";
    
    public const string RandomNumDescription = "Будет назначено новое неизвестное число в ячейку";
    public const string RandomNumDescriptionName = "Поменять цифру";
    
    public const string WhiteAllDescription = "Все видимые цифры станут белыми";
    public const string WhiteAllDescriptionName = "Белые цифры";
    
    public const string СatastropheDescription = "Котострофа, случайным образом будут заблокировано много ячеек";
    public const string СatastropheDescriptionName = "Котострофа";
    
    public const string HideRowDescription = "Один из активных рядов станет невидимым";
    public const string HideRowDescriptionName = "Спрятать ряд";
    
    public const string SeeAllDescription = "Узнать одну случайную цифру (только 1 раз за рануд)";
    public const string SeeAllDescriptionName = "Узнать цифру";
    
    public const string TimeExpDescription = "+2 деления для к Time bonus";
    public const string TimeExpDescriptionName = "+ опыт к Тайм бонус";
    
    public const string RowExpDescription = "+1 деление к Row bonus";
    public const string RowExpDescriptionName = "+ опыт к Бонус ряда";
    
    public const string LevelUpDescription = "Мгновенно добавить 10 делений к уровню";
    public const string LevelUpDescriptionName = "+10 к уровню";
}
 