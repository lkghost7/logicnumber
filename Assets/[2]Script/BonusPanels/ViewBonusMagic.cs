﻿using DG.Tweening;
using UnityEngine;

public class ViewBonusMagic : MonoBehaviour
{
    public void Show()
    {
        gameObject.SetActive(true);
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(1, 0.5f).SetDelay(0f).SetEase(Ease.OutCubic);
    }

    public void Hide()
    {
        transform.DOScale(0, 0.5f).SetDelay(0).SetEase(Ease.InBack).OnComplete(() =>
        {
            transform.gameObject.SetActive(false);
        });
    }
}