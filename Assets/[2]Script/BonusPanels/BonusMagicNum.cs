﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
 
public class BonusMagicNum : MonoBehaviour
{
    [Header("buttons")]
    [SerializeField] private Button[] magicButtons;

    [SerializeField] private ViewBonusMagic viewBonus;
    
    public void Show()
    {
        gameObject.SetActive(true);
        viewBonus.gameObject.SetActive(false);
        GetComponent<CanvasGroup>().alpha = 0;
        GetComponent<CanvasGroup>().DOFade(1, 0.5f).OnComplete(() =>
        {
            viewBonus.Show();
        });
        
        foreach (Button magicButton in magicButtons)
        {
            magicButton.interactable = true;
        }
    }

    public void Hide()
    {
        viewBonus.Hide();
        GetComponent<CanvasGroup>().DOFade(0, 0.5f).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }

    public void PressMagicNum(int num)
    {
        foreach (Button magicButton in magicButtons)
        {
            magicButton.interactable = false;
        }

        BonusManager.Instance.ControlBonusMagic(num);
        
        Hide();
    }
}
