﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RouletteItemUi : MonoBehaviour
{
    public string Id { get; set; }
    [SerializeField] public TextMeshProUGUI percent;
    [SerializeField] public TextMeshProUGUI description;
    [SerializeField] public Image icon;
}
