﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Pool;
using UnityEngine;
using UnityEngine.UI;

public class LevelUp : MonoBehaviour
{ 
    [SerializeField] private LevelUpItemUi bufItemUiPrefab;
    [SerializeField] private LevelUpItemUi deBufItemUiPrefab;
    [SerializeField] private Transform parentSpawn;
    [SerializeField] private GameObject blockRaycast;

    private int currentChance;
    private int currentMaxChance = 25;
    
    public void Show()
    {
        blockRaycast.SetActive(false);
        gameObject.SetActive(true);
        
        switch (GameSettings.Instance.CurrentDifficulty)
        {
            case DifficultyEnum.EASY:
                currentChance = 5;
                currentMaxChance = 50;
                break;
            case DifficultyEnum.NORMAL:
                currentChance = 4;
                currentMaxChance = 35;
                break;
            case DifficultyEnum.HARD:
                currentChance = 3;
                currentMaxChance = 25;
                break;
        }
        print(currentMaxChance + " currentMaxChance swith");
        GetComponent<CanvasGroup>().alpha = 0;
        GetComponent<CanvasGroup>().DOFade(1, 1);
        SetLevelUpItem();
        TimeManager.Instance.StopTimer = true;
   
        GameSettings.Instance.achiveManager.ChekLevelAchieve(ExpManager.Instance.CurrentLevelExp, GameSettings.Instance.CurrentDifficulty);
    }

    public void StartHide(float delay, LevelUpItemUi levelUpItemUi, int chance, Button button)
    {
        levelUpItemUi.ShowEffect(chance);
        StartCoroutine(Hide(delay));
        button.interactable = false;
        blockRaycast.SetActive(true); 
    }

    public IEnumerator Hide(float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        GetComponent<CanvasGroup>().DOFade(0, 0.7f).OnComplete(() =>
        {
            gameObject.SetActive(false);
            TimeManager.Instance.StopTimer = false;
        });
    }
     
    public void HideNow()
    {
        GetComponent<CanvasGroup>().DOFade(0, 0.7f).OnComplete(() =>
        {
            gameObject.SetActive(false);
            TimeManager.Instance.StopTimer = false;
        });
    }

    public void TutorHide()
    {
        StartCoroutine(Hide(0));
    }

    public void SetChanceDeBuf(string key)
    {
        print("SetChanceDeBuf" + key);
        if (BonusManager.Instance.bonusItemInit.BonusItemDictionary.ContainsKey(key))
        {
            BonusItem bonus = BonusManager.Instance.bonusItemInit.BonusItemDictionary[key];
            
            int currentBonus = bonus.Chance -= currentChance;

            if (currentBonus < 1)
            {
                bonus.Chance = 1;
            }
            
            BonusManager.Instance.bonusItemInit.BonusItemDictionary[key] = bonus;
        }
    }

    public void SetChanceBuf(string key)
    {
        print(currentMaxChance + " currentMaxChance");
        if (BonusManager.Instance.bonusItemInit.BonusItemDictionary.ContainsKey(key))
        {
            BonusItem bonus = BonusManager.Instance.bonusItemInit.BonusItemDictionary[key];
            
            int currentBonus = bonus.Chance += currentChance;
            
            if (currentBonus > currentMaxChance)
            {
                bonus.Chance = currentMaxChance;
            }
            
            BonusManager.Instance.bonusItemInit.BonusItemDictionary[key] = bonus;
        }
    }

    private void SetLevelUpItem()
    {
        GameManager.ClearParent(parentSpawn);

        List<BonusItem> currentBonusList = new List<BonusItem>();

        foreach (KeyValuePair<string, BonusItem> pair in BonusManager.Instance.bonusItemInit.BonusItemDictionary)
        {
            if (pair.Value.Name == "Сatastrophe")
            {
                continue;
            }
            
            if (pair.Value.Name == "Life")
            {
                continue;
            }
            
            currentBonusList.Add(pair.Value);
        }

        int[] uniqueNum = Helper.SetUniqueArray(5, 15, 0);

        for (int i = 0; i < uniqueNum.Length; i++)
        {
            BonusItem bonusItems = currentBonusList[uniqueNum[i]]; // достаем рандомный бонус

            if (bonusItems.isBuf)
            {
                LevelUpItemUi buf = LeanPool.Spawn(bufItemUiPrefab, parentSpawn);
                buf.Init(bonusItems.NameDescription, bonusItems.Name, bonusItems.Chance, bonusItems.Description);
                
                print("currentMaxChance 2" + currentMaxChance);
                print("bonusItems.Chance 2" + bonusItems.Chance);
                if (bonusItems.Chance >= currentMaxChance -2)
                {
                    buf.button.interactable = false;
                    buf.percent.text = "Max";
                }
                
                buf.button.onClick.AddListener(() =>
                {
                    SetChanceBuf(bonusItems.Name);
                    StartHide(0.7f, buf, bonusItems.Chance + currentChance, buf.button);

                    print("currentMaxChance 3" + currentMaxChance);
                    print("bonusItems.Chance " + bonusItems.Chance );
                    print("currentChance " + currentChance);
                    if (bonusItems.Chance + currentChance >= currentMaxChance -1)
                    {
                        bonusItems.Chance = currentMaxChance;
                        buf.percent.text = "Max";
                    }
                });
            }
            else
            {
                LevelUpItemUi deBuf = LeanPool.Spawn(deBufItemUiPrefab, parentSpawn);
                deBuf.Init(bonusItems.NameDescription, bonusItems.Name, bonusItems.Chance, bonusItems.Description);
                
                if (bonusItems.Chance <= 1)
                {
                    deBuf.button.interactable = false;
                    deBuf.percent.text = "Min";
                }
                
                deBuf.button.onClick.AddListener(() =>
                {
                    SetChanceDeBuf(bonusItems.Name);
                    StartHide(0.7f, deBuf, bonusItems.Chance - currentChance, deBuf.button);
                    
                    if (bonusItems.Chance - currentChance <= 1)
                    {
                        bonusItems.Chance = 1;
                        deBuf.percent.text = "Min";
                    } 
                });
            }
        }
    }
}