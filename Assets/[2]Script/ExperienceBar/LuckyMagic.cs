﻿using DG.Tweening;
using Lean.Pool;
using UnityEngine;
using UnityEngine.UI;

public class LuckyMagic : MonoBehaviour
{
    [SerializeField] private Image iconX;
    [SerializeField] private Image iconV;
    [SerializeField] private Transform effectTransform;
    [SerializeField] private CanvasGroup canvasGroup;

    public void CompleteRoundBonus()
    {
        canvasGroup.alpha = 0;
        
        iconX.gameObject.SetActive(false);
        iconV.gameObject.SetActive(true);

        canvasGroup.DOFade(0.8f, 2);
        
        GameObject effectBum = LeanPool.Spawn(GameManager.Instance.effector.bonusUp, effectTransform.position, Quaternion.identity);
        StartCoroutine(GameManager.Instance.effector.Despawner(effectBum, 2));
    }
}
