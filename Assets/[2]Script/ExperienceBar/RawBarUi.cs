﻿using System;
using System.Collections;
using Coffee.UIExtensions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class RawBarUi : MonoBehaviour
{
    private float delayWand = 0.25f;
    private float delayViewWand = 0.07f;
    [SerializeField] private ExpRawType expRawType;

    public Image[] wands;

    public void ShowRawStart(int expCount) // показать при старте сцены текущую экспу
    {
        foreach (Image image in wands)
        {
            image.GetComponent<CanvasGroup>().alpha = 0;
            image.gameObject.SetActive(false);
        }

        for (int i = 0; i < expCount; i++)
        {
            wands[i].gameObject.SetActive(true);
            wands[i].GetComponent<CanvasGroup>().DOFade(1, delayWand);
        }
    }
 
    public void AddExp(int expCount) // прибавить к экспе
    {
        StartCoroutine(DelayAddWand(expCount, expRawType));
    }

    IEnumerator DelayAddWand(int expCount, ExpRawType expRawType)
    {
        int currentAddWand = 0;
        int isActiveWand = 0;
        int countExpCount = 0;

        countExpCount = expCount;
        
        foreach (Image wand in wands)
        {
            if (wand.IsActive())
            {
                isActiveWand++;
            }
        }

        for (int i = 0; i < expCount; i++)
        {
            yield return new WaitForSecondsRealtime(delayViewWand);
            
            if (i + isActiveWand == 20)
            {
                wands[19].gameObject.SetActive(true);

                UIShiny shiny2 = wands[19].GetComponent<UIShiny>();
                CanvasGroup fadeWand2 = wands[19].GetComponent<CanvasGroup>();
                StartCoroutine(DelayShiny(shiny2));
                StartCoroutine(DelayFade(fadeWand2));

                yield return new WaitForSecondsRealtime(delayViewWand);
                // ZeroRaw(countExpCount);
                
                // ExpManager.Instance.AddRawWandAction.Invoke(expRawType);
                break;
            }
            
            wands[i + isActiveWand].gameObject.SetActive(true);

            UIShiny shiny = wands[i + isActiveWand].GetComponent<UIShiny>();
            CanvasGroup fadeWand = wands[i + isActiveWand].GetComponent<CanvasGroup>();
            StartCoroutine(DelayShiny(shiny));
            StartCoroutine(DelayFade(fadeWand));
            countExpCount--;
        }
 
        if (isActiveWand + expCount >= 20)
        {
            ZeroRaw(countExpCount, expRawType);
            // ExpManager.Instance.AddRawWandAction.Invoke(expRawType);
            ExpManager.Instance.ExpPanelLevelUp(expRawType);
        }
        
        CheckWound(expRawType, isActiveWand + expCount);
    }

    private void CheckWound(ExpRawType expRawType, int amountWand)
    {
        // print("expRawType " + expRawType);
        if (amountWand >= 20)
        {
            amountWand = 0;
        }
        
        switch (expRawType)
        {
            case ExpRawType.NONE:
                break;
            case ExpRawType.LEVEL_EXP:
                ExpManager.Instance.AmountRawWandLvl = amountWand;
                break;
            case ExpRawType.TIME_EXP:
                ExpManager.Instance.AmountRawWandTime = amountWand;
                break;
            case ExpRawType.NUM_EXP:
                ExpManager.Instance.AmountRawWandNum = amountWand;
                break;
            case ExpRawType.RAW_EXP:
                ExpManager.Instance.AmountRawWandRaw = amountWand;
                break;
        }
    }
 
    IEnumerator DelayShiny(UIShiny shiny)
    {
        yield return new WaitForSecondsRealtime(delayWand / 2);
        shiny.Play();
    }

    IEnumerator DelayFade(CanvasGroup canvasGroup)
    {
        yield return new WaitForSecondsRealtime(0);
        canvasGroup.DOFade(1, delayWand);
    }

    public void ZeroRaw(int countExpCount, ExpRawType expRawType) // обнулить при получении лвла
    {
        StartCoroutine(DelayEndShiny(countExpCount));

        switch (expRawType)
        { 
            case ExpRawType.NONE:
                break;
            case ExpRawType.LEVEL_EXP:
                ExpManager.Instance.CurrentExperience += ExpManager.Instance.AddExpToScoreBonus(1);
                ExpManager.Instance.CurrentExperienceBonus += ExpManager.Instance.AddExpToScoreBonus(1);
                ExpManager.Instance.countExperienceBonus++;
                
                ExpManager.Instance.ShowScore();
                break;
            case ExpRawType.TIME_EXP:
                ExpManager.Instance.CurrentExperience += ExpManager.Instance.AddExpToScoreBonus(2);
                ExpManager.Instance.CurrentExperienceBonus += ExpManager.Instance.AddExpToScoreBonus(2);
                ExpManager.Instance.countExperienceBonus++;
                
                ExpManager.Instance.ShowScore();
                break;
            case ExpRawType.NUM_EXP:
                ExpManager.Instance.CurrentExperience += ExpManager.Instance.AddExpToScoreBonus(3);
                ExpManager.Instance.CurrentExperienceBonus+= ExpManager.Instance.AddExpToScoreBonus(3);
                ExpManager.Instance.ShowScore();
                ExpManager.Instance.countExperienceBonus++;
                break;
            case ExpRawType.RAW_EXP:
                ExpManager.Instance.CurrentExperience += ExpManager.Instance.AddExpToScoreBonus(4);
                ExpManager.Instance.CurrentExperienceBonus += ExpManager.Instance.AddExpToScoreBonus(4);
                ExpManager.Instance.countExperienceBonus++;
                ExpManager.Instance.ShowScore();
                break;
        }
    }

    IEnumerator DelayEndShiny(int countExpCount)
    {
        for (int i = wands.Length - 1; i >= 0; i--)
        {
            wands[i].GetComponent<UIShiny>().Play();
            yield return new WaitForSecondsRealtime(0.04f);
        }

        foreach (Image image in wands)
        {
            image.GetComponent<CanvasGroup>().DOFade(0, delayWand).OnComplete(() =>
            {
                image.gameObject.SetActive(false);
            });
        }

        StartCoroutine(DelayWandTwo(countExpCount));
    }

    IEnumerator DelayWandTwo(int countExpCount)
    {
        yield return new WaitForSecondsRealtime(1f);
        if (countExpCount > 0)
        {
            AddExp(countExpCount);
        }
    }
}