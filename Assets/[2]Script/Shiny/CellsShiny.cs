﻿using Coffee.UIExtensions;
using UnityEngine;

public class CellsShiny : MonoBehaviour
{
    [SerializeField] private UIShiny[] shinysList;
    [SerializeField] private GameObject locker;
    public bool isLock;

    public void Show()
    {
        foreach (UIShiny uiShiny in shinysList)
        {
            uiShiny.enabled = true;
            uiShiny.Play();
        }
    }

    public void Hide()
    {
        foreach (UIShiny uiShiny in shinysList)
        {
            uiShiny.Stop();
            uiShiny.enabled = false;
        }
    }

    public void Lock()
    {
        isLock = true;
        locker.SetActive(true);
    }

    public void UnLock()
    {
        isLock = false;
        locker.SetActive(false);
    }
}