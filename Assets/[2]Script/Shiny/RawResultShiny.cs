﻿using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using DG.Tweening;
using UnityEngine;

public class RawResultShiny : MonoBehaviour
{
    [SerializeField] private UIShiny[] shinysList;
    [SerializeField] private CanvasGroup shtorka;

    public void Show()
    {
        foreach (UIShiny uiShiny in shinysList)
        {
            uiShiny.enabled = true;
            uiShiny.loop = false;
            uiShiny.Play();
            StartCoroutine(Hide());
        }
    }
    
    public void ShowMain()
    {
        shtorka.DOFade(0, 1.5f);
        foreach (UIShiny uiShiny in shinysList)
        {
            uiShiny.enabled = true;
            uiShiny.loop = false;
            uiShiny.Play();
            StartCoroutine(Hide());
        }
    }

    IEnumerator Hide()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        foreach (UIShiny uiShiny in shinysList)
        {
            uiShiny.Stop();
            uiShiny.enabled = false;
        }
    }
}