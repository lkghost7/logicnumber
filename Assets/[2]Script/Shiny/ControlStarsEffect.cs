﻿using UnityEngine;
using UnityEngine.UI;

public class ControlStarsEffect : MonoBehaviour
{
    [Header("Effects object")] 
    // [SerializeField] private GameObject starsEffect;
    // [SerializeField] private GameObject shinyEffect;

    [Header("Buttons")]
    [SerializeField] private Button buttonOnHelper;
    [SerializeField] private Button buttonOfHelper;

    [SerializeField] private Button buttonRuLocalization;
    [SerializeField] private Button buttonEngLocalization;
    
    [SerializeField] private Button buttonOnAvatar;
    [SerializeField] private Button buttonOfAvatar;

    public void Show()
    {
        
        if (GameSettings.Instance.isAvatarOn)
        {
            PressButtonAvatarOn();
        }
        else
        {
            PressButtonAvatarOf();
        }
        
        
        if (GameSettings.Instance.isHelperOn)
        {
            PressButtonHelperOn();
        }
        else
        {
            PressButtonHelperOf();
        }

        if (GameSettings.Instance.isLangueRu)
        {
            PressButtonEngOn();
        }
        else
        {
            PressButtonRuOn();
        }
    }
    
    public void PressButtonAvatarOn()
    {
        //Avatar
        GameSettings.Instance.isAvatarOn = true;
        buttonOfAvatar.gameObject.SetActive(true);
        buttonOnAvatar.gameObject.SetActive(false);
    }

    public void PressButtonAvatarOf()
    {
        //Avater
        GameSettings.Instance.isAvatarOn = false;
        buttonOnAvatar.gameObject.SetActive(true);
        buttonOfAvatar.gameObject.SetActive(false);
    }

    public void PressButtonHelperOn()
    {
        //helper
        GameSettings.Instance.isHelperOn = true;
        buttonOfHelper.gameObject.SetActive(true);
        buttonOnHelper.gameObject.SetActive(false);
    }

    public void PressButtonHelperOf()
    {
        //helper
        GameSettings.Instance.isHelperOn = false;
        buttonOnHelper.gameObject.SetActive(true);
        buttonOfHelper.gameObject.SetActive(false);
    }

    public void PressButtonEngOn()
    {
        //localization
        LoadingScene.Instance.ChangeLocalizationEngOn();
        GameSettings.Instance.isLangueRu = true;
        buttonEngLocalization.gameObject.SetActive(false);
        buttonRuLocalization.gameObject.SetActive(true);
    }

    public void PressButtonRuOn()
    {
        //localization
        LoadingScene.Instance.ChangeLocalizationRuOn();
        GameSettings.Instance.isLangueRu = false;
        buttonRuLocalization.gameObject.SetActive(false);
        buttonEngLocalization.gameObject.SetActive(true);
    }
}