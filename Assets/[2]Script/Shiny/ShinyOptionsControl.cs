﻿
using Coffee.UIExtensions;
using UnityEngine;

public class ShinyOptionsControl : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private UIShiny[] shinyOptions;

    public void Show()
    {
        foreach (UIShiny shinyOption in shinyOptions)
        {
            shinyOption.Play();
        }
    }

}
