﻿using System.Collections;
using Coffee.UIExtensions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ShinyButtonControl : MonoBehaviour
{
    [SerializeField] private UIShiny[] shinysList;
    private Coroutine hideCoroutine = null;
    private bool isHideActive;
    public Button accept;
    public CanvasGroup shadowButton;
    public Image border;
    
    [Header("Debauf Block")]
    public Image lockImage;
    public Image buttonImage;
    public Image borderImage;
    
    public void Show()
    {
        if (BtnClick.Instance._turnControl.blockButton && hideCoroutine != null)
        {
            return;
        }
        
        if (hideCoroutine != null)
        {
            return;
        }
        
        foreach (UIShiny uiShiny in shinysList)
        {
            uiShiny.enabled = true;
            uiShiny.loop = false;
            uiShiny.Play();
            hideCoroutine = StartCoroutine(Hide());
        }
    }
    
    public void ShowDel()
    {
        if (BtnClick.Instance._turnControl.markNumber == 0)
        {
            return;
        }
        
        if (hideCoroutine != null)
        {
            return;
        }
        
        foreach (UIShiny uiShiny in shinysList)
        {
            uiShiny.enabled = true;
            uiShiny.loop = false;
            uiShiny.Play();
            hideCoroutine = StartCoroutine(Hide());
        }
    }

     IEnumerator Hide()
    {
        yield return new WaitForSecondsRealtime(1f);
        foreach (UIShiny uiShiny in shinysList)
        {
            uiShiny.Stop();
            uiShiny.enabled = false;
        }

        hideCoroutine = null;
    }

     public void ButtonAcceptEnable()
     {
         accept.interactable = true;
         border.gameObject.SetActive(true);
         shadowButton.DOFade(0, 1f);
         
         foreach (UIShiny uiShiny in shinysList)
         {
             uiShiny.enabled = true;
             uiShiny.loop = true;
             uiShiny.Play();
         }
     } 
     
     public void ButtonAcceptDisable()
     {
         accept.interactable = false;
         border.gameObject.SetActive(false);
         shadowButton.DOFade(0.65f, 1f);
         
         foreach (UIShiny uiShiny in shinysList)
         {
             uiShiny.enabled = false;
             uiShiny.loop = false;
             uiShiny.Stop();
         }
     }
}
